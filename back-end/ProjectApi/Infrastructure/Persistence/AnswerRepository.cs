﻿using AppDomain.DTO;
using AppDomain.DTOs.Answer;
using AppDomain.Entities.NotificationRelated.AdditionalDetails;
using AppDomain.Entities.UserActionRelated;
using AppDomain.Enums;
using AppDomain.Interfaces;
using AppDomain.Responses.UserResponses;
using Application.Services;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Infrastructure.Persistence;

public class AnswerRepository : IAnswerRepository
{
    private ClubrickDbContext _context;
    private DapperContext _dapperContext;
    private IUserRepository _userRepository;
    private readonly INotificationRepository _notificationRepository;
    private readonly IMapper _mapper;

    public AnswerRepository(
        ClubrickDbContext context,
        DapperContext dapperContext,
        IUserRepository userRepository,
        INotificationRepository notificationRepository,
        IMapper mapper
    )
    {
        _context = context;
        _dapperContext = dapperContext;
        _userRepository = userRepository;
        _mapper = mapper;
        _notificationRepository = notificationRepository;
    }

    public Task<List<AppDomain.Entities.ContentRelated.Answer>> GetAnswerById(string id)
    {
        var answer = _context.Answers.Where(x => x.QuestionId == id).ToList();

        answer = answer.OrderByDescending(x => x.UpdateTime).ToList();

        return Task.FromResult(answer);
    }

    public async Task<string> InsertAnswer(AppDomain.Entities.ContentRelated.Answer answer)
    {
        var user = await _userRepository.GetCurrentUser();

        if (user.IsFrozen)
            return "-2";
        try
        {
            var findQuestion = await _context.Questions.FirstOrDefaultAsync(
                x => x.Id == answer.QuestionId
            );

            if (findQuestion is null)
                return "0";

            _context.Answers.Add(answer);

            await _context.SaveChangesAsync();

            if (findQuestion.UserId != answer.UserId)
            {
                var answerDetails = new AnswerDetails
                {
                    AnswerId = answer.Id,
                    AnswererId = user.Id,
                    QuestionId = findQuestion.Id,
                };

                await _notificationRepository.PostNotificationAsync(
                    new()
                    {
                        Type = NotificationType.Answer,
                        AdditionalDetails = JsonConvert.SerializeObject(answerDetails)
                    }
                );

                await _userRepository.AddBudsAsync(
                    ContentType.Answer,
                    answer.Id,
                    BudsActionType.WriteAnswerToUser,
                    answer.UserId
                );
            }

            return answer.Id;
        }
        catch (Exception)
        {
            return "-1";
        }
    }

    public async Task<string> UpdateAnswer(string answerId, string body)
    {
        var user = await _userRepository.GetCurrentUser();

        if (user.IsFrozen)
            return "-2";
        try
        {
            var findAnswer = _context.Answers.FirstOrDefault(x => x.Id == answerId);

            if (findAnswer is null)
                return "0";

            if (findAnswer.UserId != user.Id)
                return "0";

            findAnswer.Body = body;

            await _context.SaveChangesAsync();

            return answerId;
        }
        catch (Exception)
        {
            return "-1";
        }
    }

    public async Task<int> DeleteAnswer(string answerId)
    {
        var user = await _userRepository.GetCurrentUser();

        if (user.IsFrozen)
            return -2;

        try
        {
            var findAnswer = _context.Answers.FirstOrDefault(x => x.Id == answerId);

            if (findAnswer is null)
                return 0;

            if (findAnswer.UserId != user.Id)
                return 0;

            findAnswer.IsDeleted = true;

            findAnswer.UpdateTime = DateTime.UtcNow;

            await _context.SaveChangesAsync();

            return 1;
        }
        catch (Exception)
        {
            return -1;
        }
    }

    public async Task<int> VoteAnswer(VoteStatus status, string answerId, string userId)
    {
        var user = _context.Users.Include(u => u.VoteList).FirstOrDefault(x => x.Id == userId);

        if (user is null)
            return -1;

        if (user.IsFrozen)
            return -2;

        try
        {
            var answer = _context.Answers.FirstOrDefault(y => y.Id == answerId);

            if (user is null || answer is null)
                return 0;

            var voteEntity = user.VoteList.FirstOrDefault(
                s => s.ContentType == ContentType.Answer && s.ContentId.Equals(answerId)
            );

            if (voteEntity is null)
            {
                Vote vote =
                    new()
                    {
                        Id = IDGeneratorService.GetShortUniqueId(),
                        ContentId = answerId,
                        UserId = userId,
                        ContentType = ContentType.Answer,
                        Status = status
                    };

                answer.VoteCount += status == VoteStatus.UpVote ? 1 : -1;
                user.VoteList.Add(vote);
            }
            else
            {
                if (status != voteEntity.Status)
                {
                    if (voteEntity.Status == VoteStatus.DownVote)
                        answer.VoteCount += 1;
                    else if (voteEntity.Status == VoteStatus.UpVote)
                        answer.VoteCount -= 1;
                    voteEntity.Status = status;
                    answer.VoteCount += status == VoteStatus.DownVote ? -1 : 1;
                    _context.Update(voteEntity);
                }
                else
                {
                    answer.VoteCount += status == VoteStatus.DownVote ? 1 : -1;
                    user.VoteList.Remove(voteEntity);
                }
            }

            _context.Update(user);

            _context.Answers.Update(answer);

            await _context.SaveChangesAsync();

            return 1;
        }
        catch (Exception)
        {
            return -1;
        }
    }

    public async Task<GetByIdAnswerDTO> GetById(string answerId, string tokenId)
    {
        try
        {
            var findAnswer = _context.Answers
                .Include(x => x.Question)
                .FirstOrDefault(x => x.Id == answerId);

            var tokenUser = _context.Users
                .Include(x => x.VoteList)
                .FirstOrDefault(x => x.Id == tokenId);

            var answerUser = _context.Users.FirstOrDefault(x => x.Id == findAnswer.UserId);

            if (findAnswer is null)
                return null;

            GetByIdAnswerDTO getByIdAnswerDTO = _mapper.Map<GetByIdAnswerDTO>(findAnswer);

            getByIdAnswerDTO.UserContent = new UserContentTitleResponse()
            {
                UserId = answerUser.Id,
                Name = answerUser.Name,
                Username = answerUser.Username,
                ProfilePhoto = answerUser.ProfilePhoto,
            };

            VoteStatus voteStatus;

            if (tokenUser != null)
            {
                Vote vote = tokenUser.VoteList.FirstOrDefault(
                    x => x.ContentId == answerId && x.ContentType == ContentType.Answer
                );

                if (vote is null)
                    voteStatus = VoteStatus.UnVote;
                else
                    voteStatus = vote.Status;
            }
            else
                voteStatus = VoteStatus.UnVote;

            getByIdAnswerDTO.VoteDTO = new VoteDTO()
            {
                VoteCount = findAnswer.VoteCount,
                Status = voteStatus
            };

            return getByIdAnswerDTO;
        }
        catch (Exception)
        {
            throw;
        }
    }

    public async Task<Task> DeleteAnswers(string questionId)
    {
        await _context.Answers
            .Where(answer => answer.QuestionId == questionId)
            .ForEachAsync(answer =>
            {
                DeleteAnswer(answer.Id);
            });

        return Task.CompletedTask;
    }
}
