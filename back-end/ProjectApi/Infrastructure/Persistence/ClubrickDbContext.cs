﻿using AppDomain.Entities.AdminRelated;
using AppDomain.Entities.ContentRelated;
using AppDomain.Entities.TagBaseRelated;
using AppDomain.Entities.UserActionRelated;
using AppDomain.Entities.UserRelated;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Persistence;

public class ClubrickDbContext : DbContext
{
    public ClubrickDbContext(DbContextOptions<ClubrickDbContext> options)
        : base(options)
    {
        Database.EnsureCreated();
    }

    public DbSet<Vote> Votes { get; set; }
    public DbSet<Save> Saves { get; set; }
    public DbSet<View> Views { get; set; }
    public DbSet<Watch> Watches { get; set; }
    public DbSet<User> Users { get; set; }
    public DbSet<PendingUser> PendingUsers { get; set; }
    public DbSet<Article> Articles { get; set; }
    public DbSet<Answer> Answers { get; set; }
    public DbSet<Question> Questions { get; set; }
    public DbSet<Category> Categories { get; set; }
    public DbSet<Resource> Resources { get; set; }
    public DbSet<Lesson> Lessons { get; set; }
    public DbSet<Comment> Comments { get; set; }
    public DbSet<ArticleFlag> ArticleFlags { get; set; }
    public DbSet<ResourceFlag> ResourceFlags { get; set; }
    public DbSet<EmailVerification> EmailVerifications { get; set; }
    public DbSet<Follow> Follows { get; set; }
    public DbSet<Report> Reports { get; set; }
    public DbSet<Banner> Banners { get; set; }
    public DbSet<Product> Products { get; set; }
    public DbSet<Review> Reviews { get; set; }
    public DbSet<Favourite> Favourites { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Category>().HasQueryFilter(c => c.IsDeleted == false);
        modelBuilder.Entity<ResourceFlag>().HasQueryFilter(c => c.IsDeleted == false);
        modelBuilder.Entity<ArticleFlag>().HasQueryFilter(c => c.IsDeleted == false);
        modelBuilder.Entity<Answer>().HasQueryFilter(c => c.IsDeleted == false);
        modelBuilder.Entity<Question>().HasQueryFilter(c => c.IsDeleted == false);
        modelBuilder.Entity<Resource>().HasQueryFilter(c => c.IsDeleted == false);
        modelBuilder.Entity<Article>().HasQueryFilter(c => c.IsDeleted == false);
        modelBuilder.Entity<Comment>().HasQueryFilter(c => c.IsDeleted == false);
        modelBuilder.Entity<PendingUser>().HasQueryFilter(c => c.IsDeleted == false);
        modelBuilder.Entity<Product>().HasQueryFilter(c => c.IsDeleted == false);

        modelBuilder.Entity<Category>().Property(b => b.IsDeleted).HasDefaultValue(false);

        modelBuilder.Entity<Resource>().Property(r => r.IsDeleted).HasDefaultValue(false);

        modelBuilder.Entity<ResourceFlag>().Property(b => b.IsDeleted).HasDefaultValue(false);

        modelBuilder.Entity<ArticleFlag>().Property(b => b.IsDeleted).HasDefaultValue(false);

        modelBuilder.Entity<Answer>().Property(b => b.IsDeleted).HasDefaultValue(false);

        modelBuilder.Entity<Question>().Property(b => b.IsDeleted).HasDefaultValue(false);

        modelBuilder.Entity<Article>().Property(b => b.IsDeleted).HasDefaultValue(false);

        modelBuilder.Entity<User>().Property(b => b.IsDeleted).HasDefaultValue(false);

        modelBuilder.Entity<User>().Property(b => b.IsFrozen).HasDefaultValue(false);

        modelBuilder.Entity<Save>().Property(b => b.IsDeleted).HasDefaultValue(false);

        modelBuilder.Entity<View>().Property(b => b.IsDeleted).HasDefaultValue(false);

        modelBuilder.Entity<Vote>().Property(b => b.IsDeleted).HasDefaultValue(false);

        modelBuilder.Entity<Watch>().Property(b => b.IsDeleted).HasDefaultValue(false);

        modelBuilder.Entity<Comment>().Property(b => b.IsDeleted).HasDefaultValue(false);

        modelBuilder.Entity<Product>().Property(b => b.IsDeleted).HasDefaultValue(false);
    }
}