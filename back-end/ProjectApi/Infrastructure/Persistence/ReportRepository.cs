﻿using AppDomain.DTOs.Report;
using AppDomain.Entities.AdminRelated;
using AppDomain.Enums;
using AppDomain.Exceptions.UserExceptions;
using AppDomain.Interfaces;
using Application.Services;
using Infrastructure.Services;

namespace Infrastructure.Persistence;

/// <summary>
/// Repository for managing reports.
/// </summary>
public class ReportRepository : IReportRepository
{
    private readonly IUserRepository _userRepository;
    private readonly IModeratorRepository _moderatorRepository;
    private readonly ClubrickDbContext _context;

    /// <summary>
    /// Initializes a new instance of the <see cref="ReportRepository"/> class.
    /// </summary>
    /// <param name="userRepository">The repository for user-related operations.</param>
    /// <param name="context">The database context for accessing data.</param>
    public ReportRepository(
        IUserRepository userRepository,
        ClubrickDbContext context,
        IModeratorRepository moderatorRepository
    )
    {
        _userRepository = userRepository;
        _context = context;
        _moderatorRepository = moderatorRepository;
    }

    /// <inheritdoc/>
    public async Task<Task> ReportContent(ReportDTO reportDTO)
    {
        var user = await _userRepository.GetCurrentUser();

        if (user.IsFrozen)
            throw new UserForbiddenException(
                user.Id,
                "report",
                user.FrozenUntil ?? DateTime.UtcNow
            );

        var report = reportDTO.ToReport();

        report.Id = IDGeneratorService.GetShortUniqueId();
        report.UserId = user.Id;
        report.Status = ReportStatus.Active;
        report.CreatedDate = DateTime.UtcNow;

        _context.Reports.Add(report);

        await _context.SaveChangesAsync();

        LogService.LogUserAction(
            UserActions.Reported,
            user.Id,
            $"content type: {report.ContentType} with ID: {report.Id}."
        );

        return Task.CompletedTask;
    }

    /// <inheritdoc/>
    public async Task<IEnumerable<Report>> GetAllReports()
    {
        var reports = _context.Reports.OrderBy(r => r.CreatedDate).ToList();

        return reports;
    }

    /// <inheritdoc/>
    public async Task<IEnumerable<Report>> GetAllReportsByStatus(ReportStatus reportStatus)
    {
        var reports = _context.Reports
            .Where(r => r.Status == reportStatus)
            .OrderBy(r => r.CreatedDate)
            .ToList();

        return reports;
    }

    /// <inheritdoc/>
    public async Task<IEnumerable<Report>> GetAllReportsByModeratorId(
        string moderatorId,
        ReportStatus reportStatus = ReportStatus.All
    )
    {
        var reports = _context.Reports
            .Where(r => r.ResponsibleModeratorId == moderatorId)
            .OrderBy(r => r.CreatedDate)
            .ToList();

        if (reportStatus != ReportStatus.All)
            return reports.Where(r => r.Status == reportStatus);

        return reports;
    }

    /// <inheritdoc/>
    public async Task<Report> GetReportById(string reportId)
    {
        var report = _context.Reports.Find(reportId);

        return report;
    }

    /// <inheritdoc/>
    public async Task<Report> SubmitReportAsync(
        string reportId,
        ReportStatus reportStatus,
        string moderatorMessage
    )
    {
        var report = _context.Reports.Find(reportId);
        report.ResponsibleModeratorId = await _userRepository.GetCurrentUserId();
            
        switch (reportStatus)
        {
            case ReportStatus.Active:
                throw new Exception("Report cannot be submitted as Active.");
            case ReportStatus.Declined:
                break;
            case ReportStatus.ResultedWithContentFreeze:
                await _moderatorRepository.FreezeContentAsync(
                    report.ContentId,
                    report.ContentType,
                    report.ResponsibleModeratorId
                );
                break;
            default:
                break;
        }

        report.ModeratorMessage = moderatorMessage;
        report.Status = reportStatus;
        report.CaseClosedDate = DateTime.UtcNow;

        report = _context.Reports.Update(report).Entity;

        await _context.SaveChangesAsync();

        LogService.LogModeratorActionOnReport(
            reportStatus,
            report.ResponsibleModeratorId,
            reportId
        );

        return report;
    }

    /// <inheritdoc/>
    public async Task<Task> DeleteReportAsync(string reportId)
    {
        var report = _context.Reports.Find(reportId);

        report.isDeleted = true;

        _context.Reports.Update(report);

        await _context.SaveChangesAsync();

        LogService.LogModeratorAction(
            ModeratorActions.Deleted,
            report.ResponsibleModeratorId,
            $"report with ID: {reportId}"
        );

        return Task.CompletedTask;
    }
}
