﻿using AppDomain.DTO;
using AppDomain.DTOs.Article;
using AppDomain.DTOs.Category;
using AppDomain.DTOs.Comment;
using AppDomain.DTOs.Pagination;
using AppDomain.Entities.ContentRelated;
using AppDomain.Entities.NotificationRelated.AdditionalDetails;
using AppDomain.Entities.UserActionRelated;
using AppDomain.Entities.UserRelated;
using AppDomain.Enums;
using AppDomain.Interfaces;
using AppDomain.Responses;
using AppDomain.Responses.UserResponses;
using Application.Services;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Infrastructure.Persistence;

public class ArticleRepository : IArticleRepository
{
    private readonly ClubrickDbContext _context;

    private readonly IAzureBlobStorageService _azureBlobStorage;

    private readonly IUserRepository _userRepository;

    private readonly IMapper _mapper;

    private readonly INotificationRepository _notificationRepository;

    public ArticleRepository(
        ClubrickDbContext context,
        IUserRepository userRepository,
        IMapper mapper,
        IAzureBlobStorageService azureBlobStorage,
        INotificationRepository notificationRepository
    )
    {
        _context = context;
        _userRepository = userRepository;
        _mapper = mapper;
        _azureBlobStorage = azureBlobStorage;
        _notificationRepository = notificationRepository;
    }

    public async Task<List<GetAllArticleDTO>> GetArticlesForFeedAsync()
    {
        var userId = await _userRepository.GetCurrentUserId();

        var findUser = _context.Users
            .Include(x => x.VoteList)
            .Include(x => x.WatchList)
            .Include(x => x.ViewList)
            .Include(x => x.VoteList)
            .Include(x => x.SaveList)
            .Where(x => x.Id == userId)
            .FirstOrDefault();

        List<Article> query = new();

        if (
            findUser is null
            || findUser.IsFrozen is true
            || findUser.CategoryFollowedList.Count is 0
        )
        {
            query = _context.Articles
                .Where(c => !c.IsDeleted)
                .Include(x => x.Categories)
                .Include(x => x.Flag)
                .Include(x => x.Comments)
                .ToList();
        }
        else
        {
            foreach (var item in findUser.CategoryFollowedList)
            {
                var userArticles = _context.Articles
                    .Where(c => !c.IsDeleted && c.Categories.Any(x => x.Id == item))
                    .Include(x => x.Categories)
                    .Include(x => x.Flag)
                    .Include(x => x.Comments)
                    .ToList();

                if (userArticles.Count != 0)
                {
                    foreach (var article in userArticles)
                    {
                        if (!query.Any(x => x == article))
                            query.Add(article);
                    }
                }
            }
        }

        query = query.OrderByDescending(x => x.VoteCount).ToList();

        List<GetAllArticleDTO> getAllArticleDTOList = new();

        foreach (var item in query)
        {
            GetAllArticleDTO getAllArticleDTO = _mapper.Map<GetAllArticleDTO>(item);

            bool saved =
                findUser != null
                    ? findUser.SaveList.Exists(
                        x => x.ContentId == item.Id && x.ContentType == ContentType.Article
                    )
                    : false;

            bool viewed =
                findUser != null
                    ? findUser.ViewList.Exists(
                        x => x.ContentId == item.Id && x.ContentType == ContentType.Article
                    )
                    : false;

            VoteStatus voteStatus;

            if (findUser != null)
            {
                Vote vote = findUser.VoteList.FirstOrDefault(
                    x => x.ContentId == item.Id && x.ContentType == ContentType.Article
                );

                if (vote is null)
                    voteStatus = VoteStatus.UnVote;
                else
                    voteStatus = vote.Status;
            }
            else
                voteStatus = VoteStatus.UnVote;

            UserContentTitleResponse userContentTitleResponse = new() { UserId = item.UserId };

            TagDTO articleFlag = _mapper.Map<TagDTO>(item.Flag);

            getAllArticleDTO.IsSaved = saved;

            getAllArticleDTO.IsViewed = viewed;

            getAllArticleDTO.VoteDTO = new VoteDTO()
            {
                Status = voteStatus,
                VoteCount = item.VoteCount
            };

            getAllArticleDTO.UserContent = userContentTitleResponse;

            getAllArticleDTO.ArticleFlag = articleFlag;

            getAllArticleDTO.CommentCount = item.Comments.Count;

            foreach (var category in item.Categories)
            {
                TagDTO questionCategory = _mapper.Map<TagDTO>(category);
                getAllArticleDTO.ArticleCategories.Add(questionCategory);
            }

            getAllArticleDTOList.Add(getAllArticleDTO);
        }

        foreach (var item in getAllArticleDTOList)
        {
            var articleUser = _context.Users.FirstOrDefault(x => x.Id == item.UserContent.UserId);
            item.UserContent = new UserContentTitleResponse()
            {
                Name = articleUser?.Name,
                Username = articleUser?.Username,
                ProfilePhoto = articleUser?.ProfilePhoto,
                UserId = articleUser?.Id
            };
        }

        return getAllArticleDTOList;
    }

    public async Task<IEnumerable<GetLastArticlesDTO>> GetLastArticlesAsync()
    {
        var articles = _context.Articles
            .Include(a => a.Categories)
            .Include(a => a.Flag)
            .OrderByDescending(x => x.UpdateTime)
            .Take(5)
            .Select(x => x.ToLastArticleDTO())
            .ToList();

        return articles;
    }

    public async Task<PaginatedListDto<GetAllArticleDTO>> GetAllArticle(
        string searchQuery,
        List<string> categoryIdList,
        string? flagId,
        SortOptions sort,
        int page = 1
    )
    {
        IQueryable<Article> query = _context.Articles
            .Where(c => !c.IsDeleted)
            .Include(x => x.Categories)
            .Include(x => x.Flag)
            .Include(x => x.Comments);

        int pageSize = await _userRepository.GetContentPerPage();

        query = query.Skip((page - 1) * pageSize).Take(pageSize);

        if (categoryIdList != null)
        {
            foreach (var category in categoryIdList)
            {
                if (!string.IsNullOrWhiteSpace(category))
                    query = query.Where(x => x.Categories.Any(c => c.Id.Equals(category)));
            }
        }

        if (!string.IsNullOrWhiteSpace(flagId))
            query = query.Where(r => r.Flag.Id.Equals(flagId));

        if (!string.IsNullOrWhiteSpace(searchQuery))
            query = query.Where(r => r.Header.Title.ToLower().Contains(searchQuery.ToLower()));

        query = sort switch
        {
            SortOptions.Newest => query.OrderByDescending(r => r.UpdateTime),
            SortOptions.TopDay
                => query
                    .OrderByDescending(r => r.VoteCount)
                    .Where(r => r.UpdateTime >= DateTime.UtcNow.AddDays(-1)),
            SortOptions.TopWeek
                => query
                    .OrderByDescending(r => r.VoteCount)
                    .Where(r => r.UpdateTime >= DateTime.UtcNow.AddDays(-7)),
            SortOptions.TopMonth
                => query
                    .OrderByDescending(r => r.VoteCount)
                    .Where(r => r.UpdateTime >= DateTime.UtcNow.AddMonths(-1)),
            SortOptions.TopYear
                => query
                    .OrderByDescending(r => r.VoteCount)
                    .Where(r => r.UpdateTime >= DateTime.UtcNow.AddYears(-1)),
            SortOptions.TopInfinity => query.OrderByDescending(r => r.VoteCount),
            _ => query.OrderByDescending(r => _context.Saves.Count(r => r.ContentId.Equals(r.Id)))
        };

        List<GetAllArticleDTO> getAllArticleDTOList = new();

        var tokenId = _userRepository.GetClaimValue("userId");

        var findUser = _context.Users
            .Include(x => x.SaveList)
            .Include(x => x.ViewList)
            .Include(x => x.VoteList)
            .FirstOrDefault(x => x.Id == tokenId);

        foreach (var item in query)
        {
            GetAllArticleDTO getAllArticleDTO = _mapper.Map<GetAllArticleDTO>(item);

            bool saved =
                findUser != null
                    ? findUser.SaveList.Exists(
                        x => x.ContentId == item.Id && x.ContentType == ContentType.Article
                    )
                    : false;

            bool viewed =
                findUser != null
                    ? findUser.ViewList.Exists(
                        x => x.ContentId == item.Id && x.ContentType == ContentType.Article
                    )
                    : false;

            VoteStatus voteStatus;

            if (findUser != null)
            {
                Vote vote = findUser.VoteList.FirstOrDefault(
                    x => x.ContentId == item.Id && x.ContentType == ContentType.Article
                );

                if (vote is null)
                    voteStatus = VoteStatus.UnVote;
                else
                    voteStatus = vote.Status;
            }
            else
                voteStatus = VoteStatus.UnVote;

            UserContentTitleResponse userContentTitleResponse = new() { UserId = item.UserId };

            TagDTO articleFlag = _mapper.Map<TagDTO>(item.Flag);

            getAllArticleDTO.IsSaved = saved;

            getAllArticleDTO.IsViewed = viewed;

            getAllArticleDTO.VoteDTO = new VoteDTO()
            {
                Status = voteStatus,
                VoteCount = item.VoteCount
            };

            getAllArticleDTO.UserContent = userContentTitleResponse;

            getAllArticleDTO.ArticleFlag = articleFlag;

            getAllArticleDTO.CommentCount = item.Comments.Count;

            foreach (var category in item.Categories)
            {
                TagDTO questionCategory = _mapper.Map<TagDTO>(category);
                getAllArticleDTO.ArticleCategories.Add(questionCategory);
            }

            getAllArticleDTOList.Add(getAllArticleDTO);
        }

        foreach (var item in getAllArticleDTOList)
        {
            var articleUser = _context.Users.FirstOrDefault(x => x.Id == item.UserContent.UserId);
            item.UserContent = new UserContentTitleResponse()
            {
                Name = articleUser.Name,
                Username = articleUser.Username,
                ProfilePhoto = articleUser.ProfilePhoto,
                UserId = articleUser.Id
            };
        }

        int totalCount = await query.CountAsync();

        var paginationArticles = getAllArticleDTOList
            .Select(
                x =>
                    new GetAllArticleDTO()
                    {
                        Id = x.Id,
                        CommentCount = x.CommentCount,
                        ArticleCategories = x.ArticleCategories,
                        ArticleFlag = x.ArticleFlag,
                        Header = x.Header,
                        IsSaved = x.IsSaved,
                        IsViewed = x.IsViewed,
                        ReadingTime = x.ReadingTime,
                        UpdateTime = x.UpdateTime,
                        UserContent = x.UserContent,
                        VoteDTO = x.VoteDTO
                    }
            )
            .ToList();

        return new PaginatedListDto<GetAllArticleDTO>(
            paginationArticles,
            new PaginationMeta(page, pageSize, totalCount)
        );
    }

    public async Task<PaginatedListDto<GetAllArticleDTO>> GetAllSavedArticle(
        string searchQuery,
        List<string> categoryIdList,
        string? flagId,
        SortOptions sort,
        int page = 1
    )
    {
        IQueryable<Article> query = _context.Articles
            .Where(c => !c.IsDeleted)
            .Include(x => x.Categories)
            .Include(x => x.Flag)
            .Include(x => x.Comments);

        int pageSize = await _userRepository.GetContentPerPage();

        query = query.Skip((page - 1) * pageSize).Take(pageSize);

        if (categoryIdList != null)
        {
            foreach (var category in categoryIdList)
            {
                if (!string.IsNullOrWhiteSpace(category))
                    query = query.Where(x => x.Categories.Any(c => c.Id.Equals(category)));
            }
        }

        if (!string.IsNullOrWhiteSpace(flagId))
            query = query.Where(r => r.Flag.Id.Equals(flagId));

        if (!string.IsNullOrWhiteSpace(searchQuery))
            query = query.Where(r => r.Header.Title.ToLower().Contains(searchQuery.ToLower()));

        query = sort switch
        {
            SortOptions.Newest => query.OrderByDescending(r => r.UpdateTime),
            SortOptions.TopDay
                => query
                    .OrderByDescending(r => r.VoteCount)
                    .Where(r => r.UpdateTime >= DateTime.UtcNow.AddDays(-1)),
            SortOptions.TopWeek
                => query
                    .OrderByDescending(r => r.VoteCount)
                    .Where(r => r.UpdateTime >= DateTime.UtcNow.AddDays(-7)),
            SortOptions.TopMonth
                => query
                    .OrderByDescending(r => r.VoteCount)
                    .Where(r => r.UpdateTime >= DateTime.UtcNow.AddMonths(-1)),
            SortOptions.TopYear
                => query
                    .OrderByDescending(r => r.VoteCount)
                    .Where(r => r.UpdateTime >= DateTime.UtcNow.AddYears(-1)),
            SortOptions.TopInfinity => query.OrderByDescending(r => r.VoteCount),
            _ => query.OrderByDescending(r => _context.Saves.Count(r => r.ContentId.Equals(r.Id)))
        };

        List<GetAllArticleDTO> getAllArticleDTOList = new();

        var tokenId = _userRepository.GetClaimValue("userId");

        var findUser = _context.Users
            .Include(x => x.SaveList)
            .Include(x => x.ViewList)
            .Include(x => x.VoteList)
            .FirstOrDefault(x => x.Id == tokenId);

        foreach (var item in query)
        {
            GetAllArticleDTO getAllArticleDTO = _mapper.Map<GetAllArticleDTO>(item);

            bool saved =
                findUser != null
                    ? findUser.SaveList.Exists(
                        x => x.ContentId == item.Id && x.ContentType == ContentType.Article
                    )
                    : false;

            bool viewed =
                findUser != null
                    ? findUser.ViewList.Exists(
                        x => x.ContentId == item.Id && x.ContentType == ContentType.Article
                    )
                    : false;

            VoteStatus voteStatus;

            if (findUser != null)
            {
                Vote vote = findUser.VoteList.FirstOrDefault(
                    x => x.ContentId == item.Id && x.ContentType == ContentType.Article
                );

                if (vote is null)
                    voteStatus = VoteStatus.UnVote;
                else
                    voteStatus = vote.Status;
            }
            else
                voteStatus = VoteStatus.UnVote;

            UserContentTitleResponse userContentTitleResponse = new() { UserId = item.UserId };

            TagDTO articleFlag = _mapper.Map<TagDTO>(item.Flag);

            getAllArticleDTO.IsSaved = saved;

            getAllArticleDTO.IsViewed = viewed;

            getAllArticleDTO.VoteDTO = new VoteDTO()
            {
                Status = voteStatus,
                VoteCount = item.VoteCount
            };

            getAllArticleDTO.UserContent = userContentTitleResponse;

            getAllArticleDTO.ArticleFlag = articleFlag;

            getAllArticleDTO.CommentCount = item.Comments.Count;

            foreach (var category in item.Categories)
            {
                TagDTO questionCategory = _mapper.Map<TagDTO>(category);
                getAllArticleDTO.ArticleCategories.Add(questionCategory);
            }

            getAllArticleDTOList.Add(getAllArticleDTO);
        }

        foreach (var item in getAllArticleDTOList)
        {
            var articleUser = _context.Users.FirstOrDefault(x => x.Id == item.UserContent.UserId);
            item.UserContent = new UserContentTitleResponse()
            {
                Name = articleUser.Name,
                Username = articleUser.Username,
                ProfilePhoto = articleUser.ProfilePhoto,
                UserId = articleUser.Id
            };
        }

        int totalCount = await query.CountAsync();

        var paginationArticles = getAllArticleDTOList
            .Select(
                x =>
                    new GetAllArticleDTO()
                    {
                        Id = x.Id,
                        CommentCount = x.CommentCount,
                        ArticleCategories = x.ArticleCategories,
                        ArticleFlag = x.ArticleFlag,
                        Header = x.Header,
                        IsSaved = x.IsSaved,
                        IsViewed = x.IsViewed,
                        ReadingTime = x.ReadingTime,
                        UpdateTime = x.UpdateTime,
                        UserContent = x.UserContent,
                        VoteDTO = x.VoteDTO
                    }
            )
            .ToList();

        return new PaginatedListDto<GetAllArticleDTO>(
            paginationArticles,
            new PaginationMeta(page, pageSize, totalCount)
        );
    }

    public async Task<string> InsertArticle(
        Article article,
        List<string> categoryIdList,
        string flagId
    )
    {
        try
        {
            var categories = _context.Categories.Where(c => categoryIdList.Contains(c.Id)).ToList();

            var articleFlag = _context.ArticleFlags.FirstOrDefault(x => x.Id == flagId);

            if (articleFlag is null)
                return "0";

            if (categories.Count is 0)
                return "1";

            if (articleFlag != null)
            {
                articleFlag.UseCount++;
                article.Flag = articleFlag;
            }

            if (categories.Count != 0)
            {
                foreach (var item in categories)
                {
                    item.UseCount++;
                }
                article.Categories = categories;
            }

            if (!string.IsNullOrWhiteSpace(article.Header.Image))
                article.Header.Image = await _azureBlobStorage.UploadCoverImage(
                    article.Header.Image,
                    article.Id
                );

            var articleDetails = new ArticleDetails
            {
                ArticleId = article.Id,
                ArticleOwnerId = article.UserId,
            };

            _context.Articles.Add(article);

            await _context.SaveChangesAsync();

            await _notificationRepository.PostNotificationAsync(
                new()
                {
                    Type = NotificationType.Article,
                    AdditionalDetails = JsonConvert.SerializeObject(articleDetails)
                }
            );

            await _userRepository.AddBudsAsync(
                ContentType.Article,
                article.Id,
                BudsActionType.PublishArticleToOwner
            );

            return article.Id;
        }
        catch (Exception)
        {
            return "-1";
        }
    }

    public async Task<int> UpdateArticle(UpdateArticleDTO updateArticleDTO, string userId)
    {
        try
        {
            var findArticle = _context.Articles
                .Include(x => x.Categories)
                .Include(x => x.Flag)
                .FirstOrDefault(x => x.Id == updateArticleDTO.Id);

            if (findArticle is null)
                return 0;

            if (findArticle.UserId != userId)
                return 0;

            if (
                updateArticleDTO.Header is null
                && updateArticleDTO.Body is null
                && updateArticleDTO.FlagId is null
                && updateArticleDTO.ReadingTime is 0
                && updateArticleDTO.CategoryIdList.Count is 0
            )
                return 1;

            if (!string.IsNullOrWhiteSpace(updateArticleDTO.Body))
                findArticle.Body = updateArticleDTO.Body;

            if (!string.IsNullOrWhiteSpace(updateArticleDTO.FlagId))
            {
                var articleFlag = _context.ArticleFlags.FirstOrDefault(
                    x => x.Id == updateArticleDTO.FlagId
                );
                if (articleFlag != null)
                {
                    findArticle.Flag.UseCount--;
                    articleFlag.UseCount++;
                    findArticle.Flag = articleFlag;
                }
            }

            if (updateArticleDTO.ReadingTime != 0)
                findArticle.ReadingTime = updateArticleDTO.ReadingTime;

            if (updateArticleDTO.Header != null)
            {
                if (!string.IsNullOrWhiteSpace(updateArticleDTO.Header.Title))
                    findArticle.Header.Title = updateArticleDTO.Header.Title;

                if (
                    !string.IsNullOrWhiteSpace(updateArticleDTO.Header.Image)
                    && !updateArticleDTO.Header.Image.StartsWith("https://")
                )
                    if (string.IsNullOrWhiteSpace(findArticle.Header.Image))
                    {
                        var imageLink = await _azureBlobStorage.UploadCoverImage(
                            updateArticleDTO.Header.Image,
                            updateArticleDTO.Id
                        );

                        findArticle.Header.Image = imageLink;
                    }
                    else
                    {
                        findArticle.Header.Image = await _azureBlobStorage.UpdateCoverImage(
                            updateArticleDTO.Header.Image,
                            findArticle.Header.Image,
                            findArticle.Id
                        );
                    }
            }

            if (updateArticleDTO.CategoryIdList.Count > 0)
            {
                foreach (var item in findArticle.Categories)
                {
                    item.UseCount--;
                }

                findArticle.Categories.RemoveAll(item => true);

                foreach (var item in updateArticleDTO.CategoryIdList)
                {
                    var findCategoryForArticle = _context.Categories.FirstOrDefault(
                        x => x.Id == item
                    );

                    if (findCategoryForArticle != null)
                    {
                        findArticle.Categories.Add(findCategoryForArticle);
                        findCategoryForArticle.UseCount++;
                    }
                }
            }

            if (updateArticleDTO != null)
                findArticle.UpdateTime = DateTime.UtcNow;

            _context.Articles.Update(findArticle);

            await _context.SaveChangesAsync();

            return 1;
        }
        catch (Exception)
        {
            return -1;
        }
    }

    public async Task<int> DeleteArticle(string articleId, string userId)
    {
        try
        {
            var findArticle = _context.Articles
                .Include(x => x.Flag)
                .Include(x => x.Categories)
                .FirstOrDefault(x => x.Id == articleId);

            if (findArticle is null)
                return 0;

            if (findArticle.UserId != userId)
                return 0;

            foreach (var item in findArticle.Categories)
            {
                item.UseCount--;
            }

            findArticle.Categories.RemoveAll(item => true);

            findArticle.Flag.UseCount--;

            findArticle.IsDeleted = true;

            findArticle.UpdateTime = DateTime.UtcNow;

            await _context.SaveChangesAsync();

            return 1;
        }
        catch (Exception)
        {
            return -1;
        }
    }

    public async Task<GetByIdArticleDTO> GetByIdArticle(string articleId, string userId)
    {
        try
        {
            var article = _context.Articles
                .Include(x => x.Categories)
                .Include(x => x.Flag)
                .Include(x => x.Comments)
                .FirstOrDefault(x => x.Id == articleId);

            var user = _context.Users
                .Include(x => x.SaveList)
                .Include(x => x.ViewList)
                .Include(x => x.VoteList)
                .FirstOrDefault(x => x.Id == userId);

            var articleUser = _context.Users.FirstOrDefault(x => x.Id == article.UserId);

            if (article is null)
                return null;

            List<GetArticleCommentDTO> commentDTOs = new List<GetArticleCommentDTO>();

            var commentList = _context.Comments
                .Where(x => x.ArticleId == articleId)
                .OrderByDescending(x => x.UpdateTime)
                .ToList();

            foreach (var comment in commentList)
            {
                var commenterUser = _context.Users
                    .Include(x => x.VoteList)
                    .FirstOrDefault(x => x.Id == comment.UserId);

                VoteStatus commentVoteStatus;

                Vote commentVote =
                    user != null
                        ? user.VoteList.FirstOrDefault(
                            x => x.ContentId == comment.Id && x.ContentType == ContentType.Comment
                        )
                        : null;

                if (commentVote is null)
                    commentVoteStatus = VoteStatus.UnVote;
                else
                    commentVoteStatus = commentVote.Status;

                commentDTOs.Add(
                    new GetArticleCommentDTO()
                    {
                        Id = comment.Id,
                        Body = comment.Body,
                        UpdateTime = comment.UpdateTime,
                        UserContent = new UserContentTitleResponse()
                        {
                            UserId = commenterUser.Id,
                            Name = commenterUser.Name,
                            Username = commenterUser.Username,
                            ProfilePhoto = commenterUser.ProfilePhoto,
                        },
                        VoteDTO = new VoteDTO()
                        {
                            VoteCount = comment.VoteCount,
                            Status = commentVoteStatus
                        }
                    }
                );
            }

            //GetByIdArticleDTO getByIdArticleDTO = _mapper.Map<GetByIdArticleDTO>(article);


            List<TagDTO> articleCategories = new();

            foreach (var item in article.Categories)
            {
                articleCategories.Add(
                    new TagDTO
                    {
                        AccentColor = item.AccentColor,
                        Id = item.Id,
                        Title = item.Title
                    }
                );
            }

            GetByIdArticleDTO getByIdArticleDTO = new GetByIdArticleDTO
            {
                Id = article.Id,
                Body = article.Body,
                Header = article.Header,
                UpdateTime = article.UpdateTime,
                ReadingTime = article.ReadingTime,
                Categories = articleCategories
            };

            bool saved =
                user != null
                    ? user.SaveList.Exists(
                        x => x.ContentId == article.Id && x.ContentType == ContentType.Article
                    )
                    : false;

            bool viewed =
                user != null
                    ? user.ViewList.Exists(
                        x => x.ContentId == article.Id && x.ContentType == ContentType.Article
                    )
                    : false;

            if (user is not null && !viewed)
            {
                await _userRepository.AddBudsAsync(
                    ContentType.Article,
                    articleId,
                    BudsActionType.ViewArticleToUser,
                    user.Id
                );
                await _userRepository.AddBudsAsync(
                    ContentType.Article,
                    articleId,
                    BudsActionType.ViewArticleToOwner,
                    articleUser.Id
                );
            }

            int saveCount = _context.Saves
                .Where(x => x.ContentId == articleId && x.ContentType == ContentType.Article)
                .Count();

            VoteStatus voteStatus;

            if (user is not null)
            {
                Vote vote = user.VoteList.FirstOrDefault(
                    x => x.ContentId == article.Id && x.ContentType == ContentType.Article
                );

                if (vote is null)
                    voteStatus = VoteStatus.UnVote;
                else
                    voteStatus = vote.Status;
            }
            else
                voteStatus = VoteStatus.UnVote;

            bool isFollow =
                user != null
                    ? _context.Follows.Any(
                        x => x.FollowerId == user.Id && x.FollowingId == articleUser.Id
                    )
                    : false;

            var userPreview = new UserPreviewResponse()
            {
                Id = articleUser.Id,
                Bio = articleUser.PersonalInfo?.Bio,
                Brand = articleUser.Brand,
                DisplayEmail = articleUser.Email,
                Education = articleUser.PersonalInfo?.Education,
                IsUserFollowed = isFollow,
                Joined = articleUser.JoinedTime,
                Location = articleUser.PersonalInfo?.Location,
                Name = articleUser.Name,
                ProfilePhoto = articleUser.ProfilePhoto,
                Username = articleUser.Username,
                Work = articleUser.PersonalInfo?.Work
            };

            TagDTO articleFlag = _mapper.Map<TagDTO>(article.Flag);

            getByIdArticleDTO.IsSaved = saved;

            getByIdArticleDTO.IsViewed = viewed;

            getByIdArticleDTO.SaveCount = saveCount;

            getByIdArticleDTO.Vote = new VoteDTO()
            {
                Status = voteStatus,
                VoteCount = article.VoteCount
            };

            getByIdArticleDTO.UserPreview = userPreview;

            getByIdArticleDTO.ArticleFlag = articleFlag;

            getByIdArticleDTO.Comments = commentDTOs;

            getByIdArticleDTO.CommentCount = commentDTOs.Count;

            if (user is not null)
            {
                if (
                    !user.ViewList.Exists(
                        x => x.ContentId == articleId && x.ContentType == ContentType.Article
                    )
                )
                {
                    user.ViewList.Add(
                        new View()
                        {
                            Id = IDGeneratorService.GetShortUniqueId(),
                            ContentId = articleId,
                            ContentType = ContentType.Article,
                            IsDeleted = false,
                            UserId = userId
                        }
                    );

                    article.ViewCount++;
                }
            }

            _context.Articles.Update(article);

            await _context.SaveChangesAsync();

            return getByIdArticleDTO;
        }
        catch (Exception)
        {
            return null;
        }
    }

    public async Task<int> SaveArticle(string articleId, string tokenId)
    {
        var user = _context.Users.Include(x => x.SaveList).FirstOrDefault(x => x.Id == tokenId);

        if (user is null)
            return -1;

        try
        {
            var findArticle = _context.Articles.FirstOrDefault(x => x.Id == articleId);

            if (findArticle is null)
                return 0;

            var save = new Save()
            {
                Id = IDGeneratorService.GetShortUniqueId(),
                UserId = tokenId,
                ContentId = articleId,
                ContentType = ContentType.Article
            };

            if (user.SaveList.Count is 0)
                user.SaveList.Add(save);
            else
            {
                var findSave = _context.Saves.FirstOrDefault(
                    x => x.ContentId == articleId && x.ContentType == ContentType.Article
                );

                if (findSave != null)
                    _context.Saves.Remove(findSave);
                else
                    user.SaveList.Add(save);
            }

            _context.Users.Update(user);

            await _context.SaveChangesAsync();

            return 1;
        }
        catch (Exception)
        {
            return -1;
        }
    }

    public async Task<int> VoteArticle(VoteStatus status, string articleId, string tokenId)
    {
        var user = _context.Users.Include(x => x.VoteList).FirstOrDefault(x => x.Id == tokenId);

        if (user is null)
            return -1;

        try
        {
            var findArticle = _context.Articles.FirstOrDefault(x => x.Id == articleId);

            if (findArticle is null)
                return 0;

            var voteEntity = user.VoteList.FirstOrDefault(
                s => s.ContentType == ContentType.Article && s.ContentId.Equals(articleId)
            );

            if (voteEntity is null)
            {
                Vote vote =
                    new()
                    {
                        Id = IDGeneratorService.GetShortUniqueId(),
                        ContentId = articleId,
                        UserId = tokenId,
                        ContentType = ContentType.Article,
                        Status = status
                    };

                findArticle.VoteCount += status == VoteStatus.UpVote ? 1 : -1;
                user.VoteList.Add(vote);
            }
            else
            {
                if (status != voteEntity.Status)
                {
                    if (voteEntity.Status == VoteStatus.DownVote)
                        findArticle.VoteCount += 1;
                    else if (voteEntity.Status == VoteStatus.UpVote)
                        findArticle.VoteCount -= 1;
                    voteEntity.Status = status;
                    findArticle.VoteCount += status == VoteStatus.DownVote ? -1 : 1;
                    _context.Update(voteEntity);
                }
                else
                {
                    findArticle.VoteCount += status == VoteStatus.DownVote ? 1 : -1;
                    user.VoteList.Remove(voteEntity);
                }
            }

            _context.Users.Update(user);

            _context.Articles.Update(findArticle);

            await _context.SaveChangesAsync();

            return 1;
        }
        catch (Exception)
        {
            return -1;
        }
    }

    public async Task<int> GetAllArticlesCount()
    {
        return await _context.Articles.CountAsync();
    }

    public async Task<PaginatedListDto<GetAllArticleDTO>> GetAllSavedArticleForUser(
        string searchQuery,
        List<string> categoryIdList,
        string flagId,
        SortOptions sort,
        int page = 1
    )
    {
        var tokenId = _userRepository.GetClaimValue("userId");

        var findUser = _context.Users
            .Include(x => x.SaveList)
            .Include(x => x.ViewList)
            .Include(x => x.VoteList)
            .FirstOrDefault(x => x.Id == tokenId);

        if (findUser != null || findUser.IsFrozen != true)
        {
            if (findUser.SaveList.Count != 0)
            {
                if (!findUser.SaveList.Any(x => x.ContentType == ContentType.Article))
                    return null;
            }
            else
                return null;
        }
        else
            return null;

        IQueryable<Article> query = _context.Articles
            .Where(c => !c.IsDeleted)
            .Include(x => x.Categories)
            .Include(x => x.Flag)
            .Include(x => x.Comments);

        int pageSize = await _userRepository.GetContentPerPage();

        query = query.Skip((page - 1) * pageSize).Take(pageSize);

        var savedArticle = findUser.SaveList
            .Where(x => x.ContentType == ContentType.Article)
            .ToList();
        var savedArticleIds = savedArticle.Select(item => item.ContentId).ToList();
        query = query.Where(x => savedArticleIds.Contains(x.Id));

        if (categoryIdList != null)
        {
            foreach (var category in categoryIdList)
            {
                if (!string.IsNullOrWhiteSpace(category))
                    query = query.Where(x => x.Categories.Any(c => c.Id.Equals(category)));
            }
        }

        if (!string.IsNullOrWhiteSpace(flagId))
            query = query.Where(r => r.Flag.Id.Equals(flagId));

        if (!string.IsNullOrWhiteSpace(searchQuery))
            query = query.Where(r => r.Header.Title.ToLower().Contains(searchQuery.ToLower()));

        query = sort switch
        {
            SortOptions.Newest => query.OrderByDescending(r => r.UpdateTime),
            SortOptions.TopDay
                => query
                    .OrderByDescending(r => r.VoteCount)
                    .Where(r => r.UpdateTime >= DateTime.UtcNow.AddDays(-1)),
            SortOptions.TopWeek
                => query
                    .OrderByDescending(r => r.VoteCount)
                    .Where(r => r.UpdateTime >= DateTime.UtcNow.AddDays(-7)),
            SortOptions.TopMonth
                => query
                    .OrderByDescending(r => r.VoteCount)
                    .Where(r => r.UpdateTime >= DateTime.UtcNow.AddMonths(-1)),
            SortOptions.TopYear
                => query
                    .OrderByDescending(r => r.VoteCount)
                    .Where(r => r.UpdateTime >= DateTime.UtcNow.AddYears(-1)),
            SortOptions.TopInfinity => query.OrderByDescending(r => r.VoteCount),
            _ => query.OrderByDescending(r => _context.Saves.Count(r => r.ContentId.Equals(r.Id)))
        };

        List<GetAllArticleDTO> getAllArticleDTOList = new();

        query = query.OrderByDescending(x => x.VoteCount);

        foreach (var item in query)
        {
            GetAllArticleDTO getAllArticleDTO = _mapper.Map<GetAllArticleDTO>(item);

            bool saved =
                findUser != null
                    ? findUser.SaveList.Exists(
                        x => x.ContentId == item.Id && x.ContentType == ContentType.Article
                    )
                    : false;

            bool viewed =
                findUser != null
                    ? findUser.ViewList.Exists(
                        x => x.ContentId == item.Id && x.ContentType == ContentType.Article
                    )
                    : false;

            VoteStatus voteStatus;

            if (findUser != null)
            {
                Vote vote = findUser.VoteList.FirstOrDefault(
                    x => x.ContentId == item.Id && x.ContentType == ContentType.Article
                );

                if (vote is null)
                    voteStatus = VoteStatus.UnVote;
                else
                    voteStatus = vote.Status;
            }
            else
                voteStatus = VoteStatus.UnVote;

            UserContentTitleResponse userContentTitleResponse = new() { UserId = item.UserId };

            TagDTO articleFlag = _mapper.Map<TagDTO>(item.Flag);

            getAllArticleDTO.IsSaved = saved;

            getAllArticleDTO.IsViewed = viewed;

            getAllArticleDTO.VoteDTO = new VoteDTO()
            {
                Status = voteStatus,
                VoteCount = item.VoteCount
            };

            getAllArticleDTO.UserContent = userContentTitleResponse;

            getAllArticleDTO.ArticleFlag = articleFlag;

            getAllArticleDTO.CommentCount = item.Comments.Count;

            foreach (var category in item.Categories)
            {
                TagDTO questionCategory = _mapper.Map<TagDTO>(category);
                getAllArticleDTO.ArticleCategories.Add(questionCategory);
            }

            getAllArticleDTOList.Add(getAllArticleDTO);
        }

        foreach (var item in getAllArticleDTOList)
        {
            var articleUser = _context.Users.FirstOrDefault(x => x.Id == item.UserContent.UserId);
            item.UserContent = new UserContentTitleResponse()
            {
                Name = articleUser.Name,
                Username = articleUser.Username,
                ProfilePhoto = articleUser.ProfilePhoto,
                UserId = articleUser.Id
            };
        }

        int totalCount = await query.CountAsync();

        var paginationArticles = getAllArticleDTOList
            .Select(
                x =>
                    new GetAllArticleDTO()
                    {
                        Id = x.Id,
                        CommentCount = x.CommentCount,
                        ArticleCategories = x.ArticleCategories,
                        ArticleFlag = x.ArticleFlag,
                        Header = x.Header,
                        IsSaved = x.IsSaved,
                        IsViewed = x.IsViewed,
                        ReadingTime = x.ReadingTime,
                        UpdateTime = x.UpdateTime,
                        UserContent = x.UserContent,
                        VoteDTO = x.VoteDTO
                    }
            )
            .ToList();

        return new PaginatedListDto<GetAllArticleDTO>(
            paginationArticles,
            new PaginationMeta(page, pageSize, totalCount)
        );
    }

    public async Task<PaginatedListDto<GetAllArticleDTO>> GetAllUserArticle(
        string tokenId,
        string searchQuery,
        List<string> categoryIdList,
        string flagId,
        SortOptions sort,
        int page = 1
    )
    {
        var findUser = _context.Users
            .Include(x => x.SaveList)
            .Include(x => x.ViewList)
            .Include(x => x.VoteList)
            .FirstOrDefault(x => x.Id == tokenId);

        if (findUser is null)
            return null;
        else if (findUser.IsFrozen is true)
            return null;

        IQueryable<Article> query = _context.Articles
            .Where(c => !c.IsDeleted && c.UserId == tokenId)
            .Include(x => x.Categories)
            .Include(x => x.Flag)
            .Include(x => x.Comments);

        int pageSize = await _userRepository.GetContentPerPage();

        query = query.Skip((page - 1) * pageSize).Take(pageSize);

        if (categoryIdList != null)
        {
            foreach (var category in categoryIdList)
            {
                if (!string.IsNullOrWhiteSpace(category))
                    query = query.Where(x => x.Categories.Any(c => c.Id.Equals(category)));
            }
        }

        if (!string.IsNullOrWhiteSpace(flagId))
            query = query.Where(r => r.Flag.Id.Equals(flagId));

        if (!string.IsNullOrWhiteSpace(searchQuery))
            query = query.Where(r => r.Header.Title.ToLower().Contains(searchQuery.ToLower()));

        query = sort switch
        {
            SortOptions.Newest => query.OrderByDescending(r => r.UpdateTime),
            SortOptions.TopDay
                => query
                    .OrderByDescending(r => r.VoteCount)
                    .Where(r => r.UpdateTime >= DateTime.UtcNow.AddDays(-1)),
            SortOptions.TopWeek
                => query
                    .OrderByDescending(r => r.VoteCount)
                    .Where(r => r.UpdateTime >= DateTime.UtcNow.AddDays(-7)),
            SortOptions.TopMonth
                => query
                    .OrderByDescending(r => r.VoteCount)
                    .Where(r => r.UpdateTime >= DateTime.UtcNow.AddMonths(-1)),
            SortOptions.TopYear
                => query
                    .OrderByDescending(r => r.VoteCount)
                    .Where(r => r.UpdateTime >= DateTime.UtcNow.AddYears(-1)),
            SortOptions.TopInfinity => query.OrderByDescending(r => r.VoteCount),
            _ => query.OrderByDescending(r => _context.Saves.Count(r => r.ContentId.Equals(r.Id)))
        };

        List<GetAllArticleDTO> getAllArticleDTOList = new();

        query = query.OrderByDescending(x => x.VoteCount);

        foreach (var item in query)
        {
            GetAllArticleDTO getAllArticleDTO = _mapper.Map<GetAllArticleDTO>(item);

            bool saved =
                findUser != null
                    ? findUser.SaveList.Exists(
                        x => x.ContentId == item.Id && x.ContentType == ContentType.Article
                    )
                    : false;

            bool viewed =
                findUser != null
                    ? findUser.ViewList.Exists(
                        x => x.ContentId == item.Id && x.ContentType == ContentType.Article
                    )
                    : false;

            VoteStatus voteStatus;

            if (findUser != null)
            {
                Vote vote = findUser.VoteList.FirstOrDefault(
                    x => x.ContentId == item.Id && x.ContentType == ContentType.Article
                );

                if (vote is null)
                    voteStatus = VoteStatus.UnVote;
                else
                    voteStatus = vote.Status;
            }
            else
                voteStatus = VoteStatus.UnVote;

            UserContentTitleResponse userContentTitleResponse = new() { UserId = item.UserId };

            TagDTO articleFlag = _mapper.Map<TagDTO>(item.Flag);

            getAllArticleDTO.IsSaved = saved;

            getAllArticleDTO.IsViewed = viewed;

            getAllArticleDTO.VoteDTO = new VoteDTO()
            {
                Status = voteStatus,
                VoteCount = item.VoteCount
            };

            getAllArticleDTO.UserContent = userContentTitleResponse;

            getAllArticleDTO.ArticleFlag = articleFlag;

            getAllArticleDTO.CommentCount = item.Comments.Count;

            foreach (var category in item.Categories)
            {
                TagDTO questionCategory = _mapper.Map<TagDTO>(category);
                getAllArticleDTO.ArticleCategories.Add(questionCategory);
            }

            getAllArticleDTOList.Add(getAllArticleDTO);
        }

        foreach (var item in getAllArticleDTOList)
        {
            var articleUser = _context.Users.FirstOrDefault(x => x.Id == item.UserContent.UserId);
            item.UserContent = new UserContentTitleResponse()
            {
                Name = articleUser.Name,
                Username = articleUser.Username,
                ProfilePhoto = articleUser.ProfilePhoto,
                UserId = articleUser.Id
            };
        }

        int totalCount = await query.CountAsync();

        var paginationArticles = getAllArticleDTOList
            .Select(
                x =>
                    new GetAllArticleDTO()
                    {
                        Id = x.Id,
                        CommentCount = x.CommentCount,
                        ArticleCategories = x.ArticleCategories,
                        ArticleFlag = x.ArticleFlag,
                        Header = x.Header,
                        IsSaved = x.IsSaved,
                        IsViewed = x.IsViewed,
                        ReadingTime = x.ReadingTime,
                        UpdateTime = x.UpdateTime,
                        UserContent = x.UserContent,
                        VoteDTO = x.VoteDTO
                    }
            )
            .ToList();

        return new PaginatedListDto<GetAllArticleDTO>(
            paginationArticles,
            new PaginationMeta(page, pageSize, totalCount)
        );
    }

    public async Task<List<GetAllArticleDTO>> GetFlow()
    {
        var tokenId = _userRepository.GetClaimValue("userId");

        var findUser = _context.Users
            .Include(x => x.VoteList)
            .Include(x => x.WatchList)
            .Include(x => x.ViewList)
            .Include(x => x.VoteList)
            .Where(x => x.Id == tokenId)
            .FirstOrDefault();

        var followingList = _context.Follows.Where(x => x.FollowerId == tokenId).ToList();

        if (followingList.Count is 0)
            return null;

        List<User> followingUserList = new();

        foreach (var item in followingList)
        {
            var followingUser = _context.Users.FirstOrDefault(x => x.Id == item.FollowingId);
            if (followingUser.IsFrozen is true)
                continue;
            if (followingUser != null)
                followingUserList.Add(followingUser);
        }

        List<Article> query = new();

        foreach (var item in followingUserList)
        {
            var userArticles = _context.Articles
                .Where(x => x.UserId == item.Id)
                .Where(r => r.UpdateTime >= DateTime.UtcNow.AddDays(-3))
                .Where(c => !c.IsDeleted)
                .Include(x => x.Categories)
                .Include(x => x.Flag)
                .Include(x => x.Comments)
                .OrderByDescending(x => x.UpdateTime)
                .Take(5)
                .ToList();

            if (userArticles.Count != 0)
                query.AddRange(userArticles);
        }

        List<GetAllArticleDTO> getAllArticleDTOList = new();

        query = query.Take(20).OrderByDescending(x => x.UpdateTime).ToList();

        foreach (var item in query)
        {
            GetAllArticleDTO getAllArticleDTO = _mapper.Map<GetAllArticleDTO>(item);

            bool saved =
                findUser != null
                    ? findUser.SaveList.Exists(
                        x => x.ContentId == item.Id && x.ContentType == ContentType.Article
                    )
                    : false;

            bool viewed =
                findUser != null
                    ? findUser.ViewList.Exists(
                        x => x.ContentId == item.Id && x.ContentType == ContentType.Article
                    )
                    : false;

            VoteStatus voteStatus;

            if (findUser != null)
            {
                Vote vote = findUser.VoteList.FirstOrDefault(
                    x => x.ContentId == item.Id && x.ContentType == ContentType.Article
                );

                if (vote is null)
                    voteStatus = VoteStatus.UnVote;
                else
                    voteStatus = vote.Status;
            }
            else
                voteStatus = VoteStatus.UnVote;

            UserContentTitleResponse userContentTitleResponse = new() { UserId = item.UserId };

            TagDTO articleFlag = _mapper.Map<TagDTO>(item.Flag);

            getAllArticleDTO.IsSaved = saved;

            getAllArticleDTO.IsViewed = viewed;

            getAllArticleDTO.VoteDTO = new VoteDTO()
            {
                Status = voteStatus,
                VoteCount = item.VoteCount
            };

            getAllArticleDTO.UserContent = userContentTitleResponse;

            getAllArticleDTO.ArticleFlag = articleFlag;

            getAllArticleDTO.CommentCount = item.Comments.Count;

            getAllArticleDTO.UpdateTime = item.UpdateTime;

            foreach (var category in item.Categories)
            {
                TagDTO questionCategory = _mapper.Map<TagDTO>(category);
                getAllArticleDTO.ArticleCategories.Add(questionCategory);
            }

            getAllArticleDTOList.Add(getAllArticleDTO);
        }

        foreach (var item in getAllArticleDTOList)
        {
            var articleUser = _context.Users.FirstOrDefault(x => x.Id == item.UserContent.UserId);
            item.UserContent = new UserContentTitleResponse()
            {
                Name = articleUser.Name,
                Username = articleUser.Username,
                ProfilePhoto = articleUser.ProfilePhoto,
                UserId = articleUser.Id
            };
        }

        return getAllArticleDTOList;
    }

    public async Task<List<ArticleMoreFromDTO>> GetArticleMoreFrom(string tokenId)
    {
        var findUser = _context.Users
            .Include(x => x.VoteList)
            .Include(x => x.WatchList)
            .Include(x => x.ViewList)
            .Include(x => x.VoteList)
            .Where(x => x.Id == tokenId)
            .FirstOrDefault();

        if (findUser is null)
            return null;
        else if (findUser.IsFrozen is true)
            return null;

        List<Article> articles = _context.Articles
            .Where(c => !c.IsDeleted && c.UserId == tokenId)
            .Include(x => x.Categories)
            .Include(x => x.Flag)
            .Include(x => x.Comments)
            .OrderByDescending(x => x.VoteCount)
            .Take(3)
            .ToList();

        List<ArticleMoreFromDTO> articleMoreFromDTOs = new();

        foreach (var item in articles)
        {
            List<MoreFromTagDTO> moreFromCategories = new();

            foreach (var category in item.Categories)
            {
                moreFromCategories.Add(new MoreFromTagDTO { Title = category.Title, });
            }

            articleMoreFromDTOs.Add(
                new ArticleMoreFromDTO
                {
                    Id = item.Id,
                    Title = item.Header.Title,
                    Flag = new MoreFromTagDTO { Title = item.Flag.Title },
                    Categories = moreFromCategories
                }
            );
        }

        return articleMoreFromDTOs;
    }
}
