using AppDomain.Interfaces;
using Infrastructure.Persistence;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Text;
using Microsoft.EntityFrameworkCore;
using AppDomain.Common.Config;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Infrastructure.Services;
using Quartz;

namespace Infrastructure;

public static class DependencyInjection
{
    public static IServiceCollection AddInfrastructure(
        this IServiceCollection services,
        IConfiguration configuration
    )
    {
        services.AddSingleton<DapperContext>();

        services.AddDbContext<ClubrickDbContext>(
            options =>
                options.UseNpgsql(
                    configuration.GetConnectionString("DefaultConnection"),
                    b => b.MigrationsAssembly("WebApi")
                )
        );

        services.AddSingleton<IAzureBlobStorageService, AzureBlobStorageService>();
        services.AddScoped<IUserRepository, UserRepository>();
        services.AddScoped<IAuthRepository, AuthRepository>();
        services.AddScoped<ICategoryRepository, CategoryRepository>();
        services.AddScoped<IArticleFlagRepository, ArticleFlagRepository>();
        services.AddScoped<IResourceRepository, ResourceRepository>();
        services.AddScoped<IResourceFlagRepository, ResourceFlagRepository>();
        services.AddScoped<IAnswerRepository, AnswerRepository>();
        services.AddScoped<IQuestionRepository, QuestionRepository>();
        services.AddScoped<IArticleRepository, ArticleRepository>();
        services.AddScoped<IReportRepository, ReportRepository>();
        services.AddScoped<ICommentRepository, CommentRepository>();
        services.AddScoped<INotificationRepository, NotificationRepository>();
        services.AddScoped<IAdminRepository, AdminRepository>();
        services.AddScoped<IProductRepository, ProductRepository>();
        services.AddScoped<IModeratorRepository, ModeratorRepository>();
        services.AddScoped<IReviewRepository, ReviewRepository>();

        var otpConfig = new EmailConfig();
        configuration.GetSection("OTP Config").Bind(otpConfig);
        services.AddSingleton(otpConfig);
        services.AddTransient<IEmailService, EmailService>(); //default AddScopped

        services.AddScoped<ICacheService, CacheService>();

        var bcryptConfig = new BCryptConfig();
        configuration.GetSection("BCrypt").Bind(bcryptConfig);
        services.AddSingleton(bcryptConfig);
        services.AddSingleton<ICryptService, CryptService>();

        var jwtConfig = new JwtConfig();
        configuration.GetSection("JWT").Bind(jwtConfig);
        services.AddSingleton(jwtConfig);
        services.AddSingleton<ITokenService, TokenService>();

        services.AddQuartz(options =>
        {
            options.UseMicrosoftDependencyInjectionJobFactory();
        });

        services.AddQuartzHostedService(options =>
        {
            options.WaitForJobsToComplete = true;
        });

        services.ConfigureOptions<QuartzServices>();

        return services;
    }

    /// <summary>
    /// Adds Swagger documentation generation to the service collection.
    /// </summary>
    /// <param name="services">The <see cref="IServiceCollection"/> instance.</param>
    /// <returns>The updated <see cref="IServiceCollection"/> instance.</returns>
    public static IServiceCollection AddSwagger(this IServiceCollection services)
    {
        _ = services.AddSwaggerGen(setup =>
        {
            setup.SwaggerDoc("v1", new OpenApiInfo { Title = "Api", Version = "v1", });

            setup.AddSecurityDefinition(
                "Bearer",
                new OpenApiSecurityScheme()
                {
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Description =
                        "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 2NG5Ff@t8ze^\""
                }
            );
            setup.AddSecurityRequirement(
                new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        Array.Empty<string>()
                    }
                }
            );

            var filePath = Path.Combine(AppContext.BaseDirectory, "project.xml");
            setup.IncludeXmlComments(filePath);
        });

        return services;
    }

    /// <summary>
    /// Adds configuration objects to the service collection based on values from the application's configuration file.
    /// </summary>
    /// <param name="services">The <see cref="IServiceCollection"/> instance.</param>
    /// <param name="configuration">The <see cref="IConfiguration"/> instance.</param>
    /// <returns>The updated <see cref="IServiceCollection"/> instance.</returns>
    public static IServiceCollection AddConfigs(
        this IServiceCollection services,
        IConfiguration configuration
    )
    {
        /* Config Jwt  */
        var jwtConfig = new JwtConfig();
        configuration.GetSection("JWT").Bind(jwtConfig);
        services.AddSingleton(jwtConfig);

        /* Config BCrypt */
        var bcryptConfig = new BCryptConfig();
        configuration.GetSection("BCrypt").Bind(bcryptConfig);
        services.AddSingleton(bcryptConfig);

        /* Config Azure */
        var azureConfig = new AzureConfig();
        configuration.GetSection("AzureConfig").Bind(azureConfig);
        services.AddSingleton(azureConfig);

        /* Config UserDefaults */
        var userDefaults = new UserDefaults();
        configuration.GetSection("UserDefaults").Bind(userDefaults);
        services.AddSingleton(userDefaults);

        return services;
    }

    /// <summary>
    /// Adds authentication to the service collection.
    /// </summary>
    /// <param name="services">The <see cref="IServiceCollection"/> instance.</param>
    /// <param name="configuration">for Jwt Token</param>
    /// <returns>The updated <see cref="IServiceCollection"/> instance.</returns>
    public static IServiceCollection AuthenticationAndAuthorization(
        this IServiceCollection services,
        IConfiguration configuration
    )
    {
        var jwtConfig = new JwtConfig();
        configuration.GetSection("JWT").Bind(jwtConfig);

        services
            .AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddCookie(x => x.Cookie.Name = "authToken")
            .AddJwtBearer(
                "Bearer",
                options =>
                {
                    options.SaveToken = true;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ClockSkew = TimeSpan.Zero,
                        ValidIssuer = jwtConfig.Issuer,
                        ValidAudience = jwtConfig.Audience,
                        IssuerSigningKey = new SymmetricSecurityKey(
                            Encoding.UTF8.GetBytes(jwtConfig.Secret)
                        ),
                    };
                    options.Events = new JwtBearerEvents
                    {
                        OnMessageReceived = context =>
                        {
                            context.Token = context.Request.Cookies["authToken"];
                            return Task.CompletedTask;
                        }
                    };
                }
            );
        services.AddAuthorization();

        return services;
    }
}