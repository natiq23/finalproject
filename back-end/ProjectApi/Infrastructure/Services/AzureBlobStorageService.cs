﻿using ActiveUp.Net.Security.OpenPGP.Packets;
using AppDomain.Common.Config;
using AppDomain.Entities.ContentRelated;
using AppDomain.Interfaces;
using Application.Services;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;

namespace Infrastructure.Services;

/// <summary>
/// BLobStorage Service for Upload Images to Azure BLob Storage
/// </summary>
public class AzureBlobStorageService : IAzureBlobStorageService
{
    private readonly AzureConfig _azureConfig;

    private readonly BlobServiceClient _blobServiceClient;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="azureConfig"></param>
    public AzureBlobStorageService(AzureConfig azureConfig)
    {
        _azureConfig = azureConfig;
        _blobServiceClient = new(_azureConfig.ConnectionString);
    }

    /// <inheritdoc/>
    public async Task<string> UpdateAvatarAsync(string imageData, string blobUrl, string userId)
    {
        BlobContainerClient avatarContainer = _blobServiceClient.GetBlobContainerClient(
            _azureConfig.AvatarContainerName
        );
        try
        {
            string imageType = GetImageTypeFromBase64(imageData);

            BinaryData binaryData = new(RemovePrefixFromBase64(imageData));

            string blobName = FindBlobName(blobUrl);

            BlobClient existAvatarBlobClient = avatarContainer.GetBlobClient(blobName);
            await existAvatarBlobClient.DeleteIfExistsAsync(DeleteSnapshotsOption.IncludeSnapshots);

            return await UploadAvatarAsync(imageData, userId);
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <inheritdoc/>
    public async Task<string> UploadAvatarAsync(string imageData, string userId)
    {
        BlobContainerClient avatarContainer = _blobServiceClient.GetBlobContainerClient(
            _azureConfig.AvatarContainerName
        );
        try
        {
            string imageType = GetImageTypeFromBase64(imageData);

            BinaryData binaryData = new(RemovePrefixFromBase64(imageData));

            string blobName =
                $"{DateTimeOffset.Now.ToString("yyyy/MM/dd/HH/mm/ss").Replace('/', '_')}{userId}.{imageType}";

            BlobClient avatarBlobClient = avatarContainer.GetBlobClient(blobName);

            await avatarBlobClient.UploadAsync(binaryData);
            string avatarImageUrl = avatarBlobClient.Uri.ToString();

            return avatarImageUrl;
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <inheritdoc/>
    public async Task<string> UploadTagIconAsync(string iconData, string tagId)
    {
        BlobContainerClient tagsContainer = _blobServiceClient.GetBlobContainerClient(
            _azureConfig.TagsContainerName
        );
        try
        {
            string imageType = GetImageTypeFromBase64(iconData);

            BinaryData binaryData = new(RemovePrefixFromBase64(iconData));
            string blobName =
                $"{DateTimeOffset.Now.ToString("yyyy/MM/dd/HH/mm/ss").Replace('/', '_')}{tagId}.{imageType}";

            BlobClient tagsBlobClient = tagsContainer.GetBlobClient(blobName);

            await tagsBlobClient.UploadAsync(binaryData);
            string tagIconUrl = tagsBlobClient.Uri.ToString();

            return tagIconUrl;
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <inheritdoc/>
    public async Task<string> UpdateTagIconAsync(string iconData, string tagUrl, string tagId)
    {
        BlobContainerClient tagsContainer = _blobServiceClient.GetBlobContainerClient(
            _azureConfig.TagsContainerName
        );
        try
        {
            string blobName = FindBlobName(tagUrl);

            BlobClient existTagsBlobClient = tagsContainer.GetBlobClient(blobName);
            await existTagsBlobClient.DeleteIfExistsAsync(DeleteSnapshotsOption.IncludeSnapshots);

            return await UploadTagIconAsync(iconData, tagId);
        }
        catch (Exception)
        {
            throw;
        }
    }

    /// <inheritdoc/>
    public async Task DeleteTagIconAsync(string tagUrl)
    {
        BlobContainerClient tagsContainer = _blobServiceClient.GetBlobContainerClient(
            _azureConfig.TagsContainerName
        );
        try
        {
            string blobName = FindBlobName(tagUrl);

            BlobClient tagsBlobClient = tagsContainer.GetBlobClient(blobName);

            await tagsBlobClient.DeleteIfExistsAsync();
            string tagIconUrl = tagsBlobClient.Uri.ToString();
        }
        catch (Exception)
        {
            throw;
        }
    }

    public async Task<string> UploadCoverImage(string imageData, string articleId)
    {
        BlobContainerClient imageContainer = _blobServiceClient.GetBlobContainerClient(
            _azureConfig.CoverImages
        );
        try
        {
            string imageType = GetImageTypeFromBase64(imageData);

            BinaryData binaryData = new(RemovePrefixFromBase64(imageData));
            string blobName =
                $"{DateTimeOffset.Now.ToString("yyyy/MM/dd/HH/mm/ss").Replace('/', '_')}{articleId}.{imageType}";

            BlobClient imageBlobClient = imageContainer.GetBlobClient(blobName);

            await imageBlobClient.UploadAsync(binaryData);
            string imageIconUrl = imageBlobClient.Uri.ToString();

            return imageIconUrl;
        }
        catch (Exception)
        {
            throw;
        }
    }

    public async Task<string> UpdateCoverImage(string imageData, string blobUrl, string articleId)
    {
        BlobContainerClient imageContainer = _blobServiceClient.GetBlobContainerClient(
            _azureConfig.CoverImages
        );
        try
        {
            string blobName = FindBlobName(blobUrl);

            BlobClient existTagsBlobClient = imageContainer.GetBlobClient(blobName);
            await existTagsBlobClient.DeleteIfExistsAsync(DeleteSnapshotsOption.IncludeSnapshots);

            return await UploadCoverImage(imageData, articleId);
        }
        catch (Exception)
        {
            throw;
        }
    }

    public async Task<List<string>> UploadProductPhotos(List<string> imagesData, string productId)
    {
        List<string> imageUrls = new();
        BlobContainerClient imageContainer = _blobServiceClient.GetBlobContainerClient(
            _azureConfig.ProductPhotos
        );
        try
        {
            for (int i = 0; i < imagesData.Count; i++)
            {
                string imageType = GetImageTypeFromBase64(imagesData[i]);

                BinaryData binaryData = new(RemovePrefixFromBase64(imagesData[i]));

                string blobName =
                    $"{DateTimeOffset.Now.ToString("yyyy/MM/dd/HH/mm/ss").Replace('/', '_')}{productId}_{IDGeneratorService.GetShortUniqueId()}.{imageType}";

                BlobClient imageBlobClient = imageContainer.GetBlobClient(blobName);

                await imageBlobClient.UploadAsync(binaryData);
                string imageIconUrl = imageBlobClient.Uri.ToString();

                imageUrls.Add(imageIconUrl);
            }
            return imageUrls;
        }
        catch (Exception)
        {
            throw;
        }
    }

    public async Task<string> UploadProductPhoto(string imageData, string productId)
    {
        BlobContainerClient imageContainer = _blobServiceClient.GetBlobContainerClient(
            _azureConfig.ProductPhotos
        );
        try
        {
            string imageType = GetImageTypeFromBase64(imageData);

            BinaryData binaryData = new(RemovePrefixFromBase64(imageData));
            string blobName =
                $"{DateTimeOffset.Now.ToString("yyyy/MM/dd/HH/mm/ss").Replace('/', '_')}{productId}_{IDGeneratorService.GetShortUniqueId()}.{imageType}";

            BlobClient imageBlobClient = imageContainer.GetBlobClient(blobName);

            await imageBlobClient.UploadAsync(binaryData);
            string imageIconUrl = imageBlobClient.Uri.ToString();

            return imageIconUrl;
        }
        catch (Exception)
        {
            throw;
        }
    }

    private byte[] RemovePrefixFromBase64(string base64String)
    {
        int prefixEndIndex = base64String.IndexOf(',') + 1;

        string base64WithoutPrefix = base64String.Substring(prefixEndIndex);

        byte[] bytes = Convert.FromBase64String(base64WithoutPrefix);

        return bytes;
    }

    private string GetImageTypeFromBase64(string base64String)
    {
        int prefixEndIndex = base64String.IndexOf(';');

        string prefix = base64String.Substring(0, prefixEndIndex);

        string imageType = prefix.Replace("data:image/", "");

        return imageType;
    }

    private string FindBlobName(string blobUrl)
    {
        int lastSlashIndex = blobUrl.LastIndexOf('/');

        if (lastSlashIndex >= 0)
        {
            return blobUrl[(lastSlashIndex + 1)..];
        }
        else
        {
            return blobUrl;
        }
    }

    public async Task DeleteProductPhoto(string imageUrl)
    {
        BlobContainerClient avatarContainer = _blobServiceClient.GetBlobContainerClient(
            _azureConfig.ProductPhotos
        );
        try
        {
            string blobName = FindBlobName(imageUrl);

            BlobClient existAvatarBlobClient = avatarContainer.GetBlobClient(blobName);
            await existAvatarBlobClient.DeleteIfExistsAsync(DeleteSnapshotsOption.IncludeSnapshots);
        }
        catch (Exception)
        {
            throw;
        }
    }
}
