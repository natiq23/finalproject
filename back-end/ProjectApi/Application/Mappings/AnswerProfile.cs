﻿using AppDomain.DTO;
using AppDomain.DTOs.Answer;
using AppDomain.Entities.ContentRelated;
using Application.Tasks.Commands.Insert.InsertAnswer;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Mappings
{
    public class AnswerProfile : Profile
    {
        public AnswerProfile()
        {
            CreateMap<Answer,AnswerDTO>().ReverseMap();
            CreateMap<InsertAnswerCommand,Answer>().ReverseMap();
            CreateMap<Answer,GetByIdAnswerDTO>().ReverseMap();
        }
    }
}
