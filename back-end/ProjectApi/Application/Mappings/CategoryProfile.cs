﻿using AppDomain.DTO;
using AppDomain.DTOs.ArticleFlag;
using AppDomain.DTOs.Category;
using AppDomain.DTOs.Tags;
using AppDomain.Entities.TagBaseRelated;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Mappings
{
    public class CategoryProfile : Profile
    {
        public CategoryProfile()
        {
            CreateMap<Category,CategoryBuildProfileDTO>().ReverseMap();
            CreateMap<InsertCategoryDTO,Category>().ReverseMap();
            CreateMap<UpdateCategoryDTO,Category>().ReverseMap();
           
            CreateMap<Category,TagFullResponseDto>().ReverseMap();
            CreateMap<TagFullResponseDto,CategoryBuildProfileDTO>().ReverseMap();
            
            CreateMap<Category,TagPreviewResponseDto>().ReverseMap();
            CreateMap<Category,TagDTO>().ReverseMap();
        }
    }
}
