﻿using AppDomain.DTOs.Product;
using AppDomain.Entities.ContentRelated;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Mappings
{
    public class ProductProfile : Profile
    {
        public ProductProfile()
        {
            CreateMap<Product,GetAllProductDTO>().ReverseMap();
            CreateMap<Product,GetByIdProductDTO>().ReverseMap();
            CreateMap<InsertProductDTO,Product>().ReverseMap();
        }
    }
}
