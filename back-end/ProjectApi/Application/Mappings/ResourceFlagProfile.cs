﻿using AppDomain.DTOs.Resource;
using AppDomain.DTOs.ResourceFlag;
using AppDomain.Entities.TagBaseRelated;
using Application.Tasks.Commands.Insert.InsertResourceFlag;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppDomain.DTOs.Tags;

namespace Application.Mappings
{
    public class ResourceFlagProfile : Profile
    {
        public ResourceFlagProfile()
        {
            CreateMap<InsertResourceFlagDTO,ResourceFlag>().ReverseMap();
            CreateMap<UpdateResourceFlagDTO,ResourceFlag>().ReverseMap();
            CreateMap<ResourceFlag,TagPreviewResponseDto>().ReverseMap();
        }
    }
}
