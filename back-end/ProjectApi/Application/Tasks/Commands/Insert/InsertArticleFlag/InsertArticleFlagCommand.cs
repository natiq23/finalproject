﻿using AppDomain.DTOs.ArticleFlag;
using MediatR;

namespace Application.Tasks.Commands.Insert.InsertArticleFlag;

public class InsertArticleFlagCommand : IRequest<string>
{
    public InsertArticleFlagDTO InsertTagDTO { get; set; }
}
