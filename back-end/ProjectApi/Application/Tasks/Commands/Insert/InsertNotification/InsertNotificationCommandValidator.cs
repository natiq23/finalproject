﻿using FluentValidation;

namespace Application.Tasks.Commands.Insert.InsertNotification;

public class InsertNotificationCommandValidator : AbstractValidator<InsertNotificationCommand>
{
    public InsertNotificationCommandValidator()
    {
        RuleFor(dto => dto.NotificationDTO.Type).IsInEnum();
    }
}