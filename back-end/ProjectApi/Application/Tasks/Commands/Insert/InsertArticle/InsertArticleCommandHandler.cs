﻿using AppDomain.Entities.ContentRelated;
using AppDomain.Interfaces;
using Application.Services;
using AutoMapper;
using MediatR;

namespace Application.Tasks.Commands.Insert.InsertArticle
{
    public class InsertArticleCommandHandler : IRequestHandler<InsertArticleCommand, string>
    {
        private readonly IArticleRepository _articleRepository;

        private readonly IUserRepository _userRepository;

        private readonly IMapper _mapper;

        public InsertArticleCommandHandler(
            IArticleRepository articleRepository,
            IUserRepository userRepository,
            IMapper mapper
        )
        {
            _articleRepository = articleRepository;
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<string> Handle(
            InsertArticleCommand request,
            CancellationToken cancellationToken
        )
        {
            try
            {
                var user = await _userRepository.GetCurrentUser();

                if (user.IsFrozen)
                    return "-2";

                Article article =
                    new()
                    {
                        Id = IDGeneratorService.GetShortUniqueId(),
                        Body = request.Body,
                        Header = request.Header,
                        ReadingTime = request.ReadingTime,
                        ViewCount = 0,
                        VoteCount = 0,
                        IsDeleted = false,
                        UserId = user.Id,
                        UpdateTime = DateTime.UtcNow,
                    };

                var result = await _articleRepository.InsertArticle(
                    article,
                    request.CategoriesIdList,
                    request.FlagId
                );

                return result;
            }
            catch (Exception)
            {
                return "-1";
            }
        }
    }
}
