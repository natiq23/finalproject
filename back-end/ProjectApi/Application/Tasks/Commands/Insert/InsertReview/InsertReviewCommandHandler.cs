﻿using AppDomain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Insert.InsertReview
{
    public class InsertReviewCommandHandler : IRequestHandler<InsertReviewCommand, string>
    {
        private readonly IReviewRepository _repository;

        public InsertReviewCommandHandler(IReviewRepository repository)
        {
            _repository = repository;
        }

        public Task<string> Handle(InsertReviewCommand request, CancellationToken cancellationToken)
        {
            return _repository.InsertReview(request.ProductId, request.Body, request.Rank);
        }
    }
}
