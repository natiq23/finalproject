﻿using AppDomain.DTOs.Report;
using MediatR;

namespace Application.Tasks.Commands.Insert.InsertContentReport;

public class InsertReportCommand : IRequest<Task>
{
    public ReportDTO ReportDTO { get; set; }
}