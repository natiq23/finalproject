﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Insert.VoteComment;

public class VoteCommentCommandHandler : IRequestHandler<VoteCommentCommand>
{
    private readonly ICommentRepository _commentRepository;

    public VoteCommentCommandHandler(ICommentRepository commentRepository)
    {
        _commentRepository = commentRepository;
    }

    public async Task Handle(VoteCommentCommand request, CancellationToken cancellationToken)
    {
        await _commentRepository.VoteCommentAsync(request.Status, request.CommentId);
    }
}
