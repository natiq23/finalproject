﻿using AppDomain.Enums;
using MediatR;

namespace Application.Tasks.Commands.Insert.UserInserts.InsertConnectedAccount;

public class InsertConnectedAccountCommand : IRequest<Task>
{
    public string Secret { get; set; }
    public string AccountUsername { get; set; }
    public string Email { get; set; }
    public ConnectedAccountType Type { get; set; }
}
