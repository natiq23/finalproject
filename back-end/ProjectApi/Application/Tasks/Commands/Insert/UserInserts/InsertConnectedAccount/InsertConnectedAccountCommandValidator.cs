﻿using FluentValidation;

namespace Application.Tasks.Commands.Insert.UserInserts.InsertConnectedAccount;

public class InsertConnectedAccountCommandValidator
    : AbstractValidator<InsertConnectedAccountCommand>
{
    public InsertConnectedAccountCommandValidator()
    {
        RuleFor(account => account.Secret).NotEmpty();

        RuleFor(account => account.AccountUsername).NotEmpty();

        RuleFor(account => account.Email).NotEmpty().EmailAddress();

        RuleFor(account => account.Type).NotEmpty().IsInEnum();
    }
}
