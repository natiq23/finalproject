﻿using AppDomain.DTOs.Auth;
using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Insert.UserInserts.InsertUser;

public class InsertUserCommandHandler : IRequestHandler<InsertUserCommand, UserAuthDto>
{
    private readonly IAuthRepository _authRepository;

    public InsertUserCommandHandler(IAuthRepository authRepository)
    {
        _authRepository = authRepository;
    }

    public async Task<UserAuthDto> Handle(
        InsertUserCommand request,
        CancellationToken cancellationToken
    )
    {
        return await _authRepository.RegisterUserAsync(request.User);
    }
}
