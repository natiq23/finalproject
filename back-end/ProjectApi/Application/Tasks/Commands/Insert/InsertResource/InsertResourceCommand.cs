﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Insert.InsertResource
{
    public class InsertResourceCommand : IRequest<string>
    {
        public string? Title { get; set; }
        public string? Url { get; set; }
        public string CategoryId { get; set; }
        public string ResourceFlagId { get; set; }
    }
}
