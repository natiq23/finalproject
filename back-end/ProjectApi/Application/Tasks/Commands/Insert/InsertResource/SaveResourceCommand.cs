﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Insert.InsertResource
{
    public class SaveResourceCommand : IRequest<int>
    {
        public string ResourceId { get; set; }
    }
}
