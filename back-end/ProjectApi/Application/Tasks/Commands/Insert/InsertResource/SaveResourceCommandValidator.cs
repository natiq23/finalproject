﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Insert.InsertResource
{
    public class SaveResourceCommandValidator : AbstractValidator<SaveResourceCommand>
    {
        public SaveResourceCommandValidator()
        {
            RuleFor(x => x.ResourceId).NotNull().NotEmpty();
        }
    }
}
