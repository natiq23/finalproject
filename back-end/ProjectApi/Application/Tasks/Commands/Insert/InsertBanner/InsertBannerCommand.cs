﻿using AppDomain.DTOs.Admin;
using MediatR;

namespace Application.Tasks.Commands.Insert.InsertBanner;

public class InsertBannerCommand : IRequest<Task>
{
    public BannerDTO BannerDTO { get; set; }
}