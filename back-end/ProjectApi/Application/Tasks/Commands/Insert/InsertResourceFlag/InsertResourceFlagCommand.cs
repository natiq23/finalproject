﻿using AppDomain.DTOs.ResourceFlag;
using MediatR;

namespace Application.Tasks.Commands.Insert.InsertResourceFlag;

public class InsertResourceFlagCommand : IRequest<string>
{
    public InsertResourceFlagDTO InsertTagDTO { get; set; }
}
