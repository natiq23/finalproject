﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Delete.DeleteQuestion
{
    public class DeleteQuestionCommandHandler : IRequestHandler<DeleteQuestionCommand,int>
    {
        private readonly IQuestionRepository _questionRepository;

        private readonly IUserRepository _userRepository;

        public DeleteQuestionCommandHandler(IQuestionRepository questionRepository, IUserRepository userRepository)
        {
            _questionRepository = questionRepository;
            _userRepository = userRepository;
        }

        public async Task<int> Handle(DeleteQuestionCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var userId = _userRepository.GetClaimValue("userId");

                var result = await _questionRepository.DeleteQuestion(request.QuestionId, userId);

                return result;
            }
            catch (Exception)
            {
                return -1;
            }
        }
    }
}