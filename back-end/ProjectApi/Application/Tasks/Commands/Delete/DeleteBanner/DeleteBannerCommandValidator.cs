﻿using FluentValidation;

namespace Application.Tasks.Commands.Delete.DeleteBanner;

public class DeleteBannerCommandValidator : AbstractValidator<DeleteBannerCommand>
{
    public DeleteBannerCommandValidator()
    {
        RuleFor(x => x.BannerId).NotEmpty();
    }
}