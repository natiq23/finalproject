﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Delete.DeleteAnswer
{
    public class DeleteAnswerCommand : IRequest<int>
    {
        public string AnswerId { get; set; }
    }
}
