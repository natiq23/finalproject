﻿using FluentValidation;

namespace Application.Tasks.Commands.Delete.DeleteComment;

public class DeleteCommentCommandValidator : AbstractValidator<DeleteCommentCommand>
{
    public DeleteCommentCommandValidator()
    {
        RuleFor(x => x.CommentId)
            .NotNull()
            .NotEmpty();
    }
}