﻿using FluentValidation;

namespace Application.Tasks.Commands.Delete.DeleteResource
{
    public class DeleteResourceCommandValidator : AbstractValidator<DeleteResourceCommand>
    {
        public DeleteResourceCommandValidator()
        {
            RuleFor(x => x.ResourceId).NotNull().NotEmpty();
        }
    }
}