﻿using MediatR;

namespace Application.Tasks.Commands.Delete.DeleteCategory
{
    public class DeleteCategoryCommand : IRequest<string>
    {
        public string CategoryId { get; set; }
    }
}