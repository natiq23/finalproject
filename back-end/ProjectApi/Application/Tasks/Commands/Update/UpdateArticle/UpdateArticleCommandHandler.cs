﻿using AppDomain.DTOs.Article;
using AppDomain.Interfaces;
using AutoMapper;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateArticle
{
    public class UpdateArticleCommandHandler : IRequestHandler<UpdateArticleCommand,int>
    {
        private readonly IArticleRepository _articleRepository;

        private readonly IUserRepository _userRepository;

        private readonly IMapper _mapper;

        public UpdateArticleCommandHandler(IArticleRepository articleRepository, IUserRepository userRepository, IMapper mapper)
        {
            _articleRepository = articleRepository;
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<int> Handle(UpdateArticleCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var user = await _userRepository.GetCurrentUser();

                if (user.IsFrozen)
                    return -2;

                UpdateArticleDTO updateArticleDTO = _mapper.Map<UpdateArticleDTO>(request);

                var result = await _articleRepository.UpdateArticle(updateArticleDTO,user.Id);

                return result;
            }
            catch (Exception)
            {
                return -1;
            }
        }
    }
}
