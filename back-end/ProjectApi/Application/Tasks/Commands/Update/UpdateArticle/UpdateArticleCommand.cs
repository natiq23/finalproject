﻿using AppDomain.ValueObjects;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Update.UpdateArticle
{
    public class UpdateArticleCommand : IRequest<int>
    {
        public string Id { get; set; }
        public string? Body { get; set; }
        public Header? Header { get; set; }
        public int? ReadingTime { get; set; }
        public string? FlagId { get; set; }
        public List<string>? CategoryIdList { get; set; }
    }
}
