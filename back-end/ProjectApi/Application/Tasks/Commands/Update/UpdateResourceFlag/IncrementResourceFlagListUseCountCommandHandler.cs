﻿using AppDomain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Update.UpdateResourceFlag
{
    public class IncrementResourceFlagListUseCountCommandHandler : IRequestHandler<IncrementResourceFlagListUseCountCommand,int>
    {
        private readonly IResourceFlagRepository _resourceFlagRepository;

        public IncrementResourceFlagListUseCountCommandHandler(IResourceFlagRepository resourceFlagRepository)
        {
            _resourceFlagRepository = resourceFlagRepository;
        }

        public async Task<int> Handle(IncrementResourceFlagListUseCountCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var result = await _resourceFlagRepository.IncrementUseCounts(request.ResourceFlagIdList);

                return result;
            }
            catch (Exception)
            {
                return -1;
            }
        }
    }
}