﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Update.UpdateResourceFlag
{
    public class IncrementResourceFlagListUseCountCommand : IRequest<int>
    {
        public List<string> ResourceFlagIdList { get; set; }
    }
}
