﻿/*using AppDomain.Entities.AdminRelated;
using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateReportStatus;

public class UpdateReportStatusCommandHandler : IRequestHandler<UpdateReportStatusCommand, Report>
{
    private readonly IReportRepository _reportRepository;

    public UpdateReportStatusCommandHandler(IReportRepository reportRepository)
    {
        _reportRepository = reportRepository;
    }

    public async Task<Report> Handle(UpdateReportStatusCommand request, CancellationToken cancellationToken)
    {
        return await _reportRepository.UpdateReportStatusAsync(request.ReportStatus, request.ReportId, request.ModeratorId);
    }
}*/