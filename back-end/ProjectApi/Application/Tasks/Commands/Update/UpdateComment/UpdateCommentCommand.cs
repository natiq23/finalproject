﻿using MediatR;

namespace Application.Tasks.Commands.Update.UpdateUser.UpdateComment;

public class UpdateCommentCommand : IRequest<Task>
{
    public string ContentId { get; set; }
    public string Body { get; set; }
}
