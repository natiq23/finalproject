﻿using AppDomain.DTOs.Product;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Update.UpdateProduct
{
    public class UpdateProductCommand : IRequest
    {
        public UpdateProductDTO UpdateProductDTO { get; set; }
    }
}
