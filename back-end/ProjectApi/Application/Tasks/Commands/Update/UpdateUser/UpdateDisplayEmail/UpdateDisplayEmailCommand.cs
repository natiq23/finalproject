﻿using MediatR;

namespace Application.Tasks.Commands.Update.UpdateUser.UpdateDisplayEmail;

public class UpdateDisplayEmailCommand : IRequest<Task>
{
    public string? Email { get; set; }

    public bool UsePrimaryEmail { get; set; }

    public bool RemoveDisplayEmail { get; set; }
}
