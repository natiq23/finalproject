﻿using MediatR;

namespace Application.Tasks.Commands.Update.UpdateUser.UpdatePassword;

public class UpdatePasswordCommand : IRequest<Task>
{
    public string CurrentPassword { get; set; }
    public string NewPassword { get; set; }
}