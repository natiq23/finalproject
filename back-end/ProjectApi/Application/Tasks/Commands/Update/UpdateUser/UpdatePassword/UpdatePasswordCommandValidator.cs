﻿using FluentValidation;

namespace Application.Tasks.Commands.Update.UpdateUser.UpdatePassword;

public class UpdatePasswordCommandValidator : AbstractValidator<UpdatePasswordCommand>
{
    public UpdatePasswordCommandValidator()
    {
        RuleFor(u => u.NewPassword)
            .NotEmpty()
            .NotNull()
            .MinimumLength(8) // Minimum length requirement
            .MaximumLength(16) // Maximum length requirement
            .Matches("[A-Z]") // At least one uppercase letter
            .Matches("[a-z]") // At least one lowercase letter
            .Matches("[0-9]") // At least one digit
            .Matches("[^a-zA-Z0-9]"); // At least one special character

        RuleFor(u => u.CurrentPassword)
            .NotEmpty()
            .NotNull()
            .MinimumLength(8) // Minimum length requirement
            .MaximumLength(16) // Maximum length requirement
            .Matches("[A-Z]") // At least one uppercase letter
            .Matches("[a-z]") // At least one lowercase letter
            .Matches("[0-9]") // At least one digit
            .Matches("[^a-zA-Z0-9]"); // At least one special character
    }
}
