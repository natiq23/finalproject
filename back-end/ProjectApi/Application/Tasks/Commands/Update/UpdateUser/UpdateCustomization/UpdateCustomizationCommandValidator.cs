﻿using FluentValidation;

namespace Application.Tasks.Commands.Update.UpdateUser.UpdateCustomization;

public class UpdateCustomizationCommandValidator
    : AbstractValidator<UpdateCustomizationSettingsCommand>
{
    public UpdateCustomizationCommandValidator() { }
}
