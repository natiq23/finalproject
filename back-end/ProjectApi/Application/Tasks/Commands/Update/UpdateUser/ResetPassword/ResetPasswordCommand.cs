﻿using MediatR;

namespace Application.Tasks.Commands.Update.UpdateUser.ResetPassword;

public class ResetPasswordCommand : IRequest<Task>
{
    public string Email { get; set; }
    public string NewPassword { get; set; }
}
