﻿using FluentValidation;

namespace Application.Tasks.Commands.Update.UpdateUser.UpdatePinnedRepositories;

public class UpdatePinnedRepositoriesCommandValidator : AbstractValidator<UpdatePinnedRepositoriesCommand>
{
    public UpdatePinnedRepositoriesCommandValidator()
    {
        RuleFor(x => x.RepositoryId).NotEmpty();
    }
}