﻿using MediatR;

namespace Application.Tasks.Commands.Update.UpdateUser.UpdatePrimaryEmail;

public class UpdatePrimaryEmailCommand : IRequest<Task>
{
    public string Email { get; set; }
}
