﻿using MediatR;

namespace Application.Tasks.Commands.Update.UpdateUser.UpdatePinnedArticles;

public class UpdatePinnedArticlesCommand : IRequest<Task>
{
    public string ArticleId { get; set; }
}
