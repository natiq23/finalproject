﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Update.UpdateQuestion.SetCorrectAnswer
{
    public class SetCorrectAnswerCommand : IRequest<string>
    {
        public string QuestionId { get; set; }
        public string AnswerId { get; set; }
    }
}
