﻿using AppDomain.Enums;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Update.UpdateQuestion.VoteQuestion
{
    public class VoteQuestionCommand : IRequest<int>
    {
        public VoteStatus Status { get; set; }
        public string QuestionId { get; set; }
    }
}
