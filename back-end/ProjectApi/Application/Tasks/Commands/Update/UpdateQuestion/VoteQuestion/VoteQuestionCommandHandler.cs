﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateQuestion.VoteQuestion
{
    public class VoteQuestionCommandHandler : IRequestHandler<VoteQuestionCommand,int>
    {
        private readonly IQuestionRepository _questionRepository;

        private readonly IUserRepository _userRepository;

        public VoteQuestionCommandHandler(IQuestionRepository questionRepository, IUserRepository userRepository)
        {
            _questionRepository = questionRepository;
            _userRepository = userRepository;
        }

        public async Task<int> Handle(VoteQuestionCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var userId = _userRepository.GetClaimValue("userId");

                var result = await _questionRepository.VoteQuestion(request.Status,request.QuestionId, userId);

                return result;
            }
            catch (Exception)
            {
                return -1;
            }
        }
    }
}