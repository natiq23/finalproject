﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Commands.Update.UpdateResource.ResourceEdit
{
    public class EditResourceCommandHandler : IRequestHandler<EditResourceCommand,string>
    {
        private readonly IResourceRepository _resourceRepository;

        public EditResourceCommandHandler(IResourceRepository resourceRepository)
        {
            _resourceRepository = resourceRepository;
        }

        public async Task<string> Handle(EditResourceCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var result = await _resourceRepository.EditResource(request.EditResource);

                return result;
            }
            catch (Exception)
            {
                return "-1";
            }
        }
    }
}