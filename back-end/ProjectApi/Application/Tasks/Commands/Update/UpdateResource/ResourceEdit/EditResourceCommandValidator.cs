﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Commands.Update.UpdateResource.ResourceEdit
{
    public class EditResourceCommandValidator : AbstractValidator<EditResourceCommand>
    {
        public EditResourceCommandValidator()
        {
            RuleFor(x => x.EditResource.ResourceId).NotEmpty().NotNull();
        }
    }
}
