﻿using AppDomain.DTO;
using AppDomain.Interfaces;
using AutoMapper;
using MediatR;

namespace Application.Tasks.Queries.AnswerQueries.GetAnswerById
{
    public class GetAllAnswerByIdQueryHandler : IRequestHandler<GetAllAnswerByIdQuery, List<AnswerDTO>>
    {
        private readonly IAnswerRepository _answerRepository;

        private readonly IMapper _mapper;

        public GetAllAnswerByIdQueryHandler(IAnswerRepository answerRepository, IMapper mapper)
        {
            _answerRepository = answerRepository;
            _mapper = mapper;
        }

        public async Task<List<AnswerDTO>?> Handle(GetAllAnswerByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
              
                var result = await _answerRepository.GetAnswerById(request.QuestionId);

                if (result is null)
                    return null;

                List<AnswerDTO> answersDTOList = new();

                foreach (var item in result)
                {
                    AnswerDTO answerDTO = _mapper.Map<AnswerDTO>(item);
                    answersDTOList.Add(answerDTO);
                }


                return answersDTOList;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}