﻿using AppDomain.DTOs.Answer;
using AppDomain.Entities.ContentRelated;
using AppDomain.Interfaces;
using Application.Tasks.Queries.AnswerQueries.GetById;
using MediatR;

namespace Application.Tasks.Queries.AnswerQueries.GetById
{
    public class GetByIdQueryHandler : IRequestHandler<GetByIdQuery,GetByIdAnswerDTO>
    {
        private readonly IAnswerRepository _answerRepository;

        private readonly IUserRepository _userRepository;

        public GetByIdQueryHandler(IAnswerRepository answerRepository, IUserRepository userRepository)
        {
            _answerRepository = answerRepository;
            _userRepository = userRepository;
        }

        public async Task<GetByIdAnswerDTO> Handle(GetByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var userId = _userRepository.GetClaimValue("userId");

                var result = await _answerRepository.GetById(request.AnswerId, userId);

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}