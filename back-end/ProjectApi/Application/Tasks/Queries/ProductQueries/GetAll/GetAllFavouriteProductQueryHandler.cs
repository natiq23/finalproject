﻿using AppDomain.DTOs.Pagination;
using AppDomain.DTOs.Product;
using AppDomain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.ProductQueries.GetAll;

public class GetAllFavouriteProductQueryHandler : IRequestHandler<GetAllFavouriteProductQuery, PaginatedListDto<GetAllProductDTO>>
{
    private readonly IProductRepository _repository;

    public GetAllFavouriteProductQueryHandler(IProductRepository repository)
    {
        _repository = repository;
    }

    public async Task<PaginatedListDto<GetAllProductDTO>> Handle(GetAllFavouriteProductQuery request, CancellationToken cancellationToken)
    {
        return await _repository.GetAllFavouriteProducts(request.SearchQuery, request.Sort, request.Page);
    }
}
