﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.ProductQueries.GetById
{
    public class GetByIdProductQueryValidation : AbstractValidator<GetByIdProductQuery>
    {
        public GetByIdProductQueryValidation()
        {
            RuleFor(x => x.ProductId).NotNull().NotEmpty();
        }
    }
}
