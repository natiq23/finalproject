﻿using AppDomain.DTOs.Product;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.ProductQueries.GetById
{
    public class GetByIdProductQuery : IRequest<GetByIdProductDTO>
    {
        public string ProductId { get; set; }
    }
}
