﻿using AppDomain.Entities.NotificationRelated;
using AppDomain.Enums;
using MediatR;

namespace Application.Tasks.Queries.NotificationQueries.GetNotificationsByType;

public class GetNotificationsByTypeQuery : IRequest<IEnumerable<Notification>>
{
    public NotificationType Type { get; set; }
}