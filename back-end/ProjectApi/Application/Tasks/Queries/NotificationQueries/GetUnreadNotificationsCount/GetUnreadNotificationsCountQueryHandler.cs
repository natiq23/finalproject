﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Queries.NotificationQueries.GetUnreadNotificationsCount;

public class GetUnreadNotificationsCountQueryHandler : IRequestHandler<GetUnreadNotificationsCountQuery, int>
{
    private readonly INotificationRepository _notificationRepository;

    public GetUnreadNotificationsCountQueryHandler(INotificationRepository notificationRepository)
    {
        _notificationRepository = notificationRepository;
    }

    public async Task<int> Handle(GetUnreadNotificationsCountQuery request, CancellationToken cancellationToken)
    {
        return await _notificationRepository.GetUnreadNotificationsCountAsync();
    }
}