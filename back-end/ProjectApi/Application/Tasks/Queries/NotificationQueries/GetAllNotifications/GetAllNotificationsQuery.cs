﻿using AppDomain.Entities.NotificationRelated;
using MediatR;

namespace Application.Tasks.Queries.NotificationQueries.GetAllNotifications;

public class GetAllNotificationsQuery : IRequest<IEnumerable<Notification>>
{

}