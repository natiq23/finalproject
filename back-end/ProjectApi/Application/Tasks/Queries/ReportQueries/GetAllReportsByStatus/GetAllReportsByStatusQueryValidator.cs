﻿using FluentValidation;

namespace Application.Tasks.Queries.ReportQueries.GetAllReportsByStatus;

public class GetAllReportsByStatusQueryValidator : AbstractValidator<GetAllReportsByStatusQuery>
{
    public GetAllReportsByStatusQueryValidator()
    {
        RuleFor(x => x.ReportStatus)
            .NotEmpty()
            .IsInEnum();
    }
}