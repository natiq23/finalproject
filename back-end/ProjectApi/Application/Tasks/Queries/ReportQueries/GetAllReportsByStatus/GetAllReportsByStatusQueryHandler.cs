﻿using AppDomain.Entities.AdminRelated;
using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Queries.ReportQueries.GetAllReportsByStatus;

public class GetAllReportsByStatusQueryHandler : IRequestHandler<GetAllReportsByStatusQuery, IEnumerable<Report>>
{
    private readonly IReportRepository _reportRepository;

    public GetAllReportsByStatusQueryHandler(IReportRepository reportRepository)
    {
        _reportRepository = reportRepository;
    }

    public async Task<IEnumerable<Report>> Handle(GetAllReportsByStatusQuery request, CancellationToken cancellationToken)
    {
        var reports = await _reportRepository.GetAllReportsByStatus(request.ReportStatus);

        return reports;
    }
}
