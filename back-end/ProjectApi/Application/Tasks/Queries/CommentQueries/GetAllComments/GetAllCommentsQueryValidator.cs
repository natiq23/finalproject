﻿using FluentValidation;

namespace Application.Tasks.Queries.UserQueries.GetAllComments;

public class GetUserValidator : AbstractValidator<GetAllCommentsQuery>
{
    public GetUserValidator()
    {
        RuleFor(x => x.ContentId)
            .NotNull()
            .NotEmpty();
    }
}