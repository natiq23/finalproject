﻿using AppDomain.Interfaces;
using AppDomain.Responses;
using AppDomain.ValueObjects;
using MediatR;

namespace Application.Tasks.Queries.UserQueries.GetPinnedRepositories;

public class GetRepositoriesQueryHandler : IRequestHandler<GetPinnedRepositoriesQuery, IEnumerable<PinnedRepository>>
{
    private readonly IUserRepository _userRepository;

    public GetRepositoriesQueryHandler(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public async Task<IEnumerable<PinnedRepository>> Handle(GetPinnedRepositoriesQuery request, CancellationToken cancellationToken)
    {
        return await _userRepository.GetPinnedRepositoriesAsync(request.UserId);
    }
}