﻿using FluentValidation;

namespace Application.Tasks.Queries.UserQueries.GetPinnedArticles;

public class GetPinnedArticlesQueryValidator : AbstractValidator<GetPinnedArticlesQuery>
{
    public GetPinnedArticlesQueryValidator()
    {
        RuleFor(u => u.UserId).NotEmpty();
    }
}