﻿using AppDomain.Responses;
using MediatR;

namespace Application.Tasks.Queries.UserQueries.GetLeaderboard;

public class GetLeaderboardQuery : IRequest<IEnumerable<LeaderboardResponse>>
{

}