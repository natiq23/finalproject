﻿using FluentValidation;

namespace Application.Tasks.Queries.UserQueries.ConnectedAccountTypeExist;

public class ConnectedAccountTypeExistQueryValidator
    : AbstractValidator<ConnectedAccountTypeExistQuery>
{
    public ConnectedAccountTypeExistQueryValidator()
    {
        RuleFor(u => u.ConnectedAccountType).NotEmpty();
    }
}
