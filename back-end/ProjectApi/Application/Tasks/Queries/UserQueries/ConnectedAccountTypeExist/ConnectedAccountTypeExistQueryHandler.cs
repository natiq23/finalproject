﻿using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Queries.UserQueries.ConnectedAccountTypeExist;

public class ConnectedAccountTypeExistQueryHandler : IRequestHandler<ConnectedAccountTypeExistQuery, bool>
{
    private readonly IUserRepository _userRepository;

    public ConnectedAccountTypeExistQueryHandler(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public async Task<bool> Handle(ConnectedAccountTypeExistQuery request, CancellationToken cancellationToken)
    {
        return await _userRepository.IsConnectedAccountTypeExistAsync(request.ConnectedAccountType);
    }
}