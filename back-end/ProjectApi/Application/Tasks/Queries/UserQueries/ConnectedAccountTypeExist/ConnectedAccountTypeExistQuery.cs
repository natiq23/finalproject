﻿using AppDomain.Enums;
using MediatR;

namespace Application.Tasks.Queries.UserQueries.ConnectedAccountTypeExist;

public class ConnectedAccountTypeExistQuery : IRequest<bool>
{
    public ConnectedAccountType ConnectedAccountType { get; set; }
}
