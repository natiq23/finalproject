﻿using AppDomain.Responses.UserResponses;
using MediatR;

namespace Application.Tasks.Queries.UserQueries.GetMe;

public class GetMeQuery : IRequest<UserPreviewResponse>
{

}