﻿using AppDomain.Responses;
using MediatR;

namespace Application.Tasks.Queries.UserQueries.GetConnectedEmails;

public class GetConnectedEmailsQuery : IRequest<IEnumerable<EmailResponse>>
{

}