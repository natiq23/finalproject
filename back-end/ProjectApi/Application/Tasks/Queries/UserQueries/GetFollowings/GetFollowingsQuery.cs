﻿using AppDomain.Responses.UserResponses;
using MediatR;

namespace Application.Tasks.Queries.UserQueries.GetFollowings;

public class GetFollowingsQuery : IRequest<IEnumerable<UserProfileResponse>>
{

}