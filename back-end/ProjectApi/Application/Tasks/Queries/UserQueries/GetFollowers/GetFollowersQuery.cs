﻿using AppDomain.Responses.UserResponses;
using MediatR;

namespace Application.Tasks.Queries.UserQueries.GetFollowers;

public class GetFollowersQuery : IRequest<IEnumerable<UserProfileResponse>>
{

}