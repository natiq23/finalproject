﻿using AppDomain.Interfaces;
using AppDomain.Responses.UserResponses;
using MediatR;

namespace Application.Tasks.Queries.UserQueries.GetFollowers;

public class GetFollowersQueryHandler : IRequestHandler<GetFollowersQuery, IEnumerable<UserProfileResponse>>
{
    private readonly IUserRepository _userRepository;

    public GetFollowersQueryHandler(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public async Task<IEnumerable<UserProfileResponse>> Handle(GetFollowersQuery request, CancellationToken cancellationToken)
    {
        return await _userRepository.GetFollowersAsync();
    }
}