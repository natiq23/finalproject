﻿using FluentValidation;

namespace Application.Tasks.Queries.UserQueries.GetAllUsers;

public class GetUserValidator : AbstractValidator<GetAllUsersQuery>
{
    public GetUserValidator()
    {
        RuleFor(u => u.Search).NotEmpty();
    }
}