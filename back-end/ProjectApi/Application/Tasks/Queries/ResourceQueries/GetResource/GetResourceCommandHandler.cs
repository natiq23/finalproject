﻿using AppDomain.DTOs.Resource;
using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Queries.ResourceQueries.GetResource;

public class GetResourceCommandHandler : IRequestHandler<GetResourceCommand, ResourceResponse>
{
    private readonly IResourceRepository _resourceRepository;

    public GetResourceCommandHandler(IResourceRepository resourceRepository)
    {
        _resourceRepository = resourceRepository;
    }

    public Task<ResourceResponse> Handle(
        GetResourceCommand request,
        CancellationToken cancellationToken
    )
    {
        return _resourceRepository.GetResource(request.ResourceId);
    }
}