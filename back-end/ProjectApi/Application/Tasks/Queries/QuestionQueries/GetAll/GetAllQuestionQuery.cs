﻿using AppDomain.DTOs.Pagination;
using AppDomain.DTOs.Question;
using AppDomain.Enums;
using MediatR;

namespace Application.Tasks.Queries.QuestionQueries.GetAll;

public class GetAllQuestionQuery : IRequest<PaginatedListDto<GetAllQuestionDTO>>
{
    public string? SearchQuery { get; set; }
    public List<string>? CategoryIdList { get; set; }
    public SortOptions Sort { get; set; } = SortOptions.Relevant;
    public int Page { get; set; } = 1;
}