﻿using AppDomain.DTOs.Question;
using MediatR;

namespace Application.Tasks.Queries.ArticleQueries.GetLastQuestions;

public class GetLastQuestionsQuery : IRequest<IEnumerable<GetLastQuestionsDTO>>
{

}