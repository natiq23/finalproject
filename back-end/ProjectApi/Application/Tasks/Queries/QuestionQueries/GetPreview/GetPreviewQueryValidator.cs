﻿using FluentValidation;

namespace Application.Tasks.Queries.QuestionQueries.GetPreview;

public class GetPreviewQueryValidator : AbstractValidator<GetPreviewQuery>
{
    public GetPreviewQueryValidator()
    {
        RuleFor(x => x.QuestionId)
            .NotNull()
            .NotEmpty();
    }
}