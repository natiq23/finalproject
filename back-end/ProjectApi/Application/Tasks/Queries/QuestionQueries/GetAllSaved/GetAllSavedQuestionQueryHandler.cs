﻿using AppDomain.DTOs.Pagination;
using AppDomain.DTOs.Question;
using AppDomain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.QuestionQueries.GetAllSaved
{
    public class GetAllSavedQuestionQueryHandler : IRequestHandler<GetAllSavedQuestionQuery, PaginatedListDto<GetAllQuestionDTO>>
    {
        private readonly IQuestionRepository _questionRepository;

        public GetAllSavedQuestionQueryHandler(IQuestionRepository questionRepository)
        {
            _questionRepository = questionRepository;
        }

        public async Task<PaginatedListDto<GetAllQuestionDTO>> Handle(GetAllSavedQuestionQuery request, CancellationToken cancellationToken)
        {
            var data = await _questionRepository.GetAllSavedQuestion(
                request.SearchQuery,
                request.CategoryIdList,
                request.Sort,
                request.Page);

            return data;
        }
    }
}
