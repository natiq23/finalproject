﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.QuestionQueries.GetCount
{
    public class GetAllQuestionCountQuery : IRequest<int>
    {
        
    }
}
