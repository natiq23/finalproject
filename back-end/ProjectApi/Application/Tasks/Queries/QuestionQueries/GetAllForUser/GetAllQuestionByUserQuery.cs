﻿using AppDomain.DTOs.Pagination;
using AppDomain.DTOs.Question;
using AppDomain.Enums;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.QuestionQueries.GetAllForUser
{
    public class GetAllQuestionByUserQuery : IRequest<PaginatedListDto<GetAllQuestionDTO>>
    {
        public string? SearchQuery { get; set; }
        public List<string>? CategoryIdList { get; set; }
        public SortOptions Sort { get; set; } = SortOptions.Relevant;
        public int Page { get; set; } = 1;
    }
}
