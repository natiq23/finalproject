﻿using AppDomain.DTOs.Article;
using AppDomain.Interfaces;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Tasks.Queries.ArticleQueries.GetMoreFrom
{
    public class GetArticleMoreFromQueryHandler : IRequestHandler<GetArticleMoreFromQuery,List<ArticleMoreFromDTO>>
    {
        private readonly IArticleRepository _articleRepository;

        public GetArticleMoreFromQueryHandler(IArticleRepository articleRepository)
        {
            _articleRepository = articleRepository;
        }

        public async Task<List<ArticleMoreFromDTO>> Handle(GetArticleMoreFromQuery request, CancellationToken cancellationToken)
        {
            var data = await _articleRepository.GetArticleMoreFrom(request.UserId);

            return data;
        }
    }
}
