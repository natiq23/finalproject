﻿using AppDomain.DTOs.Article;
using AppDomain.DTOs.Pagination;
using AppDomain.Interfaces;
using MediatR;

namespace Application.Tasks.Queries.ArticleQueries.GetAll
{
    public class GetAllArticleQueryHandler
        : IRequestHandler<GetAllArticleQuery, PaginatedListDto<GetAllArticleDTO>>
    {
        private readonly IArticleRepository _articleRepository;

        private readonly ICacheService _cacheService;

        public GetAllArticleQueryHandler(IArticleRepository articleRepository, ICacheService cacheService)
        {
            _articleRepository = articleRepository;
            _cacheService = cacheService;
        }

        public async Task<PaginatedListDto<GetAllArticleDTO>> Handle(
            GetAllArticleQuery request,
            CancellationToken cancellationToken
        )
        {
            PaginatedListDto<GetAllArticleDTO> data;

            //var cacheData = _cacheService.GetData<PaginatedListDto<GetAllArticleDTO>>("Article");
            //if (cacheData != null)
            //{
            //    return cacheData;
            //}
            //else
            //{
            //    data = await _articleRepository.GetAllArticle(
            //    request.SearchQuery,
            //    request.CategoryIdList,
            //    request.FlagId,
            //    request.Sort,
            //    request.Page
            //    );
            //}

            //var expirationTime = DateTimeOffset.Now.AddSeconds(20.0);

            //_cacheService.SetData<PaginatedListDto<GetAllArticleDTO>>("Article", data, expirationTime);

            data = await _articleRepository.GetAllArticle(
                request.SearchQuery,
                request.CategoryIdList,
                request.FlagId,
                request.Sort,
                request.Page
                );

            return data;

        }
    }
}