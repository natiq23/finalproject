﻿namespace AppDomain.Entities.TagBaseRelated;

public class ResourceFlag : TagBase
{
    public ResourceFlag() { }

    public ResourceFlag(TagBase tag)
    {
        Id = tag.Id;
        Title = tag.Title;
        Description = tag.Description;
        UseCount = tag.UseCount;
        IconLink = tag.IconLink;
        AccentColor = tag.AccentColor;
        IsDeleted = tag.IsDeleted;
    }
}