﻿using AppDomain.Common.Entities;
using AppDomain.Enums;

namespace AppDomain.Entities.UserRelated;

public class ConnectedAccount : EntityBase
{
    public string ProviderId { get; set; }

    public string Username { get; set; } // GitHub account name

    public string Email { get; set; } = String.Empty;

    public ConnectedAccountType AccountType { get; set; }
}