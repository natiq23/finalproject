﻿using AppDomain.Common.Entities;
using AppDomain.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppDomain.Entities.UserRelated;

[Table("Users")]
public class PendingUser : EntityBase
{
    public string Name { get; set; } = String.Empty;

    public string? Username { get; set; } = String.Empty;

    public string Email { get; set; } = String.Empty;

    public string Password { get; set; } = String.Empty;

    public string? UserSecret { get; set; }

    public DateTime JoinedTime { get; set; }

    public bool IsProfileBuilt { get; set; } = false;

    public bool IsDeleted { get; set; } = false;

    public string RefreshToken { get; set; } = String.Empty;

    public DateTime ExpirationDate { get; set; }

    [Column(TypeName = "jsonb")]
    public UserRole Role { get; set; } = UserRole.Sapling;

    public string? ProfilePhoto { get; set; } = String.Empty;

    [Column(TypeName = "jsonb")]
    public List<ConnectedAccount>? ConnectedAccountList { get; set; } = new();
}