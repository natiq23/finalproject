﻿namespace AppDomain.Entities.NotificationRelated.AdditionalDetails;

public class ProductPurchaseDetails
{
    public string BuyerId { get; set; }
    public string ProductId { get; set; }
    public int PurchaseQuantity { get; set; }
    public int TotalSum { get; set; }
}
