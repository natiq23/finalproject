﻿namespace AppDomain.Entities.NotificationRelated.AdditionalDetails;

public class AnswerDetails
{
    public string AnswerId { get; set; }
    public string AnswererId { get; set; }
    public string QuestionId { get; set; }
}