﻿using AppDomain.Enums;

namespace AppDomain.ValueObjects;

public class Settings
{
    //public UiFont Font { get; set; }
    public UiTheme Theme { get; set; } = UiTheme.System;

    public int ContentPerPage { get; set; } = 20;
}
