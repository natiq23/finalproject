﻿namespace AppDomain.Exceptions.UserExceptions;

public class UserForbiddenException : Exception
{
    public UserForbiddenException(string userId, string process)
        : base($"User with ID: {userId} access denied for {process}.") { }
    public UserForbiddenException(string userId, string process, DateTime dateTime)
        : base($"User with ID: {userId} access denied for {process} until {dateTime.ToString("MM/dd/yyyy HH:mm")}.") { }
}