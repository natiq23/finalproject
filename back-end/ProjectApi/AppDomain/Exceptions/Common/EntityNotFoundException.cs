﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppDomain.Exceptions.Common;

public class EntityNotFoundException : Exception
{
    public EntityNotFoundException(string selector)
        : base($"Entity not found for - {selector}") { }
}