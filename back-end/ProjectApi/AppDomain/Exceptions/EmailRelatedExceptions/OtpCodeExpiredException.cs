﻿namespace AppDomain.Exceptions.OtpExceptions;

public class OtpCodeExpiredException : Exception
{
    public OtpCodeExpiredException(string email)
        : base($"OTP verification code expired for - {email}") { }
}