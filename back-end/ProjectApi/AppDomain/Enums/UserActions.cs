﻿namespace AppDomain.Enums;

public enum UserActions
{
    Deleted,
    Registered,
    Updated,
    Follows,
    UnFollows,
    Frozen,
    Unfrozen,
    Changed,
    Added,
    Reported,
    Built
}