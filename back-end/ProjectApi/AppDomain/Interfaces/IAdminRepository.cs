﻿using AppDomain.DTOs.Admin;

namespace AppDomain.Interfaces;

public interface IAdminRepository
{
    /// <summary>
    /// Asynchronously posts a new banner with the provided details.
    /// </summary>
    /// <param name="bannerDTO">The DTO containing banner details to be posted.</param>
    /// <returns>An asynchronous task representing the completion of the operation.</returns>
    public Task<Task> PostBannerAsync(BannerDTO bannerDTO);

    /// <summary>
    /// Asynchronously deletes a banner with the specified ID.
    /// </summary>
    /// <param name="bannerId">The ID of the banner to be deleted.</param>
    /// <returns>An asynchronous task representing the completion of the operation.</returns>
    public Task<Task> DeleteBannerAsync(string bannerId);

    /// <summary>
    /// Asynchronously updates the details of an existing banner.
    /// </summary>
    /// <param name="bannerId">The ID of the banner to be updated.</param>
    /// <param name="photo">The new photo URL for the banner (nullable).</param>
    /// <param name="url">The new URL link for the banner (nullable).</param>
    /// <param name="title">The new title for the banner (nullable).</param>
    /// <param name="description">The new description for the banner (nullable).</param>
    /// <param name="validUntil">The new validity expiration date for the banner (nullable).</param>
    /// <returns>An asynchronous task representing the completion of the operation.</returns>
    public Task<Task> UpdateBannerAsync(
        string bannerId, string? photo, string? url,
        string? title, string? description, DateTime? validUntil);
}