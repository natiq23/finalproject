﻿using AppDomain.DTOs.Pagination;
using AppDomain.DTOs.Question;
using AppDomain.Entities.ContentRelated;
using AppDomain.Enums;

namespace AppDomain.Interfaces;

public interface IQuestionRepository
{
    Task<PaginatedListDto<GetAllQuestionDTO>> GetAllQuestion(
        string? searchQuery,
        List<string>? categoryIdList,
        SortOptions sort,
        int page = 1
    );
    Task<IEnumerable<GetLastQuestionsDTO>> GetLastQuestionsAsync();
    Task<string> InsertQuestion(Question question, List<string> CategoriesIdList);
    Task<string> UpdateQuestion(UpdateQuestionDTO question, string tokenId);
    Task<int> DeleteQuestion(string questionId, string tokenId);
    Task<string> SetCorrectAnswer(string questionId, string answerId, string tokenId);
    Task<QuestionPreviewDTO> GetQuestionPreview(string questionId, string tokenId);
    Task<GetByIdQuestionDTO> GetQuestionById(string questionId, string tokenId);
    Task<int> SaveQuestion(string questionId, string tokenId);
    Task<int> WatchQuestion(string questionId, string tokenId);
    Task<int> VoteQuestion(VoteStatus status, string questionId, string tokenId);
    Task<int> GetAllQuestionCount();
    Task<PaginatedListDto<GetAllQuestionDTO>> GetAllSavedQuestion(
        string? searchQuery,
        List<string>? categoryIdList,
        SortOptions sort,
        int page = 1);

    Task<PaginatedListDto<GetAllQuestionDTO>> GetAllWatchedQuestion(
        string? searchQuery,
        List<string>? categoryIdList,
        SortOptions sort,
        int page = 1);

    Task<PaginatedListDto<GetAllQuestionDTO>> GetAllUserQuestion(
        string? searchQuery,
        List<string>? categoryIdList,
        SortOptions sort,
        int page = 1);
}