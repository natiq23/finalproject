﻿using AppDomain.DTOs.Auth;
using AppDomain.Entities.UserRelated;

namespace AppDomain.Interfaces;

/// <summary>
/// Represents a repository for authentication-related operations.
/// </summary>
public interface IAuthRepository
{
    Task<UserAuthDto> EnterWitProvider(EnterWithProviderDTO user);

    /// <summary>
    /// Registers a new user asynchronously and returns the generated user ID.
    /// </summary>
    /// <exception cref="EntityExistException"/>
    Task<UserAuthDto> RegisterUserAsync(InsertPendingUserDTO user);

    /// <summary>
    /// Logs in a user asynchronously and returns the generated token ID.
    /// </summary>
    /// <exception cref="EntityNotFoundException"/>
    /// <exception cref="UserInvalidPasswordException"/>
    Task<UserAuthDto> LogInAsync(string email, string password);

    /// <summary>
    /// Builds and returns a user object asynchronously based on provided information.
    /// </summary>
    Task<UserBuildResponseDTO> BuildUserAsync(BuildUserDTO buildUser);

    /// <summary>
    /// Sends an OTP code to the user's email for verification.
    /// </summary>
    /// <param name="toEmail">The email address of the user.</param>
    /// <returns>A task representing the asynchronous operation to send the OTP code.</returns>
    //Task<Task> SendOTPCodeAsync(string email, string name);
    Task<Task> SendOTPCodeAsync(string toEmail, string toName);

    /// <summary>
    /// Deletes an email verification record asynchronously.
    /// </summary>
    /// <param name="email">The email address associated with the verification.</param>
    /// <param name="otpCode">The one-time password code used for verification.</param>
    /// <returns>A task representing the asynchronous operation. A string indicating the result of the deletion.</returns>
    Task<bool> VerifyEmailAsync(string email, string otpCode);

    /// <summary>
    /// Updates the user's authentication token asynchronously.
    /// </summary>
    Task<string> UpdateTokenAsync(string email, string refeshtoken);

    /// <summary>
    /// Retrieves the currently authenticated user based on their user ID claim.
    /// </summary>
    /// <returns>
    /// A task that represents the asynchronous operation. The task result is the currently authenticated
    /// user if found, or null if an exception occurs or the user is not authenticated.
    /// </returns>
    /// <remarks>
    /// This method fetches the user with associated data (e.g., saved items, votes, views) based on the user's ID claim.
    /// If the user is authenticated and found in the database, the method returns the user; otherwise, it returns null.
    /// </remarks>
    Task<User> GetCurrentUser();

    /// <summary>
    /// Retrieves the value of a specific claim type associated with the user.
    /// </summary>
    /// <param name="claimType">The type of the claim to retrieve.</param>
    /// <returns>The value of the specified claim, or null if the claim is not found.</returns>
    string GetClaimValue(string claimType);
}
