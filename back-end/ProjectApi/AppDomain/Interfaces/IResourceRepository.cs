﻿using AppDomain.DTOs.Pagination;
using AppDomain.DTOs.Resource;
using AppDomain.Entities.ContentRelated;
using AppDomain.Enums;

namespace AppDomain.Interfaces;

public interface IResourceRepository
{
    Task<int> GetResourcesCount();

    Task<ResourceResponse> GetResource(string resourceId);

    Task<PaginatedListDto<ResourceResponse>> GetAllResources(
        string? searchQuery,
        string? categoryId,
        string? flagId,
        SortOptions sort,
        int page = 1
    );

    Task<List<ResourceResponse>> GetAllUserResources();

    Task<PaginatedListDto<ResourceResponse>> GetAllSavedResources(
        string? searchQuery,
        string? categoryId,
        string? flagId,
        SortOptions sort,
        int page = 1
    );
    Task<IEnumerable<GetLastResourcesDTO>> GetLastResourcesAsync();
    Task<string> InsertResource(Resource resource);
    Task<string> DeleteResource(string id);
    Task<int> SaveResource(string resourceId, string tokenId);
    Task<Task> VisitedUrl(string resourceId);
    Task<int> VoteResource(VoteStatus status, string resourceId, string tokenId);
    Task<string> EditResource(EditResourceDTO editResourceDTO);
}