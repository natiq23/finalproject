﻿
using AppDomain.Enums;

namespace AppDomain.Interfaces;

public interface IModeratorRepository
{
    Task<Task> FreezeContentAsync(string contentId, ContentType contentType, string responsibleModeratorId);
    Task<Task> FreezeUserAsync(string userId, DateTime frozenUntil);
    Task<Task> UnfreezeUserAsync(string userId);
}