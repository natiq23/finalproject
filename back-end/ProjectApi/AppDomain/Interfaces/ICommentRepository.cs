﻿using AppDomain.DTO;
using AppDomain.Enums;

namespace AppDomain.Interfaces;

/// <summary>
/// An interface for managing comments.
/// </summary>
public interface ICommentRepository
{
    /// <summary>
    /// Inserts a comment asynchronously.
    /// </summary>
    /// <param name="commentBody">The content of the comment.</param>
    /// <param name="contentId">The identifier of the content associated with the comment.</param>
    /// <returns>An asynchronous task representing the operation completion.</returns>
    Task<CommentDTO> InsertCommentAsync(string commentBody, string contentId, string? parentCommentId = null);

    /// <summary>
    /// Deletes a comment asynchronously.
    /// </summary>
    /// <param name="commentId">The identifier of the comment to be deleted.</param>
    /// <returns>An asynchronous task representing the operation completion.</returns>
    Task<Task> DeleteCommentAsync(string commentId);

    /// <summary>
    /// Retrieves all comments associated with a specific content synchronously.
    /// </summary>
    /// <param name="contentId">The identifier of the content.</param>
    /// <returns>A collection of comment data transfer objects.</returns>
    Task<IEnumerable<CommentDTO>> GetAllCommentsAsync(string contentId);

    /// <summary>
    /// Edits a comment asynchronously.
    /// </summary>
    /// <param name="commentId">The identifier of the comment to be edited.</param>
    /// <param name="content">The updated content of the comment.</param>
    /// <returns>An asynchronous task representing the operation completion.</returns>
    Task<Task> EditCommentAsync(string commentId, string content);

    Task<Task> DeleteArticleCommentsAsync(string articleId);

    Task VoteCommentAsync(VoteStatus status, string commentId);
}
