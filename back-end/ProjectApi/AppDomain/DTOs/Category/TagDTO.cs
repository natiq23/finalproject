﻿namespace AppDomain.DTOs.Category;

public class TagDTO
{
    public string Id { get; set; }
    public string Title { get; set; }
    public string AccentColor { get; set; }
}