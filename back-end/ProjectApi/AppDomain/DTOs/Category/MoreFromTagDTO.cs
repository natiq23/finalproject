﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppDomain.DTOs.Category
{
    public class MoreFromTagDTO
    {
        public string Title { get; set; }
    }
}