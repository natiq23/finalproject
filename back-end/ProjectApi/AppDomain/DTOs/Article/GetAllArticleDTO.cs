﻿using AppDomain.DTO;
using AppDomain.DTOs.Category;
using AppDomain.Responses.UserResponses;
using AppDomain.ValueObjects;

namespace AppDomain.DTOs.Article;

public class GetAllArticleDTO
{
    public string Id { get; set; }
    public UserContentTitleResponse UserContent { get; set; }
    public Header Header { get; set; }
    public List<TagDTO> ArticleCategories { get; set; } = new();
    public TagDTO ArticleFlag { get; set; }
    public DateTime UpdateTime { get; set; }
    public int ReadingTime { get; set; }
    public int CommentCount { get; set; }
    public VoteDTO VoteDTO { get; set; }
    public bool IsSaved { get; set; }
    public bool IsViewed { get; set; }
}
