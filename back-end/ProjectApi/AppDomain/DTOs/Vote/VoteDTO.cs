﻿using AppDomain.Enums;

namespace AppDomain.DTO;

public class VoteDTO
{
    public int VoteCount { get; set; }
    public VoteStatus Status { get; set; }
}