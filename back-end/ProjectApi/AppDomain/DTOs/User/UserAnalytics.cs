﻿namespace AppDomain.DTOs.User;

public class UserAnalytics
{
    // Votes
    public double Votes { get; set; }
    public double UpVotes { get; set; }
    public double DownVotes { get; set; }
    public double VoteRate { get; set; }

    // Articles
    public int SavedArticles { get; set; }
    public int UpVotedArticles { get; set; }
    public int DownVotedArticles { get; set; }
    public int ArticlesViews { get; set; }

    // Questions
    public int SavedQuestions { get; set; }
    public int WatchedQuestions { get; set; }
    public int UpVotedQuestions { get; set; }
    public int DownVotedQuestions { get; set; }
    public int QuestionsViews { get; set; }

    // Resources
    public int SavedResources { get; set; }
    public int VisitedQuestions { get; set; }
    public int UpVotedResources { get; set; }
    public int DownVotedResources { get; set; }
}
