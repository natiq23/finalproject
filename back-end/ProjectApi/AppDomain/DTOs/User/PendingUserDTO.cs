﻿namespace AppDomain.DTOs.User;

public class PendingUserDTO
{
    public string Name { get; set; } = String.Empty;
    public string Email { get; set; } = String.Empty;
    public string Password { get; set; } = String.Empty;
    public DateTime JoinedTime { get; set; }
}