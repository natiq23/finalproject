﻿namespace AppDomain.Common.Config;

public class AzureConfig
{
    public string ConnectionString { get; set; } = string.Empty;
    public string AvatarContainerName { get; set; } = string.Empty;
    public string TagsContainerName { get; set; } = string.Empty;
    public string CoverImages { get; set; } = string.Empty;

    public string ProductPhotos { get; set; } = string.Empty;
}
