﻿namespace AppDomain.Common.Config;

public class EmailConfig
{
    public string ApiKey { get; set; }
    public string Subject { get; set; }
    public string SenderEmail { get; set; }
    public string SenderName { get; set; }
    public int TemplateId { get; set; }
}