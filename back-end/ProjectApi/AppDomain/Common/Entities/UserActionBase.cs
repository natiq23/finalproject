﻿using AppDomain.Enums;

namespace AppDomain.Common.Entities;

public class UserActionBase
{
    public string Id { get; set; }
    public string UserId { get; set; }
    public string ContentId { get; set; }
    public ContentType ContentType { get; set; }
    public bool IsDeleted { get; set; }
}