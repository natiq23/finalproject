﻿using AppDomain.Entities.UserRelated;
using AppDomain.Enums;
using AppDomain.ValueObjects;

namespace AppDomain.Responses;

public class CustomizationSettings
{
    public UiTheme? Theme { get; set; }

    public int? ContentPerPage { get; set; }

    public string? AccentColor { get; set; }

    public string? BannerPhoto { get; set; }
}
