﻿using AppDomain.DTOs.Answer;
using AppDomain.DTOs.Question;

namespace AppDomain.Responses;

public class AnswerNotificationResponse
{
    public DateTime CreatedAt { get; set; }
    public GetByIdAnswerDTO AnswerDTO { get; set; }
    public GetAllQuestionDTO QuestionDTO { get; set; }
}