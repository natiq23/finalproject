﻿namespace AppDomain.Responses;

public class LeaderboardResponse
{
    public string UserId { get; set; }
    public string Name { get; set; }
    public string Username { get; set; }
    public string ProfilePhoto { get; set; }
    public int BudsPoints { get; set; }
    public int? Rank { get; set; } = null;
}