﻿using AppDomain.DTOs.Pagination;
using AppDomain.DTOs.Product;
using AppDomain.Interfaces;
using Application.Tasks.Commands.Delete.DeleteProduct;
using Application.Tasks.Commands.Insert.InsertProduct;
using Application.Tasks.Commands.Update.UpdateProduct;
using Application.Tasks.Commands.Update.UpdateProduct.Buy;
using Application.Tasks.Commands.Update.UpdateProduct.FavouriteProduct;
using Application.Tasks.Queries.ProductQueries.GetAll;
using Application.Tasks.Queries.ProductQueries.GetAllRank;
using Application.Tasks.Queries.ProductQueries.GetById;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IMediator _mediator;

        private readonly IProductRepository _repository;

        public ProductController(IMediator mediator, IProductRepository repository)
        {
            _mediator = mediator;
            _repository = repository;
        }

        // GET: api/<ProductController>
        [HttpGet("GetAllProducts")]
        public async Task<ActionResult<PaginatedListDto<GetAllProductDTO>>> GetAllProducts(
            [FromQuery] GetAllProductQuery query
        )
        {
            try
            {
                var data = await _mediator.Send(query);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpGet("{ProductId}")]
        public async Task<ActionResult<GetByIdProductDTO>> GetByIdProduct(GetByIdProductQuery query)
        {
            try
            {
                var data = await _mediator.Send(query);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpPost("InsertProduct")]
        public async Task<ActionResult> InsertProduct([FromBody] InsertProductCommand command)
        {
            try
            {
                await _mediator.Send(command);
                return Ok("Product Inserted process is successfully");
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpPatch("Favourite/{ProductId}")]
        public async Task<ActionResult> FavouriteProduct(FavouriteProductCommand command)
        {
            try
            {
                await _mediator.Send(command);
                return Ok();
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpGet("GetAllFavouriteProducts")]
        public async Task<ActionResult<PaginatedListDto<GetAllProductDTO>>> GetAllFavouriteProducts(
            [FromQuery] GetAllFavouriteProductQuery query
        )
        {
            try
            {
                var data = await _mediator.Send(query);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpDelete("DeleteProduct/{ProductId}")]
        public async Task<ActionResult> DeleteProduct(DeleteProductCommand command)
        {
            try
            {
                await _mediator.Send(command);
                return Ok();
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }


        [HttpPut("UpdateProduct")]
        public async Task<ActionResult> UpdateProduct([FromBody] UpdateProductCommand command)
        {
            try
            {
                await _mediator.Send(command);
                return Ok("Product updated process successfully");
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpPut("BuyProduct")]
        public async Task<ActionResult> BuyProduct([FromBody] BuyProductCommand command)
        {
            try
            {
                await _mediator.Send(command);
                return Ok("Buy product process is successfully");
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }

        [HttpGet("GetAllRankCount/{ProductId}")]
        public async Task<ActionResult<Dictionary<string,int>>> GetAllRankCount(GetAllRankCommand command)
        {
            try
            {
                var data = await _mediator.Send(command);
                return Ok(data);
            }
            catch (Exception ex)
            {
                return Problem(ex.Message);
            }
        }
    }
}
