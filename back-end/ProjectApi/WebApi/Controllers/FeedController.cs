﻿using AppDomain.Entities.ContentRelated;
using AppDomain.Entities.NotificationRelated;
using AppDomain.Entities.TagBaseRelated;
using AppDomain.Enums;
using Infrastructure.Persistence;
using Infrastructure.Services;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers;

/// <summary>
/// Controller for feeding various types of data into the application.
/// </summary>
[Route("api/[controller]")]
[ApiController]
public class FeedController : ControllerBase
{
    private readonly Feed _feed;

    /// <summary>
    /// Initializes a new instance of the <see cref="FeedController"/> class.
    /// </summary>
    /// <param name="feed">The feed service responsible for populating the application with data.</param>
    public FeedController(ClubrickDbContext context)
    {
        _feed = new Feed(context);
    }

    [HttpPost("FeedNotification")]
    public async Task<ActionResult<IEnumerable<Notification>>> FeedNotification(int numberOfNotifications)
    {
        try
        {
            var notifications = await _feed.FeedNotification(numberOfNotifications);

            return Ok(notifications);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Feeds tags of the specified type into the application.
    /// </summary>
    /// <param name="tagType">The type of tags to feed.</param>
    /// <param name="numberOfTags">The number of tags to generate and feed.</param>
    /// <returns>
    /// An asynchronous task representing the HTTP action result.
    /// If successful, it returns an OK result with the generated tags.
    /// If there is an error, it returns a problem result with an error message.
    /// </returns>
    [HttpPost("FeedTag")]
    public async Task<ActionResult<IEnumerable<TagBase>>> FeedTag(TagType tagType, int numberOfTags)
    {
        try
        {
            var tags = await _feed.FeedTag(tagType, numberOfTags);

            return Ok(tags);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Feeds answers into the application.
    /// </summary>
    /// <param name="numberOfAnswers">The number of answers to generate and feed.</param>
    /// <returns>
    /// An asynchronous task representing the HTTP action result.
    /// If successful, it returns an OK result with the generated answers.
    /// If there is an error, it returns a problem result with an error message.
    /// </returns>
    [HttpPost("FeedAnswer")]
    public async Task<ActionResult<IEnumerable<Answer>>> FeedAnswer(int numberOfAnswers)
    {
        try
        {
            var answers = await _feed.FeedAnswer(numberOfAnswers);

            return Ok(answers);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Feeds questions into the application.
    /// </summary>
    /// <param name="numberOfQuestions">The number of questions to generate and feed.</param>
    /// <returns>
    /// An asynchronous task representing the HTTP action result.
    /// If successful, it returns an OK result with the generated questions.
    /// If there is an error, it returns a problem result with an error message.
    /// </returns>
    [HttpPost("FeedQuestion")]
    public async Task<ActionResult<IEnumerable<Question>>> FeedQuestion(int numberOfQuestions)
    {
        try
        {
            var questions = await _feed.FeedQuestion(numberOfQuestions);

            return Ok(questions);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Feeds comments into the application.
    /// </summary>
    /// <param name="numberOfComments">The number of comments to generate and feed.</param>
    /// <returns>
    /// An asynchronous task representing the HTTP action result.
    /// If successful, it returns an OK result with the generated comments.
    /// If there is an error, it returns a problem result with an error message.
    /// </returns>
    [HttpPost("FeedComment")]
    public async Task<ActionResult<IEnumerable<Comment>>> FeedComment(int numberOfComments)
    {
        try
        {
            var comments = await _feed.FeedComment(numberOfComments);

            return Ok(comments);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Feeds resources into the application.
    /// </summary>
    /// <param name="numberOfResources">The number of resources to generate and feed.</param>
    /// <returns>
    /// An asynchronous task representing the HTTP action result.
    /// If successful, it returns an OK result with the generated resources.
    /// If there is an error, it returns a problem result with an error message.
    /// </returns>
    [HttpPost("FeedResource")]
    public async Task<ActionResult<IEnumerable<Resource>>> FeedResource(int numberOfResources)
    {
        try
        {
            var resources = await _feed.FeedResource(numberOfResources);

            return Ok(resources);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }
}