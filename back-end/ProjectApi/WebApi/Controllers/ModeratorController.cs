﻿using AppDomain.Interfaces;
using AppDomain.Responses.UserResponses;
using Application.Tasks.Commands.Delete.DeleteContent;
using Application.Tasks.Commands.Update.UpdateUser.FreezeUser;
using Application.Tasks.Commands.Update.UpdateUser.RestoreUser;
using Application.Tasks.Commands.Update.UpdateUser.UnfreezeUser;
using MediatR;
using Microsoft.AspNetCore.Mvc;


namespace WebApi.Controllers;

/// <summary>
/// Controller responsible for handling moderator-related actions.
/// </summary>
[Route("api/[controller]")]
[ApiController]
public class ModeratorController : ControllerBase
{
    private readonly IMediator _mediator;
    private readonly IUserRepository _userRepository;

    /// <summary>
    /// Initializes a new instance of the <see cref="ModeratorController"/> class.
    /// </summary>
    /// <param name="mediator">The mediator for handling mediator-related commands and queries.</param>
    /// <param name="userRepository">The repository for user-related operations.</param>
    public ModeratorController(IMediator mediator, IUserRepository userRepository)
    {
        _mediator = mediator;
        _userRepository = userRepository;
    }

    [HttpPatch("FreezeUser")]
    public async Task<ActionResult> FreezeUser([FromBody] FreezeUserCommand updateCommand)
    {
        try
        {
            var result = await _mediator.Send(updateCommand);

            return Ok(result);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    [HttpPatch("UnfreezeUser")]
    public async Task<ActionResult> UnfreezeUser([FromBody] UnfreezeUserCommand updateCommand)
    {
        try
        {
            var result = await _mediator.Send(updateCommand);

            return Ok(result);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    // This is not for now.
    /*[HttpPatch("ChangeRole")]
    public async Task<ActionResult> ChangeRole([FromBody] UpdateRoleCommand updateCommand)
    {
        try
        {
            var result = await _mediator.Send(updateCommand);

            return Ok(result);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }*/

    [HttpGet("SearchUserByEmail/{email}")]
    public async Task<ActionResult<UserProfileResponse>> SearchUserByEmail(string email)
    {
        try
        {
            var user = await _userRepository.GetUserByEmailAsync(email);

            var userProfile = ModelConvertors.ToUserProfileResponse(user);

            return Ok(userProfile);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    [HttpDelete("FreezeContent/{ContentId}")]
    public async Task<ActionResult> FreezeContent(FreezeContentCommand freezeCommand)
    {
        try
        {
            var result = await _mediator.Send(freezeCommand);

            return Ok(result);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Restores a user's account by executing a command to update user data.
    /// </summary>
    /// <param name="updateCommand">The command to restore the user's account.</param>
    /// <returns>An action result representing the outcome of the operation.</returns>
    [HttpPut("RestoreUser/{Id}")]
    public async Task<ActionResult> RestoreUser(RestoreUserCommand updateCommand)
    {
        try
        {
            var result = await _mediator.Send(updateCommand);

            return Ok(result);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }
}