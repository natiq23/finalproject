﻿using AppDomain.DTO;
using AppDomain.DTOs.Article;
using AppDomain.DTOs.Settings;
using AppDomain.DTOs.User;
using AppDomain.Exceptions.Common;
using AppDomain.Exceptions.UserExceptions;
using AppDomain.Interfaces;
using AppDomain.Responses;
using AppDomain.Responses.UserResponses;
using AppDomain.ValueObjects;
using Application.Tasks.Commands.Delete.UserDeletes.DeleteUser;
using Application.Tasks.Commands.Insert.UserInserts.FollowUser;
using Application.Tasks.Commands.Insert.UserInserts.InsertConnectedAccount;
using Application.Tasks.Commands.Update.UpdateUser.ResetPassword;
using Application.Tasks.Commands.Update.UpdateUser.UpdateCustomization;
using Application.Tasks.Commands.Update.UpdateUser.UpdateDisplayEmail;
using Application.Tasks.Commands.Update.UpdateUser.UpdatePassword;
using Application.Tasks.Commands.Update.UpdateUser.UpdatePersonalSettings;
using Application.Tasks.Commands.Update.UpdateUser.UpdatePinnedArticles;
using Application.Tasks.Commands.Update.UpdateUser.UpdatePinnedRepositories;
using Application.Tasks.Commands.Update.UpdateUser.UpdatePrimaryEmail;
using Application.Tasks.Queries.UserQueries.ConnectedAccountTypeExist;
using Application.Tasks.Queries.UserQueries.GetAllCategoryCount;
using Application.Tasks.Queries.UserQueries.GetAllUsers;
using Application.Tasks.Queries.UserQueries.GetAnswerCount;
using Application.Tasks.Queries.UserQueries.GetArticleCount;
using Application.Tasks.Queries.UserQueries.GetConnectedEmails;
using Application.Tasks.Queries.UserQueries.GetFollowers;
using Application.Tasks.Queries.UserQueries.GetFollowings;
using Application.Tasks.Queries.UserQueries.GetLeaderboard;
using Application.Tasks.Queries.UserQueries.GetMe;
using Application.Tasks.Queries.UserQueries.GetPinnedArticles;
using Application.Tasks.Queries.UserQueries.GetPinnedRepositories;
using Application.Tasks.Queries.UserQueries.GetQuestionCount;
using Application.Tasks.Queries.UserQueries.GetRepositories;
using Application.Tasks.Queries.UserQueries.GetResourceCount;
using Application.Tasks.Queries.UserQueries.GetUserBySelector;
using Application.Tasks.Queries.UserQueries.UserExistByEmail;
using Application.Tasks.Queries.UserQueries.UserExistByUsername;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers;

/// <summary>
/// Controller for managing user-related operations.
/// </summary>
[ApiController]
[Route("api/[controller]")]
public class UsersController : ControllerBase
{
    private readonly IMediator _mediator;
    private readonly IUserRepository _userRepository;

    /// <summary>
    /// Initializes a new instance of the <see cref="UsersController"/> class.
    /// </summary>
    /// <param name="mediator">The mediator used for handling commands and queries.</param>
    /// <param name="userRepository">The repository for user-related operations.</param>
    public UsersController(IMediator mediator, IUserRepository userRepository)
    {
        _mediator = mediator;
        _userRepository = userRepository;
    }


    [HttpGet("GetMe")]
    public async Task<ActionResult<int>> GetMe(
        GetMeQuery query)
    {
        try
        {
            var me = await _mediator.Send(query);

            return Ok(me);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    [HttpGet("GetAllTimeBuds")]
    public async Task<ActionResult<int>> GetAllTimeBuds()
    {
        try
        {
            var buds = await _userRepository.GetAllTimeBudsAsync();

            return Ok(buds);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    [HttpGet("GetUsableBuds")]
    public async Task<ActionResult<int>> GetUsableBuds()
    {
        try
        {
            var buds = await _userRepository.GetUsableBudsAsync();

            return Ok(buds);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    [HttpGet("GetAppSettings")]
    public async Task<ActionResult<AppSettings>> GetAppSettings()
    {
        try
        {
            var settings = await _userRepository.GetAppSettingsAsync();
            return Ok(settings);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Retrieves the leaderboard containing information about top users or content based on the query.
    /// </summary>
    /// <param name="query">The query to specify the type of leaderboard and any additional parameters.</param>
    /// <returns>
    /// HTTP 200 OK with the leaderboard data if successful,
    /// or HTTP 500 Internal Server Error if an error occurs.
    /// </returns>
    [HttpGet("GetLeaderboard")]
    public async Task<ActionResult<IEnumerable<LeaderboardResponse>>> GetLeaderboard(
        GetLeaderboardQuery query
    )
    {
        try
        {
            var leaderboard = await _mediator.Send(query);

            return Ok(leaderboard);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Checks if the user has set a password.
    /// </summary>
    /// <returns>
    /// HTTP 200 OK if the user has set a password,
    /// HTTP 404 Not Found if the user has not set a password,
    /// or HTTP 500 Internal Server Error if an error occurs.
    /// </returns>
    [HttpGet("PasswordIsSet")]
    public async Task<ActionResult<bool>> PasswordIsSet()
    {
        try
        {
            var res = await _userRepository.PasswordIsSet();

            return res ? Ok() : NotFound();
        }
        catch (Exception)
        {
            return Problem("A problem has occurred please try again later");
        }
    }

    /// <summary>
    /// Checks if the current user is following another user.
    /// </summary>
    /// <param name="userId">The ID of the user to check if the current user is following.</param>
    /// <returns>
    /// HTTP 200 OK if the current user is following the specified user,
    /// HTTP 404 Not Found if not following,
    /// or HTTP 500 Internal Server Error if an error occurs.
    /// </returns>
    [HttpGet("IsFollowed/{userId}")]
    public async Task<ActionResult> UserIsFollowed(string userId)
    {
        try
        {
            var res = await _userRepository.UserIsFollowed(userId);

            return res ? Ok() : NotFound();
        }
        catch (Exception)
        {
            return Problem("A problem has occurred please try again later");
        }
    }

    /// <summary>
    /// Retrieves user profiles based on search criteria.
    /// </summary>
    /// <param name="query">The query containing search parameters.</param>
    /// <returns>A list of user profiles that match the search criteria, or a Problem response if an error occurs.</returns>
    [HttpGet("SearchUsers/{Search}")]
    public async Task<ActionResult<IEnumerable<UserProfileResponse>>> SearchUsers(
        GetAllUsersQuery query
    )
    {
        try
        {
            var users = await _mediator.Send(query);

            return Ok(users);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Retrieves a user by a unique selector and identifier asynchronously.
    /// </summary>
    /// <param name="getCommand">The query to retrieve the user.</param>
    /// <returns>
    /// An action result representing the user's information.
    /// If successful, it returns the user's data as a UserDTO; otherwise, it returns a problem details response.
    /// </returns>
    /// <remarks>
    /// This action retrieves a user by a unique selector and identifier based on the provided query.
    /// It performs this operation asynchronously.
    /// If the retrieval is successful, it returns the user's information as a UserDTO with an HTTP 200 (OK) status;
    /// otherwise, it returns a problem details response with an HTTP 404 (Not Found) status in case of an entity not found,
    /// or an HTTP 500 (Internal Server Error) status for other exceptions.
    /// </remarks>
    [HttpGet("GetUserBySelector/{Selector}/{Identifier}")]
    public async Task<ActionResult<UserDTO>> GetUserBySelector(GetUserBySelectorQuery getCommand)
    {
        try
        {
            var user = await _mediator.Send(getCommand);
            var userDTO = ModelConvertors.ToUserDTO(user);

            return Ok(userDTO);
        }
        catch (EntityNotFoundException ex)
        {
            return NotFound(ex.Message);
        }
        catch (Exception)
        {
            return Problem("A problem has occurred please try again later");
        }
    }

    /// <summary>
    /// Retrieves a user's view response by a unique selector and identifier asynchronously.
    /// </summary>
    /// <param name="getCommand">The query to retrieve the user's view response.</param>
    /// <returns>
    /// An action result representing the user's view response.
    /// If successful, it returns the user's view response as a UserViewResponse; otherwise, it returns a problem details response.
    /// </returns>
    /// <remarks>
    /// This action retrieves a user's view response by a unique selector and identifier based on the provided query.
    /// It performs this operation asynchronously.
    /// If the retrieval is successful, it returns the user's view response as a UserViewResponse with an HTTP 200 (OK) status;
    /// otherwise, it returns a problem details response with an HTTP 404 (Not Found) status in case of an entity not found,
    /// or an HTTP 500 (Internal Server Error) status for other exceptions.
    /// </remarks>
    [HttpGet("GetUserViewResponseBySelector/{Selector}/{Identifier}")]
    public async Task<ActionResult<UserViewResponse>> GetUserViewResponseBySelector(
        GetUserBySelectorQuery getCommand
    )
    {
        try
        {
            var user = await _mediator.Send(getCommand);

            var counts = await _userRepository.GetUserViewResponsePublishedCountsAsync(user.Id);

            var userViewResponse = ModelConvertors.ToUserViewResponse(user, counts);

            return Ok(userViewResponse);
        }
        catch (EntityNotFoundException ex)
        {
            return NotFound(ex.Message);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Retrieves a user's preview response by a unique selector and identifier asynchronously.
    /// </summary>
    /// <param name="getCommand">The query to retrieve the user's preview response.</param>
    /// <returns>
    /// An action result representing the user's preview response.
    /// If successful, it returns the user's preview response as a UserPreviewResponse; otherwise, it returns a problem details response.
    /// </returns>
    /// <remarks>
    /// This action retrieves a user's preview response by a unique selector and identifier based on the provided query.
    /// It performs this operation asynchronously.
    /// If the retrieval is successful, it returns the user's preview response as a UserPreviewResponse with an HTTP 200 (OK) status;
    /// otherwise, it returns a problem details response with an HTTP 404 (Not Found) status in case of an entity not found,
    /// or an HTTP 500 (Internal Server Error) status for other exceptions.
    /// </remarks>
    [HttpGet("GetUserPreviewResponseBySelector/{Selector}/{Identifier}")]
    public async Task<ActionResult<UserPreviewResponse>> GetUserPreviewResponseBySelector(
        GetUserBySelectorQuery getCommand
    )
    {
        try
        {
            var user = await _mediator.Send(getCommand);

            var userPreviewResponse = ModelConvertors.ToUserPreviewResponse(user);

            userPreviewResponse.IsUserFollowed = await _userRepository.IsCurrentUserFollowAsync(
                user.Email
            );

            return Ok(userPreviewResponse);
        }
        catch (EntityNotFoundException ex)
        {
            return NotFound(ex.Message);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Retrieves a user's content title response by a unique selector and identifier asynchronously.
    /// </summary>
    /// <param name="getCommand">The query to retrieve the user's content title response.</param>
    /// <returns>
    /// An action result representing the user's content title response.
    /// If successful, it returns the user's content title response as a UserContentTitleResponse; otherwise, it returns a problem details response.
    /// </returns>
    /// <remarks>
    /// This action retrieves a user's content title response by a unique selector and identifier based on the provided query.
    /// It performs this operation asynchronously.
    /// If the retrieval is successful, it returns the user's content title response as a UserContentTitleResponse with an HTTP 200 (OK) status;
    /// otherwise, it returns a problem details response with an HTTP 404 (Not Found) status in case of an entity not found,
    /// or an HTTP 500 (Internal Server Error) status for other exceptions.
    /// </remarks>
    [HttpGet("GetUserContentTitleResponseBySelector/{Selector}/{Identifier}")]
    public async Task<ActionResult<UserPreviewResponse>> GetUserContentTitleResponseBySelector(
        GetUserBySelectorQuery getCommand
    )
    {
        try
        {
            var user = await _mediator.Send(getCommand);

            var userContentTitleResponse = ModelConvertors.ToUserContentTitleResponse(user);

            return Ok(userContentTitleResponse);
        }
        catch (EntityNotFoundException ex)
        {
            return NotFound(ex.Message);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Retrieves the user's profile information.
    /// </summary>
    /// <returns>The user's profile information if available, Problem response if no user profile exists.</returns>
    [HttpGet("GetUserProfile")]
    public async Task<ActionResult<UserProfileResponse>> GetUserProfile()
    {
        try
        {
            var profile = await _userRepository.GetUserProfileAsync();

            return Ok(profile);
        }
        catch (EntityNotFoundException ex)
        {
            return NotFound(ex.Message);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Retrieves customization information asynchronously.
    /// </summary>
    /// <returns>A task representing the asynchronous operation. Customization information if available, Problem response if not found.</returns>
    [HttpGet("GetCustomizationSettings")]
    public async Task<ActionResult<CustomizationSettings>> GetCustomizationSettings()
    {
        try
        {
            var customization = await _userRepository.GetCustomizationAsync();

            return Ok(customization);
        }
        catch (EntityNotFoundException ex)
        {
            return NotFound(ex.Message);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Retrieves profile settings page information asynchronously.
    /// </summary>
    /// <returns>A task representing the asynchronous operation. The personal information response if available, Problem response if no personal information exists.</returns>
    [HttpGet("GetProfileSettings")]
    public async Task<ActionResult<ProfileSettingsDTO>> GetProfileSettings()
    {
        try
        {
            var settings = await _userRepository.GetProfileSettingsAsync();

            return Ok(settings);
        }
        catch (EntityNotFoundException ex)
        {
            return NotFound(ex.Message);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Check for user exist for given email address
    /// </summary>
    /// <returns></returns>
    [HttpGet("UserExistByEmail/{Email}")]
    public async Task<ActionResult> UserExistByEmail(UserExistByEmailQuery query)
    {
        try
        {
            return await _mediator.Send(query) ? Ok() : NotFound();
        }
        catch (Exception)
        {
            return Problem("A problem has occurred please try again later");
        }
    }

    /// <summary>
    /// Check for user exist for given username
    /// </summary>
    /// <returns></returns>
    [HttpGet("UserExistByUsername/{Username}")]
    public async Task<ActionResult> UserExistByUsername(UserExistByUsernameQuery query)
    {
        try
        {
            return await _mediator.Send(query) ? Ok() : NotFound();
        }
        catch (Exception)
        {
            return Problem("A problem has occurred please try again later");
        }
    }

    /// <summary>
    /// Updates the password of  user.
    /// </summary>
    /// <param name="updateCommand">The command containing update information.</param>
    [HttpPatch("UpdatePassword")]
    public async Task<ActionResult> UpdatePassword([FromBody] UpdatePasswordCommand updateCommand)
    {
        try
        {
            await _mediator.Send(updateCommand);

            return Ok();
        }
        catch (EntityNotFoundException ex)
        {
            return NotFound(ex.Message);
        }
        catch (UserInvalidPasswordException ex)
        {
            return BadRequest(ex.Message);
        }
        catch (Exception)
        {
            return Problem("A problem has occurred please try again later");
        }
    }

    /// <summary>
    /// Updates the password of  user.
    /// </summary>
    /// <param name="updateCommand">The command containing update information.</param>
    [HttpPatch("UpdatePrimaryEmail")]
    public async Task<ActionResult> UpdatePrimaryEmail(
        [FromBody] UpdatePrimaryEmailCommand updateCommand
    )
    {
        try
        {
            await _mediator.Send(updateCommand);

            return Ok();
        }
        catch (EntityNotFoundException ex)
        {
            return NotFound(ex.Message);
        }
        catch (EntityExistException ex)
        {
            return Conflict(ex.Message);
        }
        catch (Exception)
        {
            return Problem("A problem has occurred please try again later");
        }
    }

    /// <summary>
    /// Updates the user's profile settings page information asynchronously.
    /// </summary>
    /// <param name="updateCommand">The command containing updated personal information.</param>
    /// <returns>A task representing the asynchronous operation. The updated personal information if successful, BadRequest if update fails.</returns>
    [HttpPatch("UpdateProfileSettings")]
    public async Task<ActionResult<ProfileSettingsDTO>> UpdateProfileSettings(
        [FromBody] UpdateProfileSettingsCommand updateCommand
    )
    {
        try
        {
            var personalInfo = await _mediator.Send(updateCommand);

            return Ok(personalInfo);
        }
        catch (EntityNotFoundException ex)
        {
            return NotFound(ex.Message);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Updates customization information asynchronously.
    /// </summary>
    /// <param name="updateCommand">The command containing the updated customization information.</param>
    /// <returns>A task representing the asynchronous operation. The updated customization information if successful, BadRequest if the update fails.</returns>
    [HttpPatch("UpdateCustomizationSettings")]
    public async Task<ActionResult<CustomizationSettings>> UpdateCustomizationSettings(
        [FromBody] UpdateCustomizationSettingsCommand updateCommand
    )
    {
        try
        {
            var customization = await _mediator.Send(updateCommand);

            return Ok(customization);
        }
        catch (EntityNotFoundException ex)
        {
            return NotFound(ex.Message);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Reset the password of  user.
    /// </summary>
    /// <param name="resetCommand">The command containing update information.</param>
    [HttpPatch("ResetPassword")]
    public async Task<ActionResult> ResetPassword([FromBody] ResetPasswordCommand resetCommand)
    {
        try
        {
            await _mediator.Send(resetCommand);

            return Ok();
        }
        catch (EntityNotFoundException ex)
        {
            return NotFound(ex.Message);
        }
        catch (Exception)
        {
            return Problem("A problem has occurred please try again later");
        }
    }

    /// <summary>
    /// Deletes a user.
    /// </summary>
    /// <param name="deleteCommand">The command containing user deletion information.</param>
    /// <returns>Ok if deletion is successful.</returns>
    [HttpDelete("delete/{Id}")]
    public async Task<ActionResult> DeleteUser(DeleteUserCommand deleteCommand)
    {
        try
        {
            var user = await _mediator.Send(deleteCommand);

            var userDTO = ModelConvertors.ToUserDTO(user);

            return Ok(userDTO);
        }
        catch (EntityNotFoundException ex)
        {
            return NotFound(ex.Message);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Retrieves all repositories associated with a user by sending a query.
    /// </summary>
    /// <param name="repoQuery">The query object containing user-related information.</param>
    /// <returns>
    /// An HTTP response with a collection of repositories if successful (HTTP 200 OK),
    /// or an error response if an exception occurs (HTTP 500 Internal Server Error).
    /// </returns>
    [HttpGet("GetAllRepositories")]
    public async Task<ActionResult> GetAllRepositories(GetAllRepositoriesQuery repoQuery)
    {
        try
        {
            var repos = await _mediator.Send(repoQuery);

            return Ok(repos);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Gets pinned repositories for a user by their user ID.
    /// </summary>
    /// <param name="query">The query to retrieve pinned repositories for a user.</param>
    /// <returns>
    /// An asynchronous task representing the HTTP action result.
    /// If successful, it returns an OK result with a list of pinned repositories.
    /// If there is an error, it returns a problem result with an error message.
    /// </returns>
    /// <remarks>
    /// This method allows users to retrieve their pinned repositories by providing a query.
    /// It performs this operation asynchronously and returns an HTTP action result.
    /// If the operation is successful, it returns an OK result with a list of pinned repositories.
    /// If there is an error, it returns a problem result with an error message.
    /// </remarks>
    [HttpGet("GetPinnedRepositoriesById/{userId}")]
    public async Task<ActionResult<IEnumerable<PinnedRepository>>> GetPinnedRepositoriesById(
        GetPinnedRepositoriesQuery query
    )
    {
        try
        {
            var pinnedRepositories = await _mediator.Send(query);

            return Ok(pinnedRepositories);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Retrieves a collection of pinned articles based on a query.
    /// </summary>
    /// <param name="query">The query containing parameters for retrieving pinned articles.</param>
    /// <returns>An ActionResult representing a collection of DTOs for the pinned articles.</returns>
    [HttpGet("GetPinnedArticlesById/{UserId}")]
    public async Task<ActionResult<IEnumerable<GetAllArticleDTO>>> GetPinnedArticlesById(
        GetPinnedArticlesQuery query
    )
    {
        try
        {
            var pinnedArticles = await _mediator.Send(query);

            return Ok(pinnedArticles);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Updates the pinned articles for a user based on the provided command.
    /// </summary>
    /// <param name="updateCommand">The command containing information to update pinned articles.</param>
    /// <returns>An ActionResult representing the result of the update operation.</returns>
    [HttpPatch("UpdatePinnedArticles")]
    public async Task<ActionResult> UpdatePinnedArticles(
        [FromBody] UpdatePinnedArticlesCommand updateCommand
    )
    {
        try
        {
            var result = await _mediator.Send(updateCommand);

            return Ok(result);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Updates pinned repositories for a user using a command.
    /// </summary>
    /// <param name="updateCommand">The command for updating pinned repositories.</param>
    /// <returns>
    /// An HTTP response with a collection of updated pinned repositories if successful (HTTP 200 OK),
    /// or an appropriate error response if an exception occurs.
    /// </returns>
    [HttpPatch("UpdatePinnedRepositories")]
    public async Task<ActionResult<Task>> UpdatePinnedRepositories(
        [FromBody] UpdatePinnedRepositoriesCommand updateCommand
    )
    {
        try
        {
            await _mediator.Send(updateCommand);

            return Ok();
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Updates the user's display email address asynchronously.
    /// </summary>
    /// <param name="updateCommand">The command containing the new email address.</param>
    /// <returns>
    /// An action result representing the result of the update operation. If successful, it returns
    /// the updated display email address; otherwise, it returns a problem details response.
    /// </returns>
    /// <remarks>
    /// This action handles the update of the user's display email address based on the provided command.
    /// If the update is successful, it returns the updated email address with an HTTP 200 (OK) status;
    /// otherwise, it returns a problem details response with an HTTP 500 (Internal Server Error) status.
    /// </remarks>
    [HttpPatch("UpdateDisplayEmail")]
    public async Task<ActionResult<string>> UpdateDisplayEmail(
        [FromBody] UpdateDisplayEmailCommand updateCommand
    )
    {
        try
        {
            var result = await _mediator.Send(updateCommand);

            return Ok(result);
        }
        catch (EntityExistException ex)
        {
            return Conflict(ex.Message);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Follows a user based on the provided command asynchronously.
    /// </summary>
    /// <param name="command">The command containing information for the follow operation.</param>
    /// <returns>
    /// An action result representing the result of the follow operation. If successful, it returns an OK response;
    /// otherwise, it returns a problem details response.
    /// </returns>
    /// <remarks>
    /// This action allows the currently authenticated user to follow another user based on the provided command.
    /// If the follow operation is successful, it returns an HTTP 200 (OK) response; otherwise, it returns a problem
    /// details response with an HTTP 500 (Internal Server Error) status.
    /// </remarks>
    [HttpPatch("FollowUser")]
    public async Task<ActionResult> FollowUser([FromBody] FollowUserCommand command)
    {
        try
        {
            var result = await _mediator.Send(command);

            return Ok(result);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Retrieves a list of user profiles representing the users being followed by the currently authenticated user.
    /// </summary>
    /// <param name="query">The query to retrieve followings.</param>
    /// <returns>
    /// An action result representing the result of the retrieval operation. If successful, it returns a list of user profiles
    /// representing the users being followed by the authenticated user; otherwise, it returns a problem details response.
    /// </returns>
    /// <remarks>
    /// This action handles the retrieval of user profiles for users being followed by the authenticated user.
    /// If the retrieval is successful, it returns the list of user profiles with an HTTP 200 (OK) status;
    /// otherwise, it returns a problem details response with an HTTP 500 (Internal Server Error) status.
    /// </remarks>
    [HttpGet("GetFollowings")]
    public async Task<ActionResult<UserProfileResponse>> GetFollowings(GetFollowingsQuery query)
    {
        try
        {
            var userProfiles = await _mediator.Send(query);

            return Ok(userProfiles);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Retrieves a list of user profiles representing the users who are following the currently authenticated user.
    /// </summary>
    /// <param name="query">The query to retrieve followers.</param>
    /// <returns>
    /// An action result representing the result of the retrieval operation. If successful, it returns a list of user profiles
    /// representing the users who are following the authenticated user; otherwise, it returns a problem details response.
    /// </returns>
    /// <remarks>
    /// This action handles the retrieval of user profiles for users who are following the authenticated user.
    /// If the retrieval is successful, it returns the list of user profiles with an HTTP 200 (OK) status;
    /// otherwise, it returns a problem details response with an HTTP 500 (Internal Server Error) status.
    /// </remarks>
    [HttpGet("GetFollowers")]
    public async Task<ActionResult<UserProfileResponse>> GetFollowers(GetFollowersQuery query)
    {
        try
        {
            var userProfiles = await _mediator.Send(query);

            return Ok(userProfiles);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Retrieves the count of users following the currently authenticated user asynchronously.
    /// </summary>
    /// <param name="query">The query to retrieve followers.</param>
    /// <returns>
    /// An action result representing the count of users following the authenticated user.
    /// If successful, it returns the count as an integer value; otherwise, it returns a problem details response.
    /// </returns>
    /// <remarks>
    /// This action retrieves the count of users following the currently authenticated user.
    /// It uses the provided query to fetch the followers and then counts them.
    /// If the retrieval is successful, it returns the count as an integer value with an HTTP 200 (OK) status;
    /// otherwise, it returns a problem details response with an HTTP 500 (Internal Server Error) status.
    /// </remarks>
    [HttpGet("GetFollowingsCount")]
    public async Task<ActionResult<int>> GetFollowingsCount(GetFollowingsQuery query)
    {
        try
        {
            var userProfiles = await _mediator.Send(query);

            var count = userProfiles.Count();

            return Ok(count);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Retrieves the count of users who are followers of the currently authenticated user asynchronously.
    /// </summary>
    /// <param name="query">The query to retrieve followers.</param>
    /// <returns>
    /// An action result representing the count of users who are followers of the authenticated user.
    /// If successful, it returns the count as an integer value; otherwise, it returns a problem details response.
    /// </returns>
    /// <remarks>
    /// This action retrieves the count of users who are followers of the currently authenticated user.
    /// It uses the provided query to fetch the followers and then counts them.
    /// If the retrieval is successful, it returns the count as an integer value with an HTTP 200 (OK) status;
    /// otherwise, it returns a problem details response with an HTTP 500 (Internal Server Error) status.
    /// </remarks>
    [HttpGet("GetFollowersCount")]
    public async Task<ActionResult<int>> GetFollowersCount(GetFollowersQuery query)
    {
        try
        {
            var userProfiles = await _mediator.Send(query);

            var count = userProfiles.Count();

            return Ok(count);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Retrieves a collection of connected email addresses asynchronously.
    /// </summary>
    /// <param name="query">The query to retrieve connected emails.</param>
    /// <returns>
    /// An action result representing a collection of connected email addresses.
    /// If successful, it returns the email addresses as a collection; otherwise, it returns a problem details response.
    /// </returns>
    /// <remarks>
    /// This action retrieves a collection of email addresses that are connected to the user's account
    /// based on the provided query. It performs this operation asynchronously.
    /// If the retrieval is successful, it returns the email addresses as a collection with an HTTP 200 (OK) status;
    /// otherwise, it returns a problem details response with an HTTP 500 (Internal Server Error) status.
    /// </remarks>
    [HttpGet("GetConnectedEmails")]
    public async Task<ActionResult<IEnumerable<EmailResponse>>> GetConnectedEmails(
        GetConnectedEmailsQuery query
    )
    {
        try
        {
            var emails = await _mediator.Send(query);

            return Ok(emails);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Checks if a connected account type exists asynchronously.
    /// </summary>
    /// <param name="query">The query to check for the existence of a connected account type.</param>
    /// <returns>
    /// An asynchronous task representing the HTTP action result.
    /// If the connected account type exists, it returns true; otherwise, it returns false.
    /// </returns>
    /// <remarks>
    /// This method checks if a connected account type exists based on the provided query.
    /// It performs this check asynchronously and returns true if the connected account type exists;
    /// otherwise, it returns false.
    /// </remarks>
    [HttpGet("IsConnectedAccountTypeExist/{ConnectedAccountType}")]
    public async Task<ActionResult<bool>> IsConnectedAccountTypeExist(
        ConnectedAccountTypeExistQuery query
    )
    {
        try
        {
            var result = await _mediator.Send(query);

            return result ? Ok() : NotFound();
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    /// <summary>
    /// Adds a connected account asynchronously.
    /// </summary>
    /// <param name="insertCommand">The command to insert a connected account.</param>
    /// <returns>
    /// An asynchronous task representing the HTTP action result.
    /// If the addition is successful, it returns true; otherwise, it returns false.
    /// </returns>
    /// <remarks>
    /// This method allows users to add a connected account by sending a command to insert the connected account.
    /// It performs this operation asynchronously and returns an HTTP action result.
    /// If the addition is successful, it returns true; otherwise, it returns false.
    /// </remarks>
    [HttpPatch("AddConnectedAccount")]
    public async Task<ActionResult<bool>> AddConnectedAccount(
        [FromBody] InsertConnectedAccountCommand insertCommand
    )
    {
        try
        {
            var result = await _mediator.Send(insertCommand);

            return Ok(result);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }


    [HttpGet("GetUserCategoryFollowedsCount")]
    public async Task<ActionResult<int>> GetUserCategoryFollowedsCount()
    {
        try
        {
            var data = await _mediator.Send(new GetAllUserCategoryQuery());
            return Ok(data);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    [HttpGet("GetUserPostsCount")]
    public async Task<ActionResult<int>> GetUserPostsCount()
    {
        try
        {
            var data = await _mediator.Send(new GetArticleCountQuery());
            return Ok(data);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    [HttpGet("GetUserQuestionsCount")]
    public async Task<ActionResult<int>> GetUserQuestionsCount()
    {
        try
        {
            var data = await _mediator.Send(new GetQuestionCountQuery());
            return Ok(data);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }


    [HttpGet("GetUserAnswersCount")]
    public async Task<ActionResult<int>> GetUserAnswersCount()
    {
        try
        {
            var data = await _mediator.Send(new GetAnswerCountQuery());
            return Ok(data);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    [HttpGet("GetUserResourcesCount")]
    public async Task<ActionResult<int>> GetUserResourcesCount()
    {
        try
        {
            var data = await _mediator.Send(new GetResourceCountQuery());
            return Ok(data);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }
}