﻿using AppDomain.Common.Entities;
using AppDomain.DTO;
using Application.Tasks.Commands.Delete.DeleteAnswer;
using Application.Tasks.Commands.Insert.InsertAnswer;
using Application.Tasks.Commands.Update.UpdateAnswer;
using Application.Tasks.Commands.Update.UpdateAnswer.VoteAnswer;
using Application.Tasks.Commands.Update.UpdateResource.ResourceVote;
using Application.Tasks.Queries.AnswerQueries.GetAnswerById;
using Application.Tasks.Queries.AnswerQueries.GetById;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [Route("api/answers")]
    [ApiController]
    public class AnswerController : ControllerBase
    {
        private readonly IMediator _mediator;

        public AnswerController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Retrieves all answers associated with a question based on the provided question ID.
        /// </summary>
        /// <param name="query">The query parameters specifying the question ID.</param>
        /// <returns>A response containing a list of answers for the given question ID.</returns>

        [HttpGet("GetAllAnswerByQuestionId/{QuestionId}")]
        public async Task<ActionResult<AnswerDTO>> GetAllAnswerByQuestionId(
            GetAllAnswerByIdQuery query
        )
        {
            var getAnswer = await _mediator.Send(query);

            if (getAnswer.Count is 0)
            {
                GeneralResponce responce = new() { Result = 0, Message = "Answer is not exist" };
                return NotFound(responce);
            }
            return Ok(getAnswer);
        }

        [HttpGet("GetAnswerById/{AnswerId}")]
        public async Task<ActionResult> GetAnswerById(GetByIdQuery query)
        {
            var data = await _mediator.Send(query);

            if (data is null)
            {
                GeneralResponce responce = new() { Result = 0, Message = "Answer is not exist" };
                return NotFound(responce);
            }

            return Ok(data);
        }

        /// <summary>
        /// Inserts a new answer into the system.
        /// </summary>
        /// <param name="command">The command containing the information for the new answer.</param>
        /// <returns>A response indicating the success of the insertion operation.</returns>

        [HttpPost("InsertAnswer")]
        public async Task<ActionResult<string>> InsertAnswer([FromBody] InsertAnswerCommand command)
        {
            var inserted = await _mediator.Send(command);

            if (inserted is "0")
            {
                GeneralResponce responce = new() { Result = 0, Message = "Question is not exist" };
                return NotFound(responce);
            }
            else if (inserted is "-1")
            {
                GeneralResponce responce = new() { Result = -1, Message = "System Exception" };
                return Problem(responce.Message);
            }
            else if (inserted is "-2")
            {
                return Forbid();
            }
            else
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = inserted,
                        Message = $"Answer Id: {inserted}, Insert process successfully"
                    };
                return Ok(responce);
            }
        }

        /// <summary>
        /// Edits an existing answer in the system.
        /// </summary>
        /// <param name="command">The command containing the information to update the answer.</param>
        /// <returns>A response indicating the success of the update operation.</returns>

        [HttpPut("edit")]
        public async Task<ActionResult<string>> EditAnswer([FromBody] UpdateAnswerCommand command)
        {
            var updated = await _mediator.Send(command);

            if (updated is "0")
            {
                GeneralResponce responce =
                    new() { Result = 0, Message = "This Answer is not exist" };
                return NotFound(responce);
            }
            else if (updated is "-1")
            {
                GeneralResponce responce = new() { Result = -1, Message = "System Exception" };
                return Problem(responce.Message);
            }
            else if (updated is "-2")
            {
                return Forbid();
            }
            else
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = updated,
                        Message = $"Answer Id: {updated}, Update process successfully"
                    };
                return Ok(responce);
            }
        }

        /// <summary>
        /// Deletes an answer from the system based on the provided answer ID.
        /// </summary>
        /// <param name="command">The command containing the answer ID to be deleted.</param>
        /// <returns>A response indicating the success of the deletion operation.</returns>

        [HttpDelete("delete/{AnswerId}")]
        public async Task<ActionResult<int>> DeleteAnswer(DeleteAnswerCommand command)
        {
            var deleted = await _mediator.Send(command);

            if (deleted is 0)
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = 0,
                        Message = $"This Answer Id: {command.AnswerId} is not exist"
                    };
                return NotFound(responce);
            }
            else if (deleted is -1)
            {
                GeneralResponce responce = new() { Result = -1, Message = "System Exception" };
                return Problem(responce.Message);
            }
            else if (deleted is -2)
            {
                return Forbid();
            }
            else
            {
                GeneralResponce responce =
                    new()
                    {
                        Result = 1,
                        Message = $"This Answer Id: {command.AnswerId}, Delete process successfully"
                    };
                return Ok(responce);
            }
        }

        /// <summary>
        /// Votes on an answer with the specified status and answer ID.
        /// </summary>
        /// <param name="command">The command containing the status and answer ID for voting.</param>
        /// <returns>A response indicating the success of the voting process.</returns>

        [HttpPatch("Vote/{Status}/{AnswerId}")]
        public async Task<ActionResult> Vote(VoteAnswerCommand command)
        {
            var voted = await _mediator.Send(command);

            if (voted is 0)
            {
                GeneralResponce responce =
                    new() { Result = 0, Message = "User or Answer is not exist" };
                return NotFound(responce);
            }
            else if (voted is -1)
            {
                GeneralResponce responce = new() { Result = -1, Message = "System Exception" };
                return Problem(responce.Message);
            }
            else if (voted is -2)
            {
                return Forbid();
            }
            else
            {
                GeneralResponce responce =
                    new() { Result = 1, Message = "VoteAnswer process successfully" };
                return Ok(responce);
            }
        }
    }
}
