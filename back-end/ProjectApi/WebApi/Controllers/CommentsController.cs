﻿using AppDomain.DTO;
using AppDomain.Exceptions.UserExceptions;
using Application.Tasks.Commands.Delete.DeleteComment;
using Application.Tasks.Commands.Insert.CommentInserts.InsertComment;
using Application.Tasks.Commands.Insert.VoteComment;
using Application.Tasks.Commands.Update.UpdateUser.UpdateComment;
using Application.Tasks.Queries.UserQueries.GetAllComments;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers;

[Route("api/[controller]")]
[ApiController]
public class CommentsController : ControllerBase
{
    public readonly IMediator _mediator;

    public CommentsController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpPatch("Vote/{Status}/{CommentId}")]
    public async Task<ActionResult> VoteComment(VoteCommentCommand insertCommand)
    {
        try
        {
            await _mediator.Send(insertCommand);

            return Ok();
        }
        catch (UserForbiddenException ex)
        {
            return Unauthorized(ex.Message);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    [HttpPost("MakeComment")]
    public async Task<ActionResult<CommentDTO>> MakeComment(
        [FromBody] InsertCommentCommand insertCommand
    )
    {
        try
        {
            var result = await _mediator.Send(insertCommand);

            return Ok(result);
        }
        catch (UserForbiddenException ex)
        {
            return Unauthorized(ex.Message);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    [HttpPut("edit")]
    public async Task<ActionResult> EditComment([FromBody] UpdateCommentCommand updateCommand)
    {
        try
        {
            var result = await _mediator.Send(updateCommand);

            return Ok(result);
        }
        catch (UserForbiddenException ex)
        {
            return Unauthorized(ex.Message);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    [HttpDelete("delete/{CommentId}")]
    public async Task<ActionResult> DeleteComment(DeleteCommentCommand deleteCommand)
    {
        try
        {
            var result = await _mediator.Send(deleteCommand);

            return Ok(result);
        }
        catch (UserForbiddenException ex)
        {
            return Unauthorized(ex.Message);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }

    [HttpGet("GetCommentsByContentId/{ContentId}")]
    public async Task<ActionResult<IEnumerable<CommentDTO>>> GetCommentsByContentId(
        GetAllCommentsQuery query
    )
    {
        try
        {
            var result = await _mediator.Send(query);

            return Ok(result);
        }
        catch (Exception ex)
        {
            return Problem(ex.Message);
        }
    }
}
