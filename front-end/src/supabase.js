import { supabase } from "./client.js";
import { startLoadingProcess, stopLoadingProcess } from "./utils/process.js";

export function getEnterWithProviderDTO(result) {
  const iss = result.user.user_metadata.iss;
  let type = "unknow";

  if (iss.includes("accounts.google.com")) {
    type = "google";
  } else if (iss.includes("api.github.com")) {
    type = "gitHub";
  }
  console.log(result);
  const dto = {
    secret: result.user.user_metadata.provider_id,
    accountUsername:
      result.user.user_metadata.preferred_username ??
      result.user.user_metadata.name,
    email: result.user.email,
    avatarUrl: result.user.user_metadata.avatar_url,
    type,
  };

  return { providerEnter: dto };
}

export function signInWithGithub() {
  startLoadingProcess(`Waiting GitHub`);
  supabase.auth
    .signInWithOAuth({
      provider: "github",
      options: {
        redirectTo: window.location.origin + "/provider-enter",
      },
    })
    .then(() => {
      stopLoadingProcess();
    })
    .catch(() => {
      stopLoadingProcess();
    });
}

export function connectGithub(redirectUrl) {
  startLoadingProcess(`Waiting GitHub`);
  supabase.auth
    .signInWithOAuth({
      provider: "github",
      options: {
        redirectTo: window.location.origin + { redirectUrl },
      },
    })
    .then(() => {
      stopLoadingProcess();
    })
    .catch(() => {
      stopLoadingProcess();
    });
}

export function signInWithGoogle() {
  startLoadingProcess(`Waiting Google`);

  supabase.auth
    .signInWithOAuth({
      provider: "google",
      options: {
        redirectTo: window.location.origin + "/provider-enter",
      },
    })
    .then(() => {
      stopLoadingProcess();
    })
    .catch(() => {
      stopLoadingProcess();
    });
}

export function signOut() {
  supabase.auth.signOut().catch((error) => {
    console.log(error);
  });
}
