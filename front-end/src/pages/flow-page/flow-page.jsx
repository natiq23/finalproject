import { useEffect, useState } from "react";
import { useAutoAnimate } from "@formkit/auto-animate/react";
import { getFlowArticle } from "../../services/article.js";
import { motion } from "framer-motion";
import { Helmet } from "react-helmet";
import ArticleBigSkeleton from "../../components/skeleton-loaders/article-big-skeleton/index.js";
import Article from "../../components/pages/articles-page/article/index.js";
import LastArticles from "../../components/pages/home-page/last-articles/index.js";
import LastQuestions from "../../components/pages/home-page/last-questions/index.js";
import LastResources from "../../components/pages/home-page/last-resources/index.js";
import { useUserSignedIn } from "../../utils/auth.js";
import { useNavigate } from "react-router-dom";
import NoResultFound from "../../components/common/no-result-found/index.js";
import { MdOutlinePeopleAlt } from "react-icons/md";
import classNames from "classnames";
import { useMediaQuery } from "react-responsive";

const FlowPage = () => {
  const navigate = useNavigate();
  const signedIn = useUserSignedIn();
  if (!signedIn) navigate("/");

  const { animationParent } = useAutoAnimate();
  const [articles, setArticles] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [notFound, setNotFound] = useState(null);

  const isSmall = useMediaQuery({ query: "(max-width: 1024px)" });

  useEffect(() => {
    setIsLoading(true);
    getFlowArticle()
      .then((res) => {
        if (res.hasFollowingUser) {
          setNotFound({
            message: "All catch up!",
            icon: <MdOutlinePeopleAlt />,
          });
        } else {
          setNotFound({
            message: "Please follow a user for view Flow",
            icon: <MdOutlinePeopleAlt />,
          });
        }
        setArticles(res);
      })
      .catch((err) => {
        console.log(err);
        setNotFound({
          message: "A problem occurred, please try again",
          icon: <MdOutlinePeopleAlt />,
        });
      })
      .finally(() => {
        setIsLoading(false);
      });
  }, []);

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      className={classNames({
        "w-full flex justify-between  gap-4 py-4": !isSmall,
        "w-full gap-4 py-4": isSmall,
      })}
      ref={animationParent}
    >
      <div className={"w-full flex flex-col gap-6"}>
        <Helmet>
          <meta charSet="utf-8" />
          <title>Clubrick Community</title>
        </Helmet>
        <div className={"w-full flex flex-col gap-5"}>
          {isLoading ? (
            <div>
              <ArticleBigSkeleton />
              <ArticleBigSkeleton />
              <ArticleBigSkeleton />
            </div>
          ) : articles && articles.length > 0 ? (
            articles.map((article) => (
              <Article key={article.id} article={article} />
            ))
          ) : (
            <NoResultFound label={notFound.message} icon={notFound.icon} />
          )}
        </div>
      </div>

      <div className={"flex flex-col gap-5"}>
        <LastArticles />
        <LastQuestions />
        <LastResources />
      </div>
    </motion.div>
  );
};

export default FlowPage;
