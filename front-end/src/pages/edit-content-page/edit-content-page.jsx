import { useAutoAnimate } from "@formkit/auto-animate/react";
import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { motion } from "framer-motion";
import { Helmet } from "react-helmet";
import Brand from "../../components/common/brand/index.js";
import AccentBorderButton from "../../components/buttons/accent-border-button/index.js";
import { Form, Formik } from "formik";
import {
  startLoadingProcess,
  stopLoadingProcess,
} from "../../utils/process.js";
import { errorToast, successToast } from "../../utils/toast.js";
import { PublishQuestionSchema } from "../../validation/Schemas/PublishQuestionSchema.js";
import PublishContentTitleInput from "../../components/form/publish-content-title-input/index.js";
import CategorySelectInput from "../../components/form/category-select-input/index.js";
import FlagSelectInput from "../../components/form/flag-select-input/index.js";
import MarkdownEditor from "../../components/common/markdown-editor/index.js";
import PrimaryAccentButton from "../../components/buttons/primary-accent-button/index.js";
import { getContent, updateContent } from "../../services/contentCommon.js";
import ContentNotFound from "../../components/common/content-not-found/index.js";
import { useUser } from "../../utils/user.js";
import CoverImageInput from "../../components/form/cover-image-input/index.js";
import { createModal } from "../../utils/modal.js";
import BodyStat from "../../components/form/body-stat/index.js";

const EditContentPage = () => {
  const { animationParent } = useAutoAnimate();
  const [content, setContent] = useState();
  const [isNotFound, setIsNotFound] = useState(false);

  const currentId = useUser().id;

  const navigate = useNavigate();
  const params = useParams();
  const initialType = params.type;
  const initialId = params.id;

  useEffect(() => {
    startLoadingProcess("Content Loading");
    getContent(initialType, initialId)
      .then((res) => {
        if (res.userPreview.id === currentId) {
          setContent(res);
        } else {
          errorToast("You can't edit this content");
        }
      })
      .catch((error) => {
        console.log(error);
        if (error.status === 404) {
          setIsNotFound(true);
        } else errorToast("A problem accrued, please try again later");
      })
      .finally(() => {
        stopLoadingProcess();
      });
  }, []);

  if (isNotFound) return <ContentNotFound />;

  if (!content) return null;

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      ref={animationParent}
      className={"w-full h-full flex flex-col justify-between  gap-4 p-4"}
    >
      <Helmet>
        <meta charSet="utf-8" />
        <title>Edit Content - Clubrick Community</title>
      </Helmet>
      <Formik
        initialValues={{
          image: content.header.image,
          title: content.header.title,
          categoriesIdList: content.categories.map((category) => {
            return {
              label: category.title,
              value: category.id,
            };
          }),
          flagId: content.articleFlag?.id,
          readingTime: 0,
          body: content.body,
        }}
        onSubmit={(values) => {
          const data = values;
          data.id = initialId;
          data.header = { title: values.title, image: values.image };

          startLoadingProcess(`Updating ${initialType}`);

          updateContent(initialType, data)
            .then(() => {
              successToast(`Updated ${initialType}`);
              navigate(`/${initialType}/${initialId}`);
            })
            .catch((error) => {
              console.log(error);
              errorToast(error.message);
            })
            .finally(() => {
              stopLoadingProcess();
            });
        }}
        validationSchema={PublishQuestionSchema}
      >
        {({ dirty, isValid }) => (
          <Form className={"w-full flex flex-col  full-card-schema !gap-8"}>
            <div className={"w-full flex items-center justify-between"}>
              <div className={"min-w-[200px] w-fit flex items-center gap-4"}>
                <Brand />

                <span
                  className={"w-44   font-semibold text-lg dark:text-gray-300"}
                >
                  Create {initialType}
                </span>
              </div>

              <BodyStat name={"readingTime"} />

              <div className={"w-52"}>
                <AccentBorderButton
                  title={"Back to home"}
                  click={() => {
                    if (dirty)
                      createModal("confirmContentEditorBack", {
                        contentType: initialType,
                        confirm: () => navigate("/"),
                      });
                    else navigate("/");
                  }}
                />
              </div>
            </div>

            {/*<div className={""}>*/}
            {initialType === "articles" && (
              <div className={"px-2"}>
                <CoverImageInput name={"image"} />
              </div>
            )}

            <PublishContentTitleInput
              name={"title"}
              placeholder={`New ${initialType} title here`}
            />
            <CategorySelectInput name={"categoriesIdList"} />
            {initialType === "articles" && (
              <FlagSelectInput name={"flagId"} type={initialType} />
            )}
            <MarkdownEditor
              placeholder={`Write your ${initialType} here...`}
              bigSize={false}
              name={"body"}
            />
            <div className={"w-fit"}>
              <PrimaryAccentButton
                type={"submit"}
                title={`Update ${initialType}`}
                disabled={!isValid || !dirty}
              />
            </div>
            {/*</div>*/}
          </Form>
        )}
      </Formik>
    </motion.div>
  );
};

export default EditContentPage;
