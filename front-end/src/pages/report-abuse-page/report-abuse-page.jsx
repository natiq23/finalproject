import { motion } from "framer-motion";
import { useReportAbuse } from "../../utils/process.js";
import { Link, useParams, useSearchParams } from "react-router-dom";
import { Form, Formik } from "formik";
import TextAreaInput from "../../components/form/text-area-input/index.js";
import TextInput from "../../components/form/text-input/index.js";
import RadioButtonGroup from "../../components/form/radio-button-group/index.js";
import PrimaryAccentButton from "../../components/buttons/primary-accent-button/index.js";
import { Helmet } from "react-helmet";
import { useEffect } from "react";
import { finishReportAbuse } from "../../store/process.js";
import { ReportInsertSchema } from "../../validation/Schemas/ReportInsertSchema.js";
import { getReportDtoFromUrl } from "../../utils/common.js";
import { errorToast, successToast } from "../../utils/toast.js";
import { insertReport } from "../../services/report.js";
import { useUserSignedIn } from "../../utils/auth.js";
import { createModal } from "../../utils/modal.js";

const ReportAbusePage = () => {
  const signedIn = useUserSignedIn();

  const [searchParams, setSearchParams] = useSearchParams();

  return (
    <motion.div
      nitial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      className={"w-full  px-32 py-4"}
    >
      <Helmet>
        <meta charSet="utf-8" />
        <title>Report Abuese - Clubrick Community</title>
      </Helmet>

      <div
        className={
          "w-full bg-white dark:bg-dark-component-bg dark:border-dark-border-clr dark:border-[1px] rounded-md px-14 py-8 flex flex-col gap-3 "
        }
      >
        {/*Header*/}
        <div className={"flex flex-col gap-3"}>
          <h1 className={"font-bold text-4xl dark:text-gray-300"}>
            Report abuse
          </h1>

          <p className={"text-xl dark:text-gray-400"}>
            Thank you for reporting any abuse that violates our{" "}
            <Link
              to={"/code-of-conduct"}
              className={
                "underline-animation text-accent dark:text-dark-text-accent"
              }
            >
              code of conduct
            </Link>{" "}
            or{" "}
            <Link
              to={"/terms"}
              className={
                "underline-animation text-accent dark:text-dark-text-accent"
              }
            >
              terms
            </Link>{" "}
            and conditions. <br />
            We continue to try to make this environment a great one for
            everybody.
          </p>
        </div>
        <div>
          <Formik
            initialValues={{
              reportType: "",
              reportMessage: "",
              url: searchParams.get("url") ?? "",
            }}
            enableReinitialize={true}
            onSubmit={(values) => {
              if (!signedIn) {
                createModal("enter");
                return;
              }

              const dto = getReportDtoFromUrl(values.url);

              if (dto === null) {
                errorToast("Invalid URL");
                return;
              }

              insertReport({ ...dto, ...values, id: "null" })
                .then(() => {
                  successToast("Report submitted");
                })
                .catch(() => {
                  errorToast("Failed to submit report");
                });
            }}
            validationSchema={ReportInsertSchema}
          >
            {({ isValid, values }) => (
              <Form
                className={
                  "p-4 rounded-md border-2 border-[#f3f3f3] dark:border-dark-border-clr flex flex-col gap-4"
                }
              >
                <RadioButtonGroup
                  options={[
                    {
                      data: "Rude or vulgar",
                      value: "Rude_Or_Vulgar",
                    },
                    {
                      data: "Harassment or hate speech",
                      value: "Harassment_Or_Hate",
                    },
                    {
                      data: "Spam or copyright issue",
                      value: "Spam_Or_Copyright",
                    },
                    {
                      data: "Inappropriate listings message/category",
                      value: "Inappropriate_Listing",
                    },
                    {
                      data: "Other",
                      value: "Other",
                    },
                  ]}
                  name={"reportType"}
                />

                {/*{reportAbuse.contentType !== "resource" && (*/}
                {/*)}*/}
                <TextInput
                  name={"url"}
                  title={"Reported URL"}
                  disabled={searchParams.get("url")}
                />

                <TextAreaInput
                  name={"reportMessage"}
                  placeholder={"..."}
                  title={"Message"}
                  hint={
                    "Please provide any additional information or context that will help us understand and handle the situation."
                  }
                />

                <div className={"w-fit"}>
                  <PrimaryAccentButton
                    title={"Send Feedback"}
                    type={"submit"}
                    disabled={!isValid}
                  />
                </div>
              </Form>
            )}
          </Formik>
        </div>
      </div>
    </motion.div>
  );
};

export default ReportAbusePage;
