import { motion } from "framer-motion";
import { Helmet } from "react-helmet";
import { useAutoAnimate } from "@formkit/auto-animate/react";
import { useSearchParams } from "react-router-dom";
import { useEffect, useState } from "react";
import PageFilterContainer from "../../components/pages/common/page-filter-container/index.js";
import { MagnifyingGlass, Triangle } from "react-loader-spinner";
import NoResultFound from "../../components/common/no-result-found/index.js";
import FollowNotification from "../../components/pages/notifications-page/follow-notification/index.js";
import WatchNotification from "../../components/pages/notifications-page/watch-notification/index.js";
import { IoNotificationsOffOutline } from "react-icons/io5";
import {
  getAllNotifications,
  getAllNotificationsByType,
  readNotifications,
} from "../../services/notifications.js";
import ErrorView from "../../components/common/error-view/index.js";
import CommentNotification from "../../components/pages/notifications-page/comment-notification/index.js";
import ArticleNotification from "../../components/pages/notifications-page/article-notification/index.js";
import BuyNotification from "../../components/pages/notifications-page/buy-notification/index.js";
const NotificationsPage = () => {
  const { animationParent } = useAutoAnimate();
  const [searchParams, setSearchParams] = useSearchParams({
    filter: "",
  });

  const [isLoading, setIsLoading] = useState(false);
  const [baseNotifications, setBaseNotifications] = useState([]);
  const [notifications, setNotifications] = useState([]);

  const pageFilters = [
    {
      label: "All",
      paramName: "All",
    },
    {
      label: "Watch",
      paramName: "Answer",
    },
    {
      label: "Follow",
      paramName: "Follow",
    },
  ];

  const renderItem = (item) => {
    switch (item.type) {
      case "Follow":
        return <FollowNotification data={item} />;
      case "Answer":
        return <WatchNotification data={item} />;
      case "Comment":
        return <CommentNotification data={item} />;
      case "Article":
        return <ArticleNotification data={item} />;
      case "CorrectAnswer":
        return <WatchNotification data={item} isCorrect={true} />;
      case "ProductPurchase":
        return <BuyNotification data={item} />;
      default:
        return <ErrorView message={"Unsupported notification"} />;
    }
  };

  const handleOnSearch = () => {
    setIsLoading(true);
    setNotifications([]);
    const filter = searchParams.get("filter");
    setNotifications(
      baseNotifications.filter((n) =>
        filter === "All" ? true : n.type === filter
      )
    );
    console.log(baseNotifications);
    setIsLoading(false);
  };

  useEffect(() => {
    setIsLoading(true);

    getAllNotifications()
      .then((res) => {
        console.log(res);
        setNotifications(res);
        setBaseNotifications(res);
        readNotifications().catch((error) => {
          console.log(error);
        });
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        setIsLoading(false);
      });
  }, []);

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      className={"w-full flex flex-col items-center mt-3 px-[10%] py-8  gap-4"}
      ref={animationParent}
    >
      <Helmet>
        <meta charSet="utf-8" />
        <title>Notifications - Clubrick Community</title>
      </Helmet>

      <span
        className={"w-full text-3xl font-bold dark:text-gray-300 select-none"}
      >
        Notifications
      </span>

      <div className={"w-full flex gap-4"}>
        <div className={"w-56"}>
          <PageFilterContainer
            pageFilters={pageFilters}
            paramName={"filter"}
            defaultParam={"All"}
            onSearch={handleOnSearch}
            setSearchParam={setSearchParams}
            searchParams={searchParams}
          />
        </div>
        <div className={"w-full"}>
          {isLoading ? (
            <div className={"w-full h-full flex items-center justify-center"}>
              <Triangle
                height="100"
                width="100"
                color="#4a86e1"
                ariaLabel="triangle-loading"
                wrapperStyle={{}}
                wrapperClassName=""
                visible={true}
              />
            </div>
          ) : notifications.length > 0 ? (
            <div className={"flex flex-col gap-4"}>
              {notifications.map((item) => {
                return <div key={item}>{renderItem(item)}</div>;
              })}
            </div>
          ) : (
            <div>
              <NoResultFound
                label={"All caught up!"}
                icon={<IoNotificationsOffOutline />}
              />
            </div>
          )}
        </div>
      </div>
    </motion.div>
  );
};

export default NotificationsPage;
