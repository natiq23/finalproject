import { Tab } from "@headlessui/react";
import { motion } from "framer-motion";
import TabHead from "../../../components/tabs/tab-head/index.js";
import TagTable from "../../../components/tables/tag-table/index.js";

const ManageTags = () => {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      className={"w-full"}
    >
      <Tab.Group>
        <Tab.List className="flex gap-1 rounded-md bg-body-bg dark:bg-dark-body-bg border-[1px] border-border-clr dark:border-dark-border-clr p-1">
          <TabHead key={"categories"} label={"Categories"} />
          <TabHead key={"resource-flags"} label={"Resource Flags"} />
          <TabHead key={"article-flags"} label={"Article Flags"} />
        </Tab.List>
        <Tab.Panels className="mt-2">
          <Tab.Panel key={"categories"} className={"p-3 card-color-schema"}>
            <TagTable type={"categories"} />
          </Tab.Panel>
          <Tab.Panel key={"resource-flags"} className={"p-3 card-color-schema"}>
            <TagTable type={"resource-flags"} />
          </Tab.Panel>
          <Tab.Panel key={"article-flags"} className={" p-3 card-color-schema"}>
            <TagTable type={"article-flags"} />
          </Tab.Panel>
        </Tab.Panels>
      </Tab.Group>
    </motion.div>
  );
};

export default ManageTags;
