import { useEffect, useState } from "react";
import { useRowSelect } from "@table-library/react-table-library/select";
import {
  DEFAULT_OPTIONS,
  getTheme,
} from "@table-library/react-table-library/material-ui";
import { useTheme } from "@table-library/react-table-library/theme";
import { getAllReports } from "../../../services/report.js";
import {
  Body,
  Cell,
  Header,
  HeaderCell,
  HeaderRow,
  Row,
  Table,
} from "@table-library/react-table-library/table";
import InlineLoader from "../../../components/loaders/inline-loader/index.js";
import { ThreeDots } from "react-loader-spinner";
import NoResultFound from "../../../components/common/no-result-found/index.js";
import { createModal } from "../../../utils/modal.js";

const ReportsPage = () => {
  const [nodes, setNodes] = useState([]);

  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    handleRefresh();
  }, []);

  let data = { nodes };

  const select = useRowSelect(data, {
    onChange: onSelectChange,
  });

  function onSelectChange(action, state) {
    const selectedReport = data.nodes.find((item) => item.id === state.id);

    createModal("reportView", { id: selectedReport.id });
    // createModal("tagEditor", {
    //   type,
    //   tag: selectedTag,
    //   refresh: handleRefresh,
    // });
  }

  const materialTheme = getTheme(DEFAULT_OPTIONS);
  const theme = useTheme(materialTheme);

  data = {
    nodes: data.nodes,
  };

  const handleRefresh = () => {
    setIsLoading(true);

    getAllReports()
      .then((res) => {
        setIsLoading(false);
        setNodes(res);
      })
      .catch(() => {
        setIsLoading(false);
      });
  };
  return (
    <div className={"!z-[1]"}>
      <Table
        data={data}
        select={select}
        className={"!max-h-[410px]"}
        theme={theme}
      >
        {(tableList) => (
          <>
            <Header>
              <HeaderRow
                className={
                  "dark:bg-dark-component-second-bg dark:text-gray-300"
                }
              >
                <HeaderCell>Id</HeaderCell>
                <HeaderCell>ContentId</HeaderCell>
                <HeaderCell>ContentType</HeaderCell>
                <HeaderCell>ReportType</HeaderCell>
                <HeaderCell>ReportType</HeaderCell>
              </HeaderRow>
            </Header>

            <Body className={"dark:bg-dark-body-bg"}>
              {tableList.map((item) => (
                <Row
                  item={item}
                  key={item.id}
                  className={
                    "!rounded-md dark:bg-dark-body-bg  dark:text-gray-300"
                  }
                >
                  <Cell>{item.id}</Cell>
                  <Cell>{item.contentId}</Cell>
                  <Cell>{item.contentType}</Cell>
                  <Cell>{item.reportType}</Cell>
                  <Cell>{item.reportMessage}</Cell>
                </Row>
              ))}
            </Body>
          </>
        )}
      </Table>
      <br />

      {isLoading ? (
        <>
          <InlineLoader
            loader={
              <ThreeDots
                height="24"
                width="24"
                color={"#3B49DF"}
                wrapperClass="radio-wrapper"
                radius="9"
                ariaLabel="three-dots-loading"
                wrapperStyle={{}}
                wrapperClassName=""
                visible={true}
              />
            }
            message={"Reports loading..."}
            version={"input-like"}
          />
        </>
      ) : (
        nodes.length <= 0 && <NoResultFound />
      )}
    </div>
  );
};

export default ReportsPage;
