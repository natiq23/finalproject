import { motion } from "framer-motion";
import { TypeAnimation } from "react-type-animation";
import PrimaryAccentButton from "../../components/buttons/primary-accent-button/index.js";

const AdminMemePage = () => {
  document.title = "Yore crazy!";

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      className={
        "w-full h-[100vh] bg-[#222222] text-gray-200 text-center font-bold text-xl flex flex-col items-center justify-center gap-6"
      }
    >
      <div className={"leading-10"}>
        <TypeAnimation
          sequence={[
            "You are genius",
            1000, // Waits 1s
            "You are incredible",
            2000, // Waits 1s
            () => {
              console.log("Sequence completed");
            },
          ]}
          speed={1}
          wrapper="span"
          cursor={true}
          repeat={Infinity}
          style={{ fontSize: "2em", display: "inline-block" }}
        />
      </div>
      <a
        href="https://www.youtube.com/watch?v=0ZZTP_hoMPw"
        target={"_blank"}
        className={
          "w-72 text-sm  font-semibold text-gray-200  py-2 px-4 rounded-md bg-accent hover:bg-accent-dark transition-all duration-70 disabled:cursor-not-allowed disabled:opacity-70 text-center"
        }
        rel="noreferrer"
      >
        See magic
      </a>
    </motion.div>
  );
};

export default AdminMemePage;
