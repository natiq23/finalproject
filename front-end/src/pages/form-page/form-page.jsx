import { motion } from "framer-motion";
import { useAutoAnimate } from "@formkit/auto-animate/react";
import { useEffect, useState } from "react";
import { useUserSignedIn } from "../../utils/auth.js";
import PageHeader from "../../components/pages/common/page-header/index.js";
import PrimaryAccentButton from "../../components/buttons/primary-accent-button/index.js";
import { createModal } from "../../utils/modal.js";
import TitleDivider from "../../components/title-dividers/title-divider/index.js";
import PageGuidelines from "../../components/pages/common/page-guidelines/index.js";
import PageStats from "../../components/pages/common/page-stats/index.js";
import NoResultFound from "../../components/common/no-result-found/index.js";
import { Helmet } from "react-helmet";
import { TbCategory2 } from "react-icons/tb";
import { BiSort } from "react-icons/bi";
import PageSearchBar from "../../components/common/page-search-bar/index.js";
import { useNavigate, useSearchParams } from "react-router-dom";
import Question from "../../components/pages/form-page/question/index.js";
import PageFilterContainer from "../../components/pages/common/page-filter-container/index.js";
import {
  getAllQuestion,
  getAllSavedQuestion,
  getAllWatchedQuestion,
} from "../../services/question.js";
import { getContentCount } from "../../services/contentCommon.js";
import QuestionSkeleton from "../../components/skeleton-loaders/question-skeleton/index.js";

const FormPage = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const navigate = useNavigate();

  const [animationParent] = useAutoAnimate();

  const [isLoading, setIsLoading] = useState(false);
  const [questions, setQuestions] = useState([]);
  const [count, setCount] = useState(0);

  const isSignedIn = useUserSignedIn();

  const handleSearch = () => {
    setIsLoading(true);

    getContentCount("questions").then((res) => setCount(res));

    const filter = searchParams.get("filter");

    switch (filter) {
      case "saved":
        getAllSavedQuestion(
          searchParams.get("q"),
          searchParams.get("category"),
          searchParams.get("sort")
        )
          .then((res) => {
            setQuestions(res.items);
          })
          .catch((error) => {
            console.log(error.message);
          })
          .finally(() => {
            setIsLoading(false);
          });
        break;
      case "watched":
        getAllWatchedQuestion(
          searchParams.get("q"),
          searchParams.get("category"),
          searchParams.get("sort")
        )
          .then((res) => {
            setQuestions(res.items);
          })
          .catch((error) => {
            console.log(error.message);
          })
          .finally(() => {
            setIsLoading(false);
          });
        break;
      default:
        getAllQuestion(
          searchParams.get("q"),
          searchParams.get("category"),
          searchParams.get("sort")
        )
          .then((res) => {
            setQuestions(res.items);
          })
          .catch((error) => {
            console.log(error.message);
          })
          .finally(() => {
            setIsLoading(false);
          });
    }
  };

  const handleGetSearchParam = (param) => {
    return searchParams.get(param);
  };

  const pageFilters = [
    {
      label: "Saved",
      paramName: "saved",
    },
    {
      label: "Watched",
      paramName: "watched",
    },
  ];

  useEffect(() => {
    handleSearch();
  }, []);

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      className={"w-full flex flex-col justify-between  gap-6 py-4"}
      ref={animationParent}
    >
      <Helmet>
        <meta charSet="utf-8" />
        <title>Form - Clubrick Community</title>
      </Helmet>

      <PageHeader
        iconName={"form"}
        title={"Form"}
        description={
          "It's time to ask the unanswered questions, don't be afraid, you are probably not the first person to have this problem.\nAre you ready to help!"
        }
        accentColor={"#5490f9"}
        actionSide={
          <div className={"w-26"}>
            <PrimaryAccentButton
              title={"Ask Question"}
              click={() => {
                if (isSignedIn) {
                  navigate("/publish/question");
                } else {
                  createModal("enter");
                }
              }}
            />
          </div>
        }
      />
      <div className={"flex justify-between"}>
        <aside className={"w-60 px-4 flex flex-col gap-2"}>
          {isSignedIn && (
            <>
              <TitleDivider title={"Sub Pages"} />
              <PageFilterContainer
                pageFilters={pageFilters}
                paramName={"filter"}
                onSearch={handleSearch}
                setSearchParam={setSearchParams}
                searchParams={searchParams}
                isRemovable={true}
              />
            </>
          )}

          <PageGuidelines
            title={"submission guidelines"}
            guidelines={
              "Write your question clearly.\nUse plain language\nDo not rush to get an answer\nUse gentle!"
            }
          />
          <PageStats stat={count} title={"Question Shared"} />
        </aside>
        <main className={"w-full px-1 flex flex-col gap-3"}>
          {/*Content Header*/}
          <PageSearchBar
            options={[
              {
                label: "Category",
                type: "category",
                paramName: "category",
                icon: <TbCategory2 />,
              },
              {
                label: "Sort",
                type: "sort",
                paramName: "sort",
                icon: <BiSort />,
              },
            ]}
            onSearch={handleSearch}
            placeholder={"Search question"}
            getSearchParam={handleGetSearchParam}
            setSearchParam={setSearchParams}
          />

          <div className={"flex flex-col  gap-5 pt-2"} ref={animationParent}>
            {isLoading ? (
              <div className={"flex flex-col gap-8"} ref={animationParent}>
                <QuestionSkeleton />
                <QuestionSkeleton />
              </div>
            ) : questions && questions.length > 0 ? (
              <>
                {questions.map((question) => (
                  <Question key={question.id} question={question} />
                ))}
              </>
            ) : (
              <NoResultFound />
            )}
          </div>
        </main>
      </div>
    </motion.div>
  );
};

export default FormPage;
