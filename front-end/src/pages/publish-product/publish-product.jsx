import { motion } from "framer-motion";
import { Helmet } from "react-helmet";
import AccentBorderButton from "../../components/buttons/accent-border-button/index.js";
import { useNavigate } from "react-router-dom";
import { Form, Formik } from "formik";
import TextInput from "../../components/form/text-input/index.js";
import MarkdownEditor from "../../components/common/markdown-editor/index.js";
import NumberInput from "../../components/form/number-input/index.js";
import { PublishProductSchema } from "../../validation/Schemas/PublishProductSchema.js";
import SubmitIsland from "../../components/form/submit-island/index.js";
import ProductImageInput from "../../components/form/product-image-input/index.js";
import {
  startLoadingProcess,
  stopLoadingProcess,
} from "../../utils/process.js";
import { insertProduct } from "../../services/shop.js";
import { errorToast, successToast } from "../../utils/toast.js";
import BuyCheckoutInfo from "../../components/common/buy-checkout-info/index.js";
const PublishProduct = () => {
  const navigate = useNavigate();

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      className={"w-full"}
    >
      <Helmet>
        <meta charSet="utf-8" />
        <title>Publish Products - Clubrick Community</title>
      </Helmet>

      <div
        className={
          "w-full px-2 flex flex-col items-start  sm:flex-row sm:items-center sm:justify-between"
        }
      >
        <h1 className={"font-bold text-2xl dark:text-gray-300"}>
          Publish Product
        </h1>
        <div className={"w-[160px]"}>
          <AccentBorderButton
            title={"Back to dashboard"}
            click={() => {
              navigate(-1);
            }}
          />
        </div>
      </div>

      <Formik
        initialValues={{
          name: "",
          description: "",
          photoPaths: [],
          price: 1,
          discount: 0,
          stock: 1,
        }}
        onSubmit={(values) => {
          startLoadingProcess(`Publishing Product`);

          const dto = {
            name: values.name,
            description: values.description,
            price: values.price,
            discount:
              !values.discount || values.discount === "" ? 0 : values.discount,
            stock: values.stock,
            photoPaths: values.photoPaths,
          };

          insertProduct(dto)
            .then(() => {
              successToast(`Product published successfully`);
            })
            .catch((error) => {
              console.log(error);
              errorToast(
                !error.message ||
                  (error.message === "" && "Something went wrong")
              );
            })
            .finally(() => {
              stopLoadingProcess();
            });
        }}
        validationSchema={PublishProductSchema}
      >
        {({ dirty, isValid, values }) => (
          <Form className={"card-size-schema !gap-5"}>
            <TextInput
              name={"name"}
              title={"Product name"}
              placeholder={"Enter product name"}
              bigTitle={true}
            />

            <div className={"grid grid-cols-2 gap-2 items-center"}>
              <NumberInput
                name={"price"}
                title={"Price"}
                bigTitle={true}
                min={1}
                placeholder={"Enter product price"}
              />
              <NumberInput
                name={"discount"}
                title={"Discount (percentage %)"}
                bigTitle={true}
                min={0}
                max={100}
                placeholder={"Enter product discount (percentage 0-100)"}
              />
            </div>

            <BuyCheckoutInfo
              label={"Discount Price"}
              value={Math.floor(
                values.price - values.price * (values.discount / 100)
              )}
            />

            <NumberInput
              name={"stock"}
              title={"Product in stock"}
              bigTitle={true}
              min={1}
              placeholder={"Enter product quantity"}
            />

            <MarkdownEditor
              name={"description"}
              transparent={false}
              placeholder={"Enter product description"}
            />

            <ProductImageInput name={"photoPaths"} />

            <SubmitIsland
              formIsDirty={dirty && isValid}
              label={"Publish Product"}
            />
          </Form>
        )}
      </Formik>
    </motion.div>
  );
};

export default PublishProduct;
