import { useNavigate, useParams } from "react-router-dom";
import { motion } from "framer-motion";
import { Helmet } from "react-helmet";
import AccentBorderButton from "../../components/buttons/accent-border-button/index.js";
import { Form, Formik } from "formik";
import {
  startLoadingProcess,
  stopLoadingProcess,
} from "../../utils/process.js";
import { getProduct, updateProduct } from "../../services/shop.js";
import { errorToast, successToast } from "../../utils/toast.js";
import TextInput from "../../components/form/text-input/index.js";
import NumberInput from "../../components/form/number-input/index.js";
import MarkdownEditor from "../../components/common/markdown-editor/index.js";
import ProductImageInput from "../../components/form/product-image-input/index.js";
import SubmitIsland from "../../components/form/submit-island/index.js";
import { useEffect, useState } from "react";
import ContentNotFound from "../../components/common/content-not-found/index.js";
import { UpdateProductSchema } from "../../validation/Schemas/UpdateProductSchema.js";
import BuyCheckoutInfo from "../../components/common/buy-checkout-info/index.js";

const ManageProductUpdate = () => {
  const navigate = useNavigate();

  const [isNotFound, setIsNotFound] = useState(false);
  const [product, setProduct] = useState(null);

  const { id } = useParams();

  useEffect(() => {
    startLoadingProcess("Product Reading");
    getProduct(id)
      .then((res) => {
        setProduct(res);
      })
      .catch((error) => {
        console.log(error);
        if (error.status === 404) {
          setIsNotFound(true);
        } else errorToast("A problem accrued, please try again later");
      })
      .finally(() => {
        stopLoadingProcess();
      });
  }, []);

  if (isNotFound) return <ContentNotFound />;

  if (!product) return null;

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      className={"w-full"}
    >
      <Helmet>
        <meta charSet="utf-8" />
        <title>Publish Products - Clubrick Community</title>
      </Helmet>

      <div
        className={
          "w-full px-2 flex flex-col items-start  sm:flex-row sm:items-center sm:justify-between"
        }
      >
        <h1 className={"font-bold text-2xl dark:text-gray-300"}>
          Update Product
        </h1>
        <div className={"w-[160px]"}>
          <AccentBorderButton
            title={"Return back"}
            click={() => {
              navigate(-1);
            }}
          />
        </div>
      </div>

      <Formik
        initialValues={{
          name: product.name,
          description: product.description,
          photos: product.photos.map((photo) => photo.path),
          price: product.price,
          discount: product.discount,
          quantity: product.quantity,
        }}
        onSubmit={(values) => {
          startLoadingProcess(`Updating Product`);

          const dto = {
            productId: product.id,
            name: values.name,
            description: values.description,
            price: values.price,
            discount:
              !values.discount || values.discount === "" ? 0 : values.discount,
            quantity: values.quantity,
            photoUpdateDTO: {
              Photos: values.photos,
            },
          };

          updateProduct(dto)
            .then(() => {
              successToast(`Product updated successfully`);
            })
            .catch((error) => {
              console.log(error);
              errorToast(
                !error.message ||
                  (error.message === "" && "Something went wrong")
              );
            })
            .finally(() => {
              stopLoadingProcess();
            });
        }}
        validationSchema={UpdateProductSchema}
      >
        {({ dirty, isValid, values }) => (
          <Form className={"card-size-schema !gap-5"}>
            <TextInput
              name={"name"}
              title={"Product name"}
              placeholder={"Enter product name"}
              bigTitle={true}
            />

            <div className={"grid grid-cols-2 gap-2 items-center"}>
              <NumberInput
                name={"price"}
                title={"Price"}
                bigTitle={true}
                min={1}
                placeholder={"Enter product price"}
              />
              <NumberInput
                name={"discount"}
                title={"Discount (percentage %)"}
                bigTitle={true}
                min={0}
                max={100}
                placeholder={"Enter product discount (percentage 0-100)"}
              />
            </div>

            <BuyCheckoutInfo
              label={"Discount Price"}
              value={Math.floor(
                values.price - values.price * (values.discount / 100)
              )}
            />

            <NumberInput
              name={"quantity"}
              title={"Product in stock"}
              bigTitle={true}
              min={1}
              placeholder={"Enter product quantity"}
            />

            <MarkdownEditor
              name={"description"}
              transparent={false}
              placeholder={"Enter product description"}
            />

            <ProductImageInput name={"photos"} />

            {/*<div className={"text-white flex flex-col"}>*/}
            {/*  {values.photos.map((item, index) => (*/}
            {/*    <p key={index}>{item}</p>*/}
            {/*  ))}*/}
            {/*</div>*/}

            <SubmitIsland
              formIsDirty={dirty && isValid}
              label={"Update Product"}
            />
          </Form>
        )}
      </Formik>
    </motion.div>
  );
};

export default ManageProductUpdate;
