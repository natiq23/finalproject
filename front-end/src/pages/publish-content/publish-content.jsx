import { motion } from "framer-motion";
import { useAutoAnimate } from "@formkit/auto-animate/react";
import Brand from "../../components/common/brand/index.js";
import { Helmet } from "react-helmet";
import { useNavigate, useParams } from "react-router-dom";
import MarkdownEditor from "../../components/common/markdown-editor/index.js";
import AccentBorderButton from "../../components/buttons/accent-border-button/index.js";
import PrimaryAccentButton from "../../components/buttons/primary-accent-button/index.js";
import { Form, Formik } from "formik";
import PublishContentTitleInput from "../../components/form/publish-content-title-input/index.js";
import CategorySelectInput from "../../components/form/category-select-input/index.js";
import FlagSelectInput from "../../components/form/flag-select-input/index.js";
import { PublishQuestionSchema } from "../../validation/Schemas/PublishQuestionSchema.js";
import { errorToast, successToast } from "../../utils/toast.js";
import {
  startLoadingProcess,
  stopLoadingProcess,
} from "../../utils/process.js";
import { PublishArticleSchema } from "../../validation/Schemas/PublishArticleSchema.js";
import { insertContent } from "../../services/contentCommon.js";
import CoverImageInput from "../../components/form/cover-image-input/index.js";
import { createModal } from "../../utils/modal.js";
import BodyStat from "../../components/form/body-stat/index.js";

const PublishContent = () => {
  const { animationParent } = useAutoAnimate();

  const navigate = useNavigate();
  const params = useParams();
  const initialType = params.type;

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      ref={animationParent}
      className={"w-full h-full flex flex-col justify-between  gap-4 p-4"}
    >
      <Helmet>
        <meta charSet="utf-8" />
        <title>New Content - Clubrick Community</title>
      </Helmet>
      <Formik
        initialValues={{
          image: "",
          title: "",
          body: "",
          categoriesIdList: [],
          flagId: "",
          readingTime: 0,
        }}
        onSubmit={(values) => {
          const data = values;
          data.header = { title: values.title, image: values.image };

          startLoadingProcess(`Publishing ${initialType}`);

          const requestType =
            initialType === "article" ? "articles" : "questions";

          console.log(data);

          insertContent(requestType, data)
            .then((res) => {
              successToast(`Published ${initialType}`);
              navigate(`/${requestType}/${res.result}`);
            })
            .catch((error) => {
              console.log(error);
              errorToast(error.message);
            })
            .finally(() => {
              stopLoadingProcess();
            });
        }}
        validationSchema={
          initialType === "question"
            ? PublishQuestionSchema
            : PublishArticleSchema
        }
      >
        {({ dirty, isValid }) => (
          <Form className={"w-full flex flex-col  full-card-schema !gap-8"}>
            <div className={"w-full flex items-center justify-between"}>
              <div className={"min-w-60 w-fit flex items-center gap-4"}>
                <Brand name={"readingTime"} />

                <span
                  className={"w-36   font-semibold text-lg dark:text-gray-300"}
                >
                  Create {initialType}
                </span>
              </div>

              <BodyStat name={"readingTime"} />

              <div className={"w-52"}>
                <AccentBorderButton
                  title={"Back to home"}
                  click={() => {
                    if (dirty)
                      createModal("confirmContentEditorBack", {
                        contentType: initialType,
                        confirm: () => navigate("/"),
                      });
                    else navigate("/");
                  }}
                />
              </div>
            </div>

            {/*<div className={""}>*/}
            {initialType === "article" && (
              <div className={""}>
                <CoverImageInput name={"image"} />
              </div>
            )}

            <PublishContentTitleInput
              name={"title"}
              placeholder={`New ${initialType} title here`}
            />
            <CategorySelectInput name={"categoriesIdList"} />
            {initialType === "article" && (
              <FlagSelectInput name={"flagId"} type={initialType} />
            )}
            <MarkdownEditor
              placeholder={`Write your ${initialType} here...`}
              bigSize={false}
              name={"body"}
            />
            <div className={"w-fit"}>
              <PrimaryAccentButton
                type={"submit"}
                title={`Publish ${initialType}`}
                disabled={!isValid}
              />
            </div>
            {/*</div>*/}
          </Form>
        )}
      </Formik>
    </motion.div>
  );
};

export default PublishContent;
