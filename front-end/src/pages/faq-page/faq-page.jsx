import { motion } from "framer-motion";
import { Helmet } from "react-helmet";
import { Link } from "react-router-dom";

const FaqPage = () => {
  return (
    <motion.div initial={{ opacity: 0 }} animate={{ opacity: 1 }}>
      <Helmet>
        <meta charSet="utf-8" />
        <title>Frequently Asked Questions 🤔 - Clubrick Community</title>
      </Helmet>
      <h1 className="primary-header">Who can post to dev.to?</h1>
      <p className="text">
        Anyone! Yes, you have permission to make a new post of any kind as long
        as it meets our community guidelines and gets through common-sense spam
        filters. Your post is subject to removal at the discretion of the
        moderators if they believe it does not meet the requirements of our{" "}
        <Link
          to={"/code-of-conduct"}
          className={
            "underline-animation text-accent dark:text-dark-text-accent"
          }
        >
          code of conduct
        </Link>{" "}
      </p>

      <h1 className="primary-header">
        Can I cross-post something I&apos;ve already written on my own blog or
        Medium?
      </h1>
      <p className="text">
        Absolutely, as long as you have the rights you need to do so! And if
        it&apos;s of high quality, we&apos;ll feature it.
      </p>

      <h1 className="primary-header">
        I found a security vulnerability. How do I report it?
      </h1>
      <p className="text">
        Please email{" "}
        <a
          href="mailto:support@clubrick.com"
          className="underline-animation text-accent dark:text-dark-text-accent"
        >
          support@clubrick.com
        </a>
        .
      </p>

      <h1 className="primary-header">Can I sign up with email and password?</h1>
      <p className="text">
        Yes indeed, you can! Just go to{" "}
        <Link
          to={"/enter"}
          className={
            "underline-animation text-accent dark:text-dark-text-accent"
          }
        >
          this page
        </Link>{" "}
        and you should see the option to sign up via email.
      </p>
    </motion.div>
  );
};

export default FaqPage;
