import { useAutoAnimate } from "@formkit/auto-animate/react";
import UpdateEmailForm from "../../../components/form/update-email-form/index.js";
import UpdateDisplayEmailForm from "../../../components/form/update-display-email-form/index.js";
import DeleteAccountForm from "../../../components/form/delete-account-form/index.js";
import AccountEmails from "../../../components/form/account-emails/index.js";
import ConnectGithubAccount from "../../../components/form/connect-github-account/index.js";
import PasswordActionCard from "../../../components/form/password-action-card/index.js";

const AccountSettings = () => {
  const { animationParent } = useAutoAnimate();

  return (
    <div className={"flex flex-col gap-5"} ref={animationParent}>
      <ConnectGithubAccount />

      <PasswordActionCard />
      <AccountEmails />
      <UpdateEmailForm />
      <UpdateDisplayEmailForm />
      <DeleteAccountForm />
    </div>
  );
};

export default AccountSettings;
