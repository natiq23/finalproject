import PinnedRepositories from "../../../components/form/pinned-repositories/index.js";
import ConnectGithubAccount from "../../../components/form/connect-github-account/index.js";

const ExtensionsSettings = () => {
  return (
    <div className={"flex flex-col gap-2"}>
      <ConnectGithubAccount />

      <PinnedRepositories />
    </div>
  );
};

export default ExtensionsSettings;
