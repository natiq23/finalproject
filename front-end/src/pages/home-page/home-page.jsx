import { motion } from "framer-motion";
import LastQuestions from "../../components/pages/home-page/last-questions/index.js";
import LastArticles from "../../components/pages/home-page/last-articles/index.js";
import { useAutoAnimate } from "@formkit/auto-animate/react";
import { Helmet } from "react-helmet";
import Article from "../../components/pages/articles-page/article/index.js";
import { useEffect, useState } from "react";
import { getFeed } from "../../services/article.js";
import ArticleBigSkeleton from "../../components/skeleton-loaders/article-big-skeleton/index.js";
import LastResources from "../../components/pages/home-page/last-resources/index.js";
import { useMediaQuery } from "react-responsive";
import classNames from "classnames";

const HomePage = () => {
  const { animationParent } = useAutoAnimate();
  const [articles, setArticles] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const isSmall = useMediaQuery({ query: "(max-width: 1024px)" });

  useEffect(() => {
    setIsLoading(true);
    getFeed()
      .then((res) => setArticles(res))
      .finally(() => {
        setIsLoading(false);
      });
  }, []);

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      className={classNames({
        "w-full flex justify-between  gap-4 py-4": !isSmall,
        "w-full gap-4 py-4": isSmall,
      })}
      ref={animationParent}
    >
      <div className={"w-full flex flex-col gap-6"}>
        <Helmet>
          <meta charSet="utf-8" />
          <title>Clubrick Community</title>
        </Helmet>

        <div className={"w-full flex flex-col gap-5"}>
          {isLoading ? (
            <div>
              <ArticleBigSkeleton />
              <ArticleBigSkeleton />
              <ArticleBigSkeleton />
            </div>
          ) : (
            articles.map((article) => (
              <Article key={article.id} article={article} />
            ))
          )}
        </div>
      </div>

      <div className={"flex flex-col gap-5"}>
        <LastArticles />
        <LastQuestions />
        <LastResources />
      </div>
    </motion.div>
  );
};

export default HomePage;
