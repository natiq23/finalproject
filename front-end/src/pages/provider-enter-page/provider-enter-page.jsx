import { supabase } from "../../client.js";
import { getEnterWithProviderDTO, signOut } from "../../supabase.js";
import { AuthService } from "../../services/";
import {
  startLoadingProcess,
  stopLoadingProcess,
} from "../../utils/process.js";
import { useEffect } from "react";
import { signInUser } from "../../utils/auth.js";
import { saveUser } from "../../utils/user.js";
import { useNavigate } from "react-router-dom";
import { getAppSettings } from "../../services/user.js";
import { updateSettings } from "../../utils/appSettings.js";

const ProviderEnterPage = () => {
  const navigate = useNavigate();

  useEffect(() => {
    startLoadingProcess();
    supabase.auth.getUser().then(({ data }) => {
      const dto = getEnterWithProviderDTO(data);
      AuthService.sigInWithProvider(dto)
        .then((result) => {
          stopLoadingProcess();
          signInUser(result, true);

          if (result.isProfileBuilt) {
            saveUser(result);
            startLoadingProcess("Reading App Settings");
            getAppSettings()
              .then((res) => {
                updateSettings(res);
              })
              .catch(() => {})
              .finally(() => {
                stopLoadingProcess();
              });
          }
          signOut();

          navigate("/");
        })
        .catch((error) => {
          stopLoadingProcess();
          console.log(error);
        });
    });
  }, []);
  return <div></div>;
};

export default ProviderEnterPage;
