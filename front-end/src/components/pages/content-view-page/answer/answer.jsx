import UserPreviewHoverCard from "../../../common/user-preview-hover-card/index.js";
import DotDivider from "../../../common/dot-divider/index.js";
import { formatDate } from "../../../../utils/common.js";
import PropTypes from "prop-types";
import MarkdownViewer from "../../../markdown-viewer/index.js";
import LikeButton from "../../../buttons/like-button/index.js";
import { SlBadge } from "react-icons/sl";
import TooltipContainer from "../../../common/tooltip-container/index.js";
import { useEffect, useState } from "react";
import { errorToast, successToast } from "../../../../utils/toast.js";
import MoreOptions from "../../../common/more-options/index.js";
import { setCorrectAnswer } from "../../../../services/question.js";
import classNames from "classnames";
import { useUserSignedIn } from "../../../../utils/auth.js";
import { useUser } from "../../../../utils/user.js";
import { createModal } from "../../../../utils/modal.js";
import { MdOutlineDelete } from "react-icons/md";

const Answer = ({
  answer,
  questionId,
  authorId,
  correctAnswerId,
  onDelete,
}) => {
  const isAuthorAnswer = answer.userContent.userId === authorId;

  const [body, setBody] = useState(answer.body);

  const signedIn = useUserSignedIn();
  const userId = useUser()?.id;

  const [dropdownItems, setDropdownItems] = useState([]);

  const handleSetCorrectAnswer = () => {
    console.log(questionId);
    setCorrectAnswer(questionId, answer.id)
      .then(() => {
        successToast("Correct Answer Set");
      })
      .catch((error) => {
        errorToast(error.message ?? "A problem occurred");
      });
  };

  const handleOnUpdate = (values) => {
    setBody(values.body);
  };

  const handleEditAnswer = () => {
    createModal("entryEdit", {
      type: "answers",
      title: "answer",
      id: answer.id,
      body: body,
      onUpdate: handleOnUpdate,
    });
  };
  useEffect(() => {
    let options = [];

    if (userId === authorId) {
      if (correctAnswerId === answer.id)
        options.push({
          label: "Remove Correct Answer",
          onClick: () => {
            handleSetCorrectAnswer();
          },
          type: "danger-item",
        });
      else if (!correctAnswerId)
        options.push({
          label: "Set Correct Answer",
          onClick: () => {
            handleSetCorrectAnswer();
          },
          type: "green-item",
        });
    }
    if (answer.userContent.userId === userId) {
      options.push({
        label: "Edit answer",
        onClick: () => {
          handleEditAnswer();
        },
        type: "item",
      });
      if (correctAnswerId !== answer.id)
        options.push({
          label: "Delete Answer",
          onClick: () => {
            createModal("contentDelete", {
              contentId: answer.id,
              contentType: "answers",
              onDelete: () => {
                onDelete(answer.id);
              },
            });
          },
          type: "danger-item",
        });
    }

    setDropdownItems(options);
  }, [correctAnswerId]);

  return (
    <div className={"w-full flex gap-2"}>
      <img
        src={answer.userContent.profilePhoto}
        alt="Avatar"
        className={"w-8 h-8 rounded-full object-cover mt-2"}
      />
      <div className={"w-full flex flex-col gap-2"}>
        <div
          className={classNames({
            "full-card-schema !p-3 !border-[1px] dark:hover:border-gray-700": true,
            "!border-green-400 dark:!border-green-500":
              correctAnswerId === answer.id,
          })}
        >
          {/*Avatar*/}
          <div className={"flex items-center justify-between"}>
            <div className={"flex items-center gap-1"}>
              <UserPreviewHoverCard userId={answer.userContent.userId}>
                <div
                  className={
                    "rounded-md dark:text-gray-300 p-1 hover:bg-border-clr hover:dark:bg-dark-hover-bg-clr smooth-animation"
                  }
                >
                  <span className={" font-semibold cursor-pointer"}>
                    {answer.userContent.name}
                  </span>
                </div>
              </UserPreviewHoverCard>
              {isAuthorAnswer && (
                <TooltipContainer tooltip={"Author"}>
                  <div className={"dark:text-gray-300"}>
                    <SlBadge />
                  </div>
                </TooltipContainer>
              )}
              <DotDivider size={20} />
              <span className={"text-sm text-second-text dark:text-gray-500"}>
                {formatDate(answer.updateTime)}
              </span>
            </div>
            {signedIn && dropdownItems && dropdownItems.length > 0 && (
              <MoreOptions items={dropdownItems} />
            )}
          </div>
          <div className={"pl-1"}>
            <MarkdownViewer source={body} />
          </div>
          {/*Body*/}
        </div>
        <LikeButton
          contentType={"answers"}
          contentId={answer.id}
          vote={answer.voteDTO}
        />
      </div>
    </div>
  );
};

Answer.propTypes = {
  answer: PropTypes.object,
  authorId: PropTypes.string,
  correctAnswerId: PropTypes.bool,
  questionId: PropTypes.string,
  onDelete: PropTypes.func,
};

export default Answer;
