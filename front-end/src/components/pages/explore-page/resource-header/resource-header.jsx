import DotDivider from "../../../common/dot-divider/index.js";
import { formatDate, share } from "../../../../utils/common.js";
import SaveButton from "../../../buttons/save-button/index.js";
import MoreOptions from "../../../common/more-options/index.js";
import { PiShareFat } from "react-icons/pi";
import { GoReport } from "react-icons/go";
import PropTypes from "prop-types";
import { BiLink } from "react-icons/bi";
import { successToast } from "../../../../utils/toast.js";
import { writeClipboard } from "../../../../utils/clipboard.js";
import UserPreviewHoverCard from "../../../common/user-preview-hover-card/index.js";
import { useEffect, useState } from "react";
import { FiEdit } from "react-icons/fi";
import { MdOutlineDelete } from "react-icons/md";
import { useUser } from "../../../../utils/user.js";
import { startReportAbuseProcess } from "../../../../utils/process.js";
import { useNavigate } from "react-router-dom";
import { createModal } from "../../../../utils/modal.js";
import TagPreview from "../../../common/tag-preview/index.js";

const ResourceHeader = ({
  resource,
  updateResource,
  onDeleteResource,
  mode,
}) => {
  const { id, user, title, updateTime, category, flag, isSaved, url } =
    resource;
  const currentId = useUser()?.id ?? -1;
  const [dropdownItems, setDropdownItems] = useState([]);

  const navigate = useNavigate();

  const handleUpdateResource = (updatedResource) =>
    updateResource(updatedResource);

  useEffect(() => {
    const options = [
      {
        label: "Copy Resource Link",
        icon: <BiLink />,
        onClick: () => {
          writeClipboard(url).then(() => successToast("Resource Link Copied"));
        },
        type: "item",
      },
      {
        label: "Share Resource",
        icon: <PiShareFat />,
        onClick: () => {
          share(title, url);
        },
        type: "item",
      },
    ];

    if (currentId && user.userId === currentId) {
      options.push(
        {
          type: "separator",
        },
        {
          label: "Edit Resource",
          icon: <FiEdit size={22} />,
          onClick: () => {
            createModal("editResource", {
              resource,
              updateResource: handleUpdateResource,
            });
          },
          type: "warning-item",
        },
        {
          label: "Delete Resource",
          icon: <MdOutlineDelete />,
          onClick: () => {
            createModal("contentDelete", {
              contentId: id,
              contentType: "resource",
              onDelete: onDeleteResource,
            });
          },
          type: "danger-item",
        }
      );
    } else {
      options.push(
        {
          type: "separator",
        },
        {
          label: "Report Resource",
          icon: <GoReport />,
          onClick: () => {
            navigate(`/report-abuse?url=/explore/${id}`);
          },
          type: "danger-item",
        }
      );
    }

    setDropdownItems(options);
  }, []);

  return (
    <div className={"flex items-center justify-between gap-3 "}>
      {/*Avatar UserInfo & Timer*/}
      <div className={"flex items-center gap-3"}>
        {/*Avatar*/}
        <img
          className={"w-8 h-8 rounded-full object-cover"}
          src={user.profilePhoto}
          alt="avatar"
        />

        {/*Name*/}
        <UserPreviewHoverCard userId={resource.user.userId}>
          <div
            className={
              "rounded-md dark:text-gray-300 p-1 hover:bg-border-clr hover:dark:bg-dark-hover-bg-clr smooth-animation"
            }
          >
            <span className={"font-medium cursor-pointer"}>{user.name}</span>
          </div>
        </UserPreviewHoverCard>

        <DotDivider />

        {/*Time*/}
        <span
          className={
            "text-xs text-second-text dark:text-gray-500 ml-1 font-medium"
          }
        >
          {formatDate(updateTime)}
        </span>

        <DotDivider />

        <div className={"flex items-center gap-5"}>
          <TagPreview
            title={category.title}
            colorId={category.accentColor}
            isSecond={true}
          />
          <TagPreview
            title={flag.title}
            colorId={flag.accentColor}
            isSecond={true}
          />
        </div>
      </div>
      {mode === "component" ? (
        <div className={"flex gap-3"}>
          <SaveButton
            isSaved={isSaved}
            contentType={"resource"}
            contentId={id}
          />

          <MoreOptions items={dropdownItems} />
        </div>
      ) : null}
    </div>
  );
};

ResourceHeader.propTypes = {
  resource: PropTypes.object,
  updateResource: PropTypes.func,
  onDeleteResource: PropTypes.func,
  mode: PropTypes.string,
};

export default ResourceHeader;
