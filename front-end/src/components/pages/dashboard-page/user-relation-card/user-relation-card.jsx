import FollowButton from "../../../buttons/follow-button/index.js";
import PropTypes from "prop-types";

const UserRelationCard = ({ user }) => {
  return (
    <div className={"h-56 full-card-schema !w-80 !items-center !gap-2"}>
      {/*Header*/}
      <img
        src={user.profilePhoto}
        alt=""
        className={"w-16 h-16 rounded-full object-cover"}
      />
      <a
        href={`/${user.username}`}
        className={
          "text-xl font-bold text-accent dark:text-dark-text-accent hover:text-accent hover:dark:text-dark-text-accent"
        }
      >
        {user.name}
      </a>
      <a
        href={`/${user.username}`}
        className={"font-sm cursor-pointer dark:text-gray-300"}
      >
        @{user.username}
      </a>

      <div className={"w-20 h-full flex justify-center items-end"}>
        <FollowButton targetId={user.userId} isFollowes={user.isUserFollowed} />
      </div>
    </div>
  );
};

UserRelationCard.propTypes = {
  user: PropTypes.object,
};
export default UserRelationCard;
