import PropTypes from "prop-types";
import classNames from "classnames";
import {
  formatDate,
  getBudHistoryTypeTitle,
} from "../../../../utils/common.js";
import { Link } from "react-router-dom";
import ErrorView from "../../../common/error-view/index.js";
import DotDivider from "../../../common/dot-divider/index.js";

const BudHistoryView = ({ data }) => {
  const bud = data.budsDTO;
  const entry = JSON.parse(data.entryDTO);
  console.log(entry);

  const renderTitle = () => {
    switch (bud.forWhat) {
      case "ViewArticleToUser":
        return (
          <div className={"flex items-center gap-1"}>
            <p>You read</p>
            <Link
              to={`/articles/${entry.Id}`}
              target={"_blank"}
              className={
                "text-lg font-bold dark:text-gray-300 hover:text-accent hover:dark:text-dark-text-accent"
              }
            >
              {entry.Header.Title}
            </Link>
          </div>
        );
      case "ViewArticleToOwner":
        return (
          <div className={"flex items-center gap-1"}>
            <p>A user read your</p>
            <Link
              to={`/articles/${entry.Id}`}
              target={"_blank"}
              className={
                "text-lg font-bold dark:text-gray-300 hover:text-accent hover:dark:text-dark-text-accent"
              }
            >
              {entry.Header.Title}
            </Link>
            <p>article!</p>
          </div>
        );
      case "VisitResourceToOwner":
        return "Visit Resource By User";
      case "PublishArticleToOwner":
        return (
          <div className={"flex items-center gap-1"}>
            <p>You published</p>
            <Link
              to={`/articles/${entry.Id}`}
              target={"_blank"}
              className={
                "text-lg font-bold dark:text-gray-300 hover:text-accent hover:dark:text-dark-text-accent"
              }
            >
              {entry.Header.Title}
            </Link>
            <p>with community!</p>
          </div>
        );
      case "PublishQuestionToOwner":
        return (
          <div className={"flex items-center gap-1"}>
            <p>You asked help from community with</p>
            <Link
              to={`/questions/${entry.Id}`}
              target={"_blank"}
              className={
                "text-lg font-bold dark:text-gray-300 hover:text-accent hover:dark:text-dark-text-accent"
              }
            >
              {entry.Header.Title}
            </Link>
          </div>
        );
      case "PublishResourceToOwner":
        return (
          <div className={"flex items-center gap-1"}>
            <p>You shared</p>
            <Link
              to={entry.Url}
              target={"_blank"}
              className={
                "text-lg font-bold dark:text-gray-300 hover:text-accent hover:dark:text-dark-text-accent"
              }
            >
              {entry.Title}
            </Link>
            <p>with community!</p>
          </div>
        );
      case "ProductPurchase":
        return (
          <div className={"flex items-center gap-1"}>
            <p>You purchased</p>
            <Link
              to={`/shop/${entry.Id}`}
              target={"_blank"}
              className={
                "text-lg font-bold dark:text-gray-300 hover:text-accent hover:dark:text-dark-text-accent"
              }
            >
              {entry.Name}
            </Link>
          </div>
        );
      default:
        return <ErrorView message={"Unsupported bud type"} />;
    }
  };

  return (
    <div className={"full-card-schema !flex-row !justify-between"}>
      <div className={"flex flex-col gap-0.5"}>
        <div className={"flex items-center"}>
          <p className={"text-sm text-second-text dark:text-dark-text"}>
            {getBudHistoryTypeTitle(bud.forWhat)}
          </p>
          <DotDivider />
          <p className={"text-sm text-second-text dark:text-dark-text"}>
            {formatDate(bud.createdTime)}
          </p>
        </div>
        <div>{renderTitle()}</div>
      </div>
      <div className={"flex items-center gap-1 "}>
        <p
          className={classNames({
            "text-green-400 text-lg  font-medium mt-0 ": true,
            "!text-red-400": bud.budsAmount < 0,
          })}
        >
          {bud.budsAmount > 0 && "+ "}
          {bud.budsAmount}
        </p>
        <img src={`/bud.png`} alt={"bud icon"} className={"h-5"} />
      </div>
    </div>
  );
};

BudHistoryView.propTypes = {
  data: PropTypes.object,
};
export default BudHistoryView;
