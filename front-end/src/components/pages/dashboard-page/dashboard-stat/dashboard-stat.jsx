import PropTypes from "prop-types";
import { IconContext } from "react-icons";
import { useEffect, useState } from "react";
import { getDashboardStats } from "../../../../services/user.js";
import { ThreeDots } from "react-loader-spinner";

const DashboardStat = ({ endpoint, label, icon, color }) => {
  const [stat, setStat] = useState(null);

  useEffect(() => {
    getDashboardStats(endpoint)
      .then((res) => setStat(res))
      .catch((e) => {
        console.log(e.message);
      });
  }, [endpoint]);

  return (
    <div className={"!w-full !h-full !p-8  full-card-schema"}>
      <div className={"flex justify-between "}>
        {stat !== null ? (
          <span className={" dark:text-gray-300 text-3xl font-bold"}>
            {stat}
          </span>
        ) : (
          <div className={"w-full flex items-center justify-center"}>
            <ThreeDots
              height="24"
              width="24"
              color={"#3B49DF"}
              wrapperClass="radio-wrapper"
              radius="9"
              ariaLabel="three-dots-loading"
              wrapperStyle={{}}
              wrapperClassName=""
              visible={true}
            />
          </div>
        )}
        <IconContext.Provider value={{ size: 26, color: color }}>
          {icon}
        </IconContext.Provider>
      </div>
      <p className={"text-dark-text dark:text-gray-500"}>{label}</p>
    </div>
  );
};

DashboardStat.propTypes = {
  endpoint: PropTypes.string,
  label: PropTypes.string,
  icon: PropTypes.elementType,
  color: PropTypes.string,
};
export default DashboardStat;
