import LastListings from "../last-listings/index.js";
import { useEffect, useState } from "react";
import { lastArticles } from "../../../../services/lastListings.js";
import { ThreeDots } from "react-loader-spinner";
import { useMediaQuery } from "react-responsive";

const LastArticles = () => {
  const [articles, setArticles] = useState([]);

  const [isLoading, setIsLoading] = useState(false);

  const isSmall = useMediaQuery({ query: "(max-width: 1024px)" });

  useEffect(() => {
    if (isSmall) return;

    setIsLoading(true);
    lastArticles()
      .then((res) => {
        let data = res.map((a) => {
          return {
            title: a.title,
            url: `/articles/${a.id}`,
            description: a.tags.map((t) => `#${t}`).join("   "),
          };
        });
        setArticles(data);
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        setIsLoading(false);
      });
  }, []);

  if (isSmall) return null;

  return isLoading ? (
    <div className={"!w-80 full-card-schema items-center"}>
      <ThreeDots
        height="24"
        width="24"
        color={"#3B49DF"}
        wrapperClass="radio-wrapper"
        radius="9"
        ariaLabel="three-dots-loading"
        wrapperStyle={{}}
        wrapperClassName=""
        visible={true}
      />
    </div>
  ) : (
    <LastListings
      key={"Last Articles"}
      title={"Last Articles"}
      data={articles}
      seeAllUrl={"/articles"}
    />
  );
};

export default LastArticles;
