import LastListings from "../last-listings/index.js";
import { useEffect, useState } from "react";
import { lastQuestion } from "../../../../services/lastListings.js";
import { ThreeDots } from "react-loader-spinner";
import { useMediaQuery } from "react-responsive";

const LastQuestions = () => {
  const [questions, setQuestions] = useState([]);

  const [isLoading, setIsLoading] = useState(false);

  const isSmall = useMediaQuery({ query: "(max-width: 1024px)" });

  useEffect(() => {
    if (isSmall) return;

    setIsLoading(true);
    lastQuestion()
      .then((res) => {
        let data = res.map((q) => {
          return {
            title: q.title,
            url: `/questions/${q.id}`,
            description: `${q.answerCount} answers`,
          };
        });
        setQuestions(data);
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        setIsLoading(false);
      });
  }, []);

  if (isSmall) return null;

  return isLoading ? (
    <div className={"!w-80 full-card-schema items-center"}>
      <ThreeDots
        height="24"
        width="24"
        color={"#3B49DF"}
        wrapperClass="radio-wrapper"
        radius="9"
        ariaLabel="three-dots-loading"
        wrapperStyle={{}}
        wrapperClassName=""
        visible={true}
      />
    </div>
  ) : (
    <LastListings
      key={"Last Questions"}
      title={"Last Questions"}
      data={questions}
      seeAllUrl={"/form"}
    />
  );
};

export default LastQuestions;
