import IconButton from "../../../buttons/icon-button/index.js";
import { RiExpandUpDownFill } from "react-icons/ri";
import { FaRegComment } from "react-icons/fa";
import SaveButton from "../../../buttons/save-button/index.js";
import PropTypes from "prop-types";
import { useNavigate } from "react-router-dom";
import { useMediaQuery } from "react-responsive";

const ArticleFooter = ({ article }) => {
  const url = `/articles/${article.id}`;

  const navigate = useNavigate();

  const isSmall = !useMediaQuery({ query: "(min-width: 550px)" });

  return (
    <div className={"w-full flex items-center justify-between px-[5%]"}>
      <div className={" flex items-center gap-3"}>
        {/*Vote*/}
        {article.voteDTO.voteCount !== 0 && (
          <IconButton
            icon={<RiExpandUpDownFill />}
            title={
              isSmall
                ? article.voteDTO.voteCount
                : `${article.voteDTO.voteCount} Votes`
            }
            click={() => {
              navigate(url);
            }}
          />
        )}
        {/*Comment*/}
        <IconButton
          icon={<FaRegComment />}
          title={
            isSmall
              ? article.commentCount
              : article.commentCount > 0
              ? `${article.commentCount} Comments`
              : "Add comment"
          }
          click={() => {
            navigate(url);
          }}
        />
      </div>

      <div className={"flex items-center gap-3"}>
        {/*Reading time*/}
        <span className={"text-sm dark:text-gray-400"}>
          {article.readingTime} min read
        </span>
        {/*Save*/}
        <SaveButton
          isSaved={article.isSaved}
          contentId={article.id}
          contentType={"articles"}
        />
      </div>
    </div>
  );
};

ArticleFooter.propTypes = {
  article: PropTypes.object,
};

export default ArticleFooter;
