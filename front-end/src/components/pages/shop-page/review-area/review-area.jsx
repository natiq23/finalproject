import PropTypes from "prop-types";
import { useUserSignedIn } from "../../../../utils/auth.js";
import { useUser } from "../../../../utils/user.js";
import { useEffect, useState } from "react";
import { getUtcNowDataFormat } from "../../../../utils/common.js";
import WriteReview from "../write-review/index.js";
import { getReviews } from "../../../../services/shop.js";
import ReviewView from "../review-view/index.js";
import { ThreeDots } from "react-loader-spinner";

const ReviewArea = ({ productId }) => {
  const signedIn = useUserSignedIn();
  const { id, name, avatar } = useUser();

  const [localReviews, setLocalReviews] = useState([]);
  const [loading, setLoading] = useState(true);
  const [currentHasReview, setCurrentHasReview] = useState(false);

  const handleReviewPublished = (review) => {
    const newReview = { ...review };
    newReview.voteDTO = { voteCount: 0, status: "UnVote" };
    newReview.updateAt = getUtcNowDataFormat();
    newReview.userContent = {
      userId: id,
      name: name,
      profilePhoto: avatar,
    };
    const newReviews = [newReview, ...localReviews];
    setLocalReviews(newReviews);
  };

  const handleOnDelete = (id) => {
    const newReviews = [...localReviews];

    setLocalReviews(newReviews.filter((a) => a.id !== id));
  };

  useEffect(() => {
    setLoading(true);
    getReviews(productId)
      .then((res) => {
        setLocalReviews(res);
      })
      .finally(() => {
        setLoading(false);
      });
  }, []);

  useEffect(() => {
    const index = localReviews?.findIndex((a) => a.userContent?.userId === id);
    setCurrentHasReview(index > -1);
  }, [localReviews]);

  return (
    <div
      className={
        "flex flex-col gap-6 card-size-schema !border-t-[1px] !border-transparent  !rounded-t-none"
      }
    >
      {loading ? (
        <div className={"w-full flex items-center justify-center"}>
          <ThreeDots
            height="24"
            width="24"
            color={"#3B49DF"}
            wrapperClass="radio-wrapper"
            radius="9"
            ariaLabel="three-dots-loading"
            wrapperStyle={{}}
            wrapperClassName=""
            visible={true}
          />
        </div>
      ) : (
        <>
          {" "}
          <span className={"dark:text-gray-300 text-2xl font-semibold"}>
            Total Reviews ({localReviews?.length ?? 0})
          </span>
          {signedIn && !currentHasReview && (
            <WriteReview
              productId={productId}
              onReviewPublished={handleReviewPublished}
            />
          )}
          <div className={"flex flex-col gap-6"}>
            {localReviews &&
              localReviews.map((review) => (
                <ReviewView
                  key={review.id}
                  review={review}
                  onDelete={handleOnDelete}
                />
              ))}
          </div>
        </>
      )}
    </div>
  );
};

ReviewArea.propTypes = {
  productId: PropTypes.string,
};

export default ReviewArea;
