import PropTypes from "prop-types";
import classNames from "classnames";
import Divider from "../../../common/divider/index.js";
import PrimaryAccentButton from "../../../buttons/primary-accent-button/index.js";
import QuantityInput from "../../../form/quantity-input/index.js";
import { SlBasket } from "react-icons/sl";
import { useEffect, useState } from "react";
import { useAutoAnimate } from "@formkit/auto-animate/react";
import TooltipContainer from "../../../common/tooltip-container/index.js";
import ProgressBarChart from "../../../common/progress-bar-chart/index.js";
import { FaStar } from "react-icons/fa6";
import PriceIndicator from "../../../common/price-indicator/index.js";
import { createModal } from "../../../../utils/modal.js";
import { getProductRanking } from "../../../../services/shop.js";
import { ThreeDots } from "react-loader-spinner";

const ProductViewPageInfo = ({ product }) => {
  const { animationParent } = useAutoAnimate();

  const {
    id,
    quantity,
    sellCount,
    rank,
    price,
    reviewsCount,
    discount,
    discountedPrice,
  } = product;

  const [buyQuantity, setBuyQuantity] = useState(1);
  const [ranking, setRanking] = useState(null);
  const [isRankingLoading, setIsRankingLoading] = useState(true);

  useEffect(() => {
    setIsRankingLoading(true);
    getProductRanking(id)
      .then((res) => {
        console.log(res);
        setRanking(res);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        setIsRankingLoading(false);
      });
  }, [product]);

  return (
    <div className={"card-color-schema w-full px-8"}>
      <div className={"flex items-center justify-between mt-8 mb-6"}>
        <p
          className={classNames({
            "text-sm font-medium": true,
            "text-green-400": quantity > 0,
            "text-red-400": quantity <= 0,
          })}
        >
          {quantity > 0 ? "Product in stock" : "Product out of stock"}
        </p>
        <p className={"text-dark-dark-text text-sm font-medium"}>
          Product sold: <span className={"italic font-bold"}>{sellCount}</span>
        </p>
      </div>

      <Divider />

      <div
        className={"flex flex-col items-start  gap-2 my-6"}
        ref={animationParent}
      >
        {discount > 0 && (
          <div className={"flex items-center gap-2"}>
            <span
              className={
                "border border-green-400 rounded-md px-1 text-green-400 flex items-center gap-1"
              }
            >
              -{Math.floor(price - discountedPrice)}
              <img src={`/bud.png`} alt={"bud icon"} className={"h-3"} />
              {/*-{discount}%*/}
            </span>
            <PriceIndicator overLine={true} price={price} isBig={true} />
          </div>
        )}
        <div className={"flex items-center"}>
          <TooltipContainer tooltip={"Product price"}>
            <p className=" text-3xl font-extrabold mt-0">{discountedPrice}</p>
          </TooltipContainer>

          {buyQuantity > 1 && (
            <div
              className={"ml-1 text-3xl font-extrabold flex items-center gap-2"}
            >
              <span>x</span>
              <div>
                <p className="">{buyQuantity} </p>
              </div>
              <span>=</span>
              <TooltipContainer tooltip={"Total price"}>
                <p className="">{price * buyQuantity}</p>
              </TooltipContainer>
            </div>
          )}

          <img src={`/bud.png`} alt={"bud icon"} className={"h-8"} />
        </div>
      </div>

      <Divider />

      <div className={"flex flex-col gap-4 justify-start my-5"}>
        <QuantityInput
          max={quantity}
          defaultValue={1}
          value={buyQuantity}
          setValue={setBuyQuantity}
        />
        {/*<div className={"w-[160px]"}>*/}
        <PrimaryAccentButton
          title={"Buy"}
          icon={<SlBasket />}
          click={() => {
            createModal("buyProduct", {
              product,
              buyQuantity,
            });
          }}
        />
        {/*</div>*/}
      </div>

      <Divider />

      <div className={"w-full grid grid-cols-[100px,1fr] gap-1 my-6"}>
        <div className={"flex flex-col gap-1 items-center justify-center"}>
          <p className={"text-3xl font-extrabold"}>{rank}</p>
          <p className={"text-sm"}>{reviewsCount ?? 0} reviews</p>
        </div>

        {isRankingLoading ? (
          <div className={"w-full flex items-center justify-center"}>
            <ThreeDots
              height="24"
              width="24"
              color={"#3B49DF"}
              wrapperClass="radio-wrapper"
              radius="9"
              ariaLabel="three-dots-loading"
              wrapperStyle={{}}
              wrapperClassName=""
              visible={true}
            />
          </div>
        ) : (
          <ProgressBarChart
            data={{
              "5 ": ranking["5"] ?? 0,
              "4 ": ranking["4"] ?? 0,
              "3 ": ranking["3"] ?? 0,
              "2 ": ranking["2"] ?? 0,
              "1 ": ranking["1"] ?? 0,
            }}
            labelIcon={<FaStar />}
            header={"Reviews"}
          />
        )}
      </div>
    </div>
  );
};

ProductViewPageInfo.propTypes = {
  product: PropTypes.object,
};

export default ProductViewPageInfo;
