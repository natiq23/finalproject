import DotDivider from "../../../common/dot-divider/index.js";
import { FaStar } from "react-icons/fa6";
import PrimaryAccentButton from "../../../buttons/primary-accent-button/index.js";
import PropTypes from "prop-types";
import ProductFavButton from "../product-fav-button/index.js";
import { useNavigate } from "react-router-dom";
import { SlBasket } from "react-icons/sl";
import { TbShoppingBagEdit } from "react-icons/tb";
import PriceIndicator from "../../../common/price-indicator/index.js";
import { createModal } from "../../../../utils/modal.js";

const ProductCard = ({ product, isManage }) => {
  const navigate = useNavigate();

  return (
    <div
      className="min-w-[300px] bg-white dark:bg-dark-component-bg  rounded-md  p-2 mx-1 my-3 cursor-pointer dark:text-white hover:drop-shadow-lg hover:dark:shadow-gray-800 shadow smooth-animation "
      onClick={() => {
        navigate(`${product.id}`);
      }}
    >
      <div className="overflow-x-hidden rounded-md relative">
        {product.discount > 0 && (
          <div className="absolute left-2 top-2 cursor-pointer group bg-red-400 p-0.5 px-1 flex items-center rounded-full">
            <sapn className=" text-sm font-semibold">-{product.discount}%</sapn>
          </div>
        )}
        <img
          alt={"product"}
          className="h-40 rounded-md w-full object-cover"
          src={product.photos[0].path}
        />
        <div className="absolute right-2 top-2 cursor-pointer group">
          <ProductFavButton
            isFavourited={product.isFavourited}
            productId={product.id}
          />
        </div>
      </div>
      <div className="w-full grid grid-cols-[1fr,100px] mt-4  mb-2 ">
        <div className={"w-full flex flex-col"}>
          <p className="w-full dark:text-white text-lg font-semibold text-gray-900 ">
            {product.name}
          </p>
          <div className={"flex flex-col items-start "}>
            <div className={"flex items-center"}>
              {product.discount > 0 && (
                <div className={"flex items-center"}>
                  <PriceIndicator price={product.price} overLine={true} />
                  <DotDivider />
                </div>
              )}
              <PriceIndicator price={product.discountedPrice} />

              <DotDivider />

              <div className={"flex items-center gap-1"}>
                <span>{product.rank}</span>
                <FaStar fill={"#fde047"} size={18} />
              </div>
            </div>
            {/*<RatingIndicator rating={innerProduct.rank} />*/}
          </div>
        </div>
        <div className="flex h-full items-center mb-1  group cursor-pointer">
          {isManage ? (
            <PrimaryAccentButton
              title={"Edit"}
              icon={<TbShoppingBagEdit />}
              click={() => {
                navigate(`/manage/moderator/products/${product.id}/update`);
              }}
            />
          ) : (
            <PrimaryAccentButton
              title={"Buy"}
              icon={<SlBasket />}
              click={() => {
                createModal("buyProduct", {
                  product,
                  buyQuantity: 1,
                });
              }}
            />
          )}
        </div>
      </div>
    </div>
  );
};

ProductCard.propTypes = {
  product: PropTypes.object,
  isManage: PropTypes.bool,
};

export default ProductCard;
