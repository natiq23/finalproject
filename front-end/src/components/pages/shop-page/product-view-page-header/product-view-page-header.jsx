import PropTypes from "prop-types";
import ProductFavButton from "../product-fav-button/index.js";
import IconButton from "../../../buttons/icon-button/index.js";
import { GrShareOption } from "react-icons/gr";
import ProductRatingFull from "../product-rating-full/index.js";
import { useUserRole } from "../../../../utils/auth.js";
import { TbShoppingBagEdit } from "react-icons/tb";
import { useNavigate } from "react-router-dom";
import { share } from "../../../../utils/common.js";

const ProductViewPageHeader = ({ product }) => {
  const role = useUserRole();

  const navigate = useNavigate();

  return (
    <div
      className={
        "card-color-schema  px-5 w-full h-[70px] grid grid-cols-3 items-center "
      }
      //sticky top-[55px]
    >
      <div className={"flex items-center gap-1.5"}>
        <h1 className={"font-bold dark:text-white text-2sxl ellipsis-overflow"}>
          {product.name}
        </h1>
        {product.discount > 0 && (
          <div className="cursor-pointer group bg-red-400 p-0.5 px-1 flex items-center rounded-full">
            <sapn className=" text-sm font-semibold">-{product.discount}%</sapn>
          </div>
        )}
      </div>
      <ProductRatingFull rating={product.rank} />
      <div className={"flex justify-end items-center grow-0 shrink-0 gap-3"}>
        <ProductFavButton
          isFavourited={product.isFavourited}
          productId={product.id}
          transparent={true}
        />
        <IconButton
          icon={<GrShareOption />}
          size={20}
          click={() => {
            const origin = window.location.origin;
            share(product.name, `${origin}/shop/${product.id}`);
          }}
        />
        {role.toLowerCase() === "admin" && (
          <IconButton
            icon={<TbShoppingBagEdit />}
            size={20}
            click={() => {
              navigate(`/manage/moderator/products/${product.id}/update`);
            }}
          />
        )}
      </div>
    </div>
  );
};

ProductViewPageHeader.propTypes = {
  product: PropTypes.object,
};
export default ProductViewPageHeader;
