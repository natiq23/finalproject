import PropTypes from "prop-types";
import SubpageButton from "../../../buttons/subpage-button/index.js";

const SubpageContainer = ({ pages, basePart }) => {
  return (
    <div className={"flex flex-col gap-1"}>
      {pages.map((page) => (
        <SubpageButton key={page.title} page={page} basePart={basePart} />
      ))}
    </div>
  );
};

SubpageContainer.propTypes = {
  pages: PropTypes.array,
  basePart: PropTypes.string,
};

export default SubpageContainer;
