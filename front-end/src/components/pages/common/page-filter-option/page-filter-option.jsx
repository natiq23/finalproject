import { useState } from "react";
import classNames from "classnames";
import PropTypes from "prop-types";

const PageFilterOption = ({ click, title, isActive = false }) => {
  const [isActiveLocal, setIsActiveLocal] = useState(isActive);

  return (
    <button
      className={classNames({
        "py-2 px-3 dark:text-gray-300  rounded-md  hover:bg-hover-blue-clr hover:dark:bg-dark-hover-blue-clr smooth-animation text-start w-full": true,
        "font-semibold bg-white dark:bg-dark-component-second-bg ":
          isActiveLocal,
      })}
      onClick={() => {
        setIsActiveLocal(!isActiveLocal);
        click(!isActiveLocal);
      }}
    >
      {title}
    </button>
  );
};

PageFilterOption.propTypes = {
  title: PropTypes.string,
  isActive: PropTypes.bool,
  click: PropTypes.func,
};

export default PageFilterOption;
