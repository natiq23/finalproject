import classNames from "classnames";
import PropTypes from "prop-types";

const PageFilterContainerItem = ({ title, paramName, isActive, click }) => {
  return (
    <button
      className={classNames({
        "py-2 px-3 dark:text-gray-300  rounded-md  hover:bg-hover-blue-clr hover:dark:bg-dark-hover-blue-clr smooth-animation text-start w-full": true,
        "font-semibold bg-white dark:bg-dark-component-second-bg ": isActive,
      })}
      onClick={() => {
        click(paramName);
      }}
    >
      {title}
    </button>
  );
};

PageFilterContainerItem.propTypes = {
  title: PropTypes.string,
  isActive: PropTypes.bool,
  click: PropTypes.func,
  paramName: PropTypes.string,
};

export default PageFilterContainerItem;
