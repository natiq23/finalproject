import { useConnectedAccountType, useName } from "../../../../utils/auth.js";
import { createModal } from "../../../../utils/modal.js";
import { useEffect, useState } from "react";
import { convertToBase64 } from "../../../../utils/common.js";
import PropTypes from "prop-types";
import { createAvatar } from "@dicebear/core";
import { botttsNeutral } from "@dicebear/collection";
import classNames from "classnames";
import { useAutoAnimate } from "@formkit/auto-animate/react";
import {
  saveBuildFormProviderAvatarValues,
  useBuildFormProviderAvatarValues,
} from "../../../../utils/process.js";

const ProviderAvatarSelect = ({ avatarUrl, avatarChanged, error }) => {
  const [animationParent] = useAutoAnimate();

  const [avatar, setAvatar] = useState();

  const [selected, setSelected] = useState("");

  const [userAvatar, setUserAvatar] = useState();
  const [newAvatar, setNewAvatar] = useState();

  const initialState = useBuildFormProviderAvatarValues();

  const saveProviderAvatarState = () => {
    saveBuildFormProviderAvatarValues({
      avatar,
      selected,
      userAvatar,
      newAvatar,
    });
  };

  const loadProviderAvatar = () => {
    setAvatar(initialState?.avatar);
    setSelected(initialState?.selected);
    setUserAvatar(initialState?.userAvatar);
    setNewAvatar(initialState?.newAvatar);
  };

  const user = {
    name: useName(),
    connectedType: useConnectedAccountType(),
  };

  const handleSetAvatar = (newUserAvatar) => {
    setNewAvatar(newUserAvatar);
    setSelected("new-avatar");
    setAvatar(newUserAvatar);
  };

  useEffect(() => {
    loadProviderAvatar();
    console.log(avatarUrl);
    convertToBase64([avatarUrl, ""]).then((res) => {
      setUserAvatar(res[0]);
    });

    if (initialState?.avatar) {
      avatarChanged(avatar);
    }
    if (!initialState?.newAvatar) {
      const diceAvatar = createAvatar(botttsNeutral, {
        seed: user.name,
        radius: 50,
      });

      diceAvatar
        .jpeg()
        .toDataUri()
        .then((uri) => setNewAvatar(uri));
      saveProviderAvatarState();
    }
  }, []);

  useEffect(() => {
    saveProviderAvatarState();
    avatarChanged(avatar);
  }, [avatar]);

  const handleUserAvatarSelected = () => {
    setSelected("user-avatar");
    setAvatar(userAvatar);
  };

  const handleNewAvatarSelected = () => {
    setSelected("new-avatar");
    setAvatar(newAvatar);
  };

  return (
    <div
      className={"w-fit flex flex-col gap-5 items-center justify-center "}
      ref={animationParent}
    >
      {/*Header | Name & Avatar*/}
      <div
        className={"flex flex-col justify-center items-center gap-2"}
        ref={animationParent}
      >
        {/* Name */}
        <span className={"font-semibold text-lg"}>{user.name}</span>

        {/*Avatar*/}
        {avatar && (
          <img
            src={avatar}
            alt="Avatar"
            className={"w-20 h-20 rounded-full object-cover"}
          />
        )}
      </div>

      {!avatar && (
        <span
          className={classNames({
            "font-semibold": true,
            "text-red-900": error,
          })}
        >
          Please select a avatar{" "}
        </span>
      )}

      <div className={"flex justify-between gap-8 "}>
        {/* New Avatar */}
        <button
          type={"button"}
          onClick={handleNewAvatarSelected}
          className={classNames({
            "w-60 flex flex-col items-center justify-center gap-3 p-2 rounded-md border-2 border-accent   smooth-animation ": true,
            "border-solid bg-hover-blue-clr hover:border-dashed hover:bg-transparent":
              selected === "new-avatar",
            "border-dashed hover:border-solid hover:bg-hover-blue-clr":
              selected !== "new-avatar",
          })}
        >
          <span className={"font-semibold"}>Pick own avatar</span>

          <div className={"flex justify-center items-center gap-2"}>
            <img
              src={newAvatar}
              alt="Avatar"
              className={"w-20 h-20 rounded-full object-cover "}
            />

            <button
              type={"button"}
              onClick={() => {
                createModal("editProfilePhoto", handleSetAvatar);
              }}
              className={
                "w-fit h-fit p-2 px-3  text-black  font-medium rounded-lg transition-all duration-500 bg-[#d6d6d6] hover:bg-[#a3a3a3] "
              }
            >
              <p>Edit avatar</p>
            </button>
          </div>
        </button>

        {/*User's avatar*/}
        <button
          type={"button"}
          onClick={handleUserAvatarSelected}
          className={classNames({
            "w-60 flex flex-col items-center justify-center gap-3 p-2 rounded-md border-2 border-accent   smooth-animation ": true,
            "border-solid bg-hover-blue-clr hover:border-dashed hover:bg-transparent":
              selected === "user-avatar",
            "border-dashed hover:border-solid hover:bg-hover-blue-clr":
              selected !== "user-avatar",
          })}
        >
          <span className={"font-semibold"}>{user.connectedType} avatar</span>

          <img
            src={userAvatar}
            alt="Avatar"
            className={"w-20 h-20 rounded-full object-cover"}
          />
        </button>
      </div>
    </div>
  );
};

ProviderAvatarSelect.propTypes = {
  avatarUrl: PropTypes.string,
  avatarChanged: PropTypes.func,
  error: PropTypes.any,
};

export default ProviderAvatarSelect;
