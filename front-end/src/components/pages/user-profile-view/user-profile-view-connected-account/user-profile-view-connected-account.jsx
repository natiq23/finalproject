import PropTypes from "prop-types";
import { IconContext } from "react-icons";

const UserProfileViewConnectedAccount = ({ icon, data }) => {
  return (
    <div className={"flex items-center text-dark-text dark:text-gray-600"}>
      <a
        href={data.url}
        target={"_blank"}
        rel="noreferrer"
        className={"underline-animation hover:text-dark-text-accent"}
      >
        <IconContext.Provider value={{ size: 24 }}>{icon}</IconContext.Provider>
      </a>
    </div>
  );
};

UserProfileViewConnectedAccount.propTypes = {
  icon: PropTypes.element,
  data: PropTypes.object,
};

export default UserProfileViewConnectedAccount;
