import PropTypes from "prop-types";
import UserProfileViewPageAsideRepo from "../user-profile-view-page-aside-repo/index.js";

const UserProfileViewRepos = ({ repos }) => {
  if (repos.length <= 0) return null;
  return (
    <div className={"card-color-schema card-shadow-schema"}>
      <div className={"p-4 font-bold dark:text-gray-300 "}>
        GitHub Repositories
      </div>
      <div className={"flex flex-col"}>
        {repos.map((repo) => (
          <UserProfileViewPageAsideRepo key={repo.title} repo={repo} />
        ))}
      </div>
    </div>
  );
};

UserProfileViewRepos.propTypes = {
  repos: PropTypes.array,
};

export default UserProfileViewRepos;
