import UserProfileViewInfo from "../user-profile-view-info/index.js";
import { HiLocationMarker } from "react-icons/hi";
import { FaBirthdayCake } from "react-icons/fa";
import { MdEmail } from "react-icons/md";
import { BiLinkExternal } from "react-icons/bi";
import UserProfileViewSecondaryInfo from "../user-profile-view-secondary-info/index.js";
import PropTypes from "prop-types";
import PrimaryAccentButton from "../../../buttons/primary-accent-button/index.js";
import { useUser } from "../../../../utils/user.js";
import MoreOptions from "../../../common/more-options/index.js";
import { GoReport } from "react-icons/go";
import { useNavigate } from "react-router-dom";
import { formatDate } from "../../../../utils/common.js";
import FollowButton from "../../../buttons/follow-button/index.js";

const UserProfileViewPageHeader = ({ user }) => {
  const { id } = useUser();
  const navigate = useNavigate();

  return (
    <div className={"w-full"}>
      <div
        className={
          "relative flex flex-col items-center !rounded-b-none card-color-schema"
        }
      >
        <div
          className={"absolute rounded-md !rounded-b-none w-full h-20 "}
          style={{
            backgroundColor: user.brand?.accentColor ?? "#222222",
          }}
        ></div>
        <img
          src={user.profilePhoto}
          alt="avatar"
          className={
            "mt-9 z-20 w-28 h-28 rounded-full  border-[10px] box-content object-cover"
          }
          style={{
            borderColor: user.brand?.accentColor ?? "#222222",
          }}
        />
        <div className={"absolute right-6 my-24 z-20 "}>
          {id === user.id ? (
            <PrimaryAccentButton
              title={"Edit Profile"}
              click={() => {
                navigate("/settings/profile");
              }}
            />
          ) : (
            <div className={"flex gap-4 items-center"}>
              <FollowButton targetId={user.id} />
              <MoreOptions
                items={[
                  {
                    label: "Report User",
                    icon: <GoReport />,
                    onClick: () => {
                      const pathname = window.location.pathname;

                      navigate(`/report-abuse?url=${pathname}`);
                    },
                    type: "danger-item",
                  },
                ]}
              />
            </div>
          )}
        </div>
      </div>

      <div
        className={
          "relative w-full top-[-90px] mb-[-90px]  pt-[100px] p-8 !rounded-t-none !rounded-b-none  rounded-y-md z-10 flex flex-col items-center justify-end gap-3 border-t-[1px] border-x-[1px] card-color-schema"
        }
      >
        <span className={"font-bold text-3xl dark:text-gray-300"}>
          {user.name}
        </span>

        <p className={"text-lg max-w-[700px] text-center dark:text-gray-400"}>
          {user.personalInfo?.bio ?? "404 bio not found"}
        </p>
        <div className={"flex items-center gap-8"}>
          <UserProfileViewInfo
            icon={<HiLocationMarker />}
            data={user.personalInfo?.location}
          />
          <UserProfileViewInfo
            icon={<FaBirthdayCake />}
            data={`Joined at ${formatDate(user.joined)}`}
          />
          <UserProfileViewInfo
            icon={<MdEmail />}
            data={user.personalInfo?.displayEmail}
            type={"mail"}
          />
          <UserProfileViewInfo
            icon={<BiLinkExternal />}
            data={user.personalInfo?.websiteUrl}
            type={"url"}
          />
          {/*{user.personalInfo.connectedAccountList &&}*/}
          {/*<UserProfileViewConnectedAccount*/}
          {/*  icon={<AiFillGithub />}*/}
          {/*  data={user.gitHub}*/}
          {/*/>*/}
        </div>
      </div>
      {(user.personalInfo?.education || user.personalInfo?.work) && (
        <div
          className={
            "w-full h-20 flex items-center justify-center gap-16 rounded-b-md bg-component-bg  !rounded-t-none !border-t-[1px] card-color-schema"
          }
        >
          <UserProfileViewSecondaryInfo
            label={"Work"}
            data={user.personalInfo?.work}
          />
          <UserProfileViewSecondaryInfo
            label={"Education"}
            data={user.personalInfo?.education}
          />
        </div>
      )}
    </div>
  );
};

UserProfileViewPageHeader.propTypes = {
  user: PropTypes.object,
};

export default UserProfileViewPageHeader;
