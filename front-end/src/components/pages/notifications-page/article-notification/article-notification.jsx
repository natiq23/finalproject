import PropTypes from "prop-types";
import classNames from "classnames";
import { Link } from "react-router-dom";
import { formatDate } from "../../../../utils/common.js";

const ArticleNotification = ({ data }) => {
  const article = data.content.articleDTO;
  return (
    <div
      className={classNames({
        "full-card-schema !flex-row justify-between items-center  ": true,
        "!border-l-4 !border-l-accent": !data.isViewed,
      })}
    >
      <div className={"w-full flex  gap-3"}>
        <img
          src={article.userContent.profilePhoto}
          alt="avatar"
          className={"w-12 h-12 rounded-full object-cover mt-1"}
        />
        <div className={"w-full flex flex-col gap-2"}>
          <div className={"flex flex-col"}>
            <div className={"flex items-center gap-2"}>
              <Link
                to={`/${article.userContent.username}`}
                className={
                  "text-lg font-bold dark:text-gray-300 hover:text-accent hover:dark:text-dark-text-accent"
                }
              >
                {article.userContent.name}
              </Link>
              <p>made a new article</p>
              <Link
                to={`/questions/${article.id}`}
                className={
                  "text-lg font-bold dark:text-gray-300 hover:text-accent hover:dark:text-dark-text-accent"
                }
              >
                {article.header.title}
              </Link>
            </div>
            <p>{formatDate(data.createdAt)}</p>
          </div>
        </div>
      </div>
    </div>
  );
};

ArticleNotification.propTypes = {
  data: PropTypes.object,
};

export default ArticleNotification;
