import PropTypes from "prop-types";
import FollowButton from "../../../buttons/follow-button/index.js";
import { Link } from "react-router-dom";
import classNames from "classnames";
import DotDivider from "../../../common/dot-divider/index.js";
import { formatDate } from "../../../../utils/common.js";

const FollowNotification = ({ data }) => {
  const { profilePhoto, username, name } = data.content.followerUserProfile;


  return (
    <div
      className={classNames({
        "full-card-schema !flex-row justify-between items-center  ": true,
        "!border-l-4 !border-l-accent": !data.isViewed,
      })}
    >
      <div className={"flex items-center gap-3"}>
        <img
          src={profilePhoto}
          alt="avatar"
          className={"w-12 h-12 rounded-full object-cover"}
        />
        <Link
          to={`/${username}`}
          className={
            "text-lg font-bold dark:text-gray-300 hover:text-accent hover:dark:text-dark-text-accent"
          }
        >
          {name}
        </Link>
        <p>followed you!</p>
        <DotDivider size={18} />
        <p>{formatDate(data.createdAt)}</p>
      </div>
      <div>
        <FollowButton targetId={data.content.followerUserProfile.userId} />
      </div>
    </div>
  );
};

FollowNotification.propTypes = {
  data: PropTypes.object,
};
export default FollowNotification;
