import PropTypes from "prop-types";
import classNames from "classnames";
import { Link } from "react-router-dom";
import { formatDate } from "../../../../utils/common.js";
import MarkdownViewer from "../../../markdown-viewer/index.js";

const WatchNotification = ({ data, isCorrect = false }) => {
  const answer = JSON.parse(data.body).AnswerDTO;
  return (
    <div
      className={classNames({
        "full-card-schema !flex-row justify-between items-center  ": true,
        "!border-l-4 !border-l-accent": !data.isViewed,
      })}
    >
      <div className={"w-full flex  gap-3"}>
        <img
          src={answer.UserContent.ProfilePhoto}
          alt="avatar"
          className={"w-12 h-12 rounded-full object-cover mt-1"}
        />
        <div className={"w-full flex flex-col gap-2"}>
          <div className={"flex flex-col"}>
            <div className={"flex items-center gap-2"}>
              <Link
                to={`/${answer.UserContent.Username}`}
                className={
                  "text-lg font-bold dark:text-gray-300 hover:text-accent hover:dark:text-dark-text-accent"
                }
              >
                {answer.UserContent.Name}
              </Link>
              <p>
                {isCorrect
                  ? "answer selected correctly on"
                  : "answer written to"}
              </p>
              <Link
                to={`/questions/${data.content.questionDTO.id}`}
                className={
                  "text-lg font-bold dark:text-gray-300 hover:text-accent hover:dark:text-dark-text-accent"
                }
              >
                {data.content.questionDTO.header.title}
              </Link>
            </div>
            <p>{formatDate(data.createdAt)}</p>
          </div>
          <span
            className={
              "w-full rounded-md border-[1px] border-border-clr dark:border-dark-border-clr card-size-schema"
            }
          >
            <MarkdownViewer source={data.content.answerDTO.body} />
          </span>
        </div>
      </div>
    </div>
  );
};

WatchNotification.propTypes = {
  data: PropTypes.object,
  isCorrect: PropTypes.bool,
};
export default WatchNotification;
