import PropTypes from "prop-types";

const SideBarNavTitle = ({ title, iconName, type = "png" }) => {
  return (
    <div className={"flex items-center gap-2 "}>
      <img src={`/${iconName}.${type}`} alt={title} className={"h-[20px]"} />
      <span
        className={
          "group-hover:dark:text-dark-text-accent text-base font-sans dark:text-gray-400 hover:dark:text-dark-text-accent"
        }
      >
        {title}
      </span>
    </div>
  );
};

SideBarNavTitle.propTypes = {
  title: PropTypes.string,
  iconName: PropTypes.string,
  type: PropTypes.string,
};

export default SideBarNavTitle;
