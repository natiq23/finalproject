import PropTypes from "prop-types";
import { IconContext } from "react-icons";
import classNames from "classnames";

const IconButton = ({
  icon,
  title,
  disabled = false,
  click,
  verticalMode,
  type = "button",
  size = 16,
  smallPadding = false,
  ...props
}) => {
  return (
    <button
      {...props}
      type={type}
      disabled={disabled}
      onClick={(e) => {
        e.stopPropagation();
        if (click) {
          click();
        }
      }}
      className={classNames({
        "w-fit  rounded-md hover:bg-hover-bg-clr transition-all duration-70 disabled:cursor-not-allowed disabled:opacity-70 hover:dark:bg-dark-hover-blue-clr hover:dark:text-dark-text-accent dark:text-gray-300 flex items-center gap-2": true,
        "flex-col items-center": verticalMode,
        "py-2 px-3": !smallPadding,
        "py-1 px-1": smallPadding,
      })}
    >
      <IconContext.Provider value={{ size }}>{icon}</IconContext.Provider>
      {title && <span className={"text-sm dark:text-gray-300 "}>{title}</span>}
    </button>
  );
};

IconButton.propTypes = {
  icon: PropTypes.element,
  click: PropTypes.func,
  disabled: PropTypes.bool,
  type: PropTypes.string,
  title: PropTypes.string,
  size: PropTypes.number,
  verticalMode: PropTypes.bool,
  smallPadding: PropTypes.bool,
};
export default IconButton;
