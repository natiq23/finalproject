import PropTypes from "prop-types";
import { IconContext } from "react-icons";

const PrimaryAccentButton = ({
  disabled = false,
  title,
  click,
  icon,
  type = "button",
}) => {
  return (
    <button
      type={type}
      disabled={disabled}
      onClick={(e) => {
        e.stopPropagation();
        if (click) {
          click();
        }
      }}
      className={
        "w-full text-sm flex items-center justify-center gap-2  font-semibold text-gray-200  py-2 px-4 rounded-md bg-accent hover:bg-accent-dark transition-all duration-70 disabled:cursor-not-allowed disabled:opacity-70 "
      }
    >
      {icon && (
        <IconContext.Provider value={{ size: "16px" }}>
          {icon}
        </IconContext.Provider>
      )}
      {title}
    </button>
  );
};

PrimaryAccentButton.propTypes = {
  title: PropTypes.string,
  click: PropTypes.func,
  disabled: PropTypes.bool,
  type: PropTypes.string,
  icon: PropTypes.element,
};

export default PrimaryAccentButton;
