import PropTypes from "prop-types";
import classNames from "classnames";
import { Tab } from "@headlessui/react";

const TabHead = ({ key, label }) => {
  return (
    <Tab
      key={key}
      className={({ selected }) =>
        classNames(
          "w-full rounded-lg py-2.5 text-sm font-medium leading-5 text-accent bg-body-bg dark:bg-dark-body-bg dark:text-gray-300 outline-none smooth-animation",
          selected
            ? "bg-component-bg font-semibold text-accent dark:text-[#4a86e1] dark:bg-dark-component-second-bg "
            : "text-black hover:bg-hover-blue-clr hover:dark:bg-dark-hover-blue-clr hover:text-accent hover:dark:text-accent"
        )
      }
    >
      {label}
    </Tab>
  );
};

TabHead.propTypes = {
  key: PropTypes.string,
  label: PropTypes.string,
};

export default TabHead;
