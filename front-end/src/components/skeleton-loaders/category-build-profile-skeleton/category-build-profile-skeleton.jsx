import ContentLoader from "react-content-loader";

const CategoryBuildProfileSkeleton = () => {
  return (
    <div>
      <ContentLoader
        speed={2}
        width={220}
        height={70}
        viewBox="0 0 220 70"
        backgroundColor="#a3a3a3"
        foregroundColor="#ecebeb"
      >
        <rect x="49" y="30" rx="3" ry="3" width="100" height="6" />
        <rect x="49" y="48" rx="3" ry="3" width="60" height="6" />
        <circle cx="170" cy="42" r="10" />
      </ContentLoader>
    </div>
  );
};

export default CategoryBuildProfileSkeleton;
