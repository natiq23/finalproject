import ContentLoader from "react-content-loader";

const QuestionSkeleton = () => {
  return (
    <div>
      <ContentLoader
        speed={2}
        width={1020}
        height={190}
        viewBox="0 0 1020 190"
        backgroundColor="#575757"
        foregroundColor="#ecebeb"
      >
        <rect x="6" y="150" rx="5" ry="5" width="65" height="30" />
        <rect x="2" y="28" rx="5" ry="5" width="1010" height="51" />
        <rect x="2" y="91" rx="5" ry="5" width="1010" height="49" />
        <rect x="84" y="150" rx="5" ry="5" width="65" height="30" />
        <rect x="164" y="150" rx="5" ry="5" width="65" height="30" />
        <rect x="241" y="150" rx="5" ry="5" width="65" height="30" />
      </ContentLoader>
    </div>
  );
};

export default QuestionSkeleton;
