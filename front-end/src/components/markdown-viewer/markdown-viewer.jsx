import MarkdownPreview from "@uiw/react-markdown-preview";
import PropTypes from "prop-types";
import rehypeSanitize from "rehype-sanitize";
import MermaidCodeParser from "../common/mermaid-code-parser/index.js";

const MarkdownViewer = ({ source }) => {
  return (
    <div className={""}>
      <MarkdownPreview
        source={source}
        className={
          "!w-full !overflow-clip !bg-transparent !border-none !text-xl "
        }
        rehypeRewrite={(node, index, parent) => {
          if (
            node.tagName === "a" &&
            parent &&
            /^h(1|2|3|4|5|6)/.test(parent.tagName)
          ) {
            parent.children = parent.children.slice(1);
          }
        }}
        rehypePlugins={[rehypeSanitize]}
        components={{
          code: MermaidCodeParser,
        }}
      />
    </div>
  );
};

MarkdownViewer.propTypes = {
  source: PropTypes.string,
};

export default MarkdownViewer;
