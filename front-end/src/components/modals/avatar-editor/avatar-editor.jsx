import Avatar from "react-avatar-edit";
import { useState } from "react";
import PropTypes from "prop-types";

const AvatarEditor = ({ src, onClose }) => {
  const [preview, setPreview] = useState(null);
  const handleCrop = (preview) => {
    setPreview(preview);
  };

  const handleClose = () => {
    onClose();
    setPreview(null);
  };
  return (
    <div className={"w-full flex gap-16"}>
      <div className={"flex flex-col items-center justify-between gap-4"}>
        <span className={"font-semibold text-lg dark:text-gray-300"}>
          Edit Your Avatar
        </span>
        <Avatar
          cropRadius={75}
          minCropRadius={40}
          shadingColor={"#ebecfc"}
          onCrop={handleCrop}
          onClose={handleClose}
          src={src}
        />
      </div>
      {preview && (
        <div className={"flex flex-col items-center justify-between gap-4 "}>
          <span className={"font-semibold text-lg dark:text-gray-300 "}>
            Preview
          </span>

          <div className={"h-full w-full flex items-center"}>
            <img
              src={preview}
              className={""}
              style={{ width: "150px", height: "150px" }}
              alt="Preview"
            />
          </div>
        </div>
      )}
    </div>
  );
};

AvatarEditor.propTypes = {
  src: PropTypes.string,
  onClose: PropTypes.func,
};

export default AvatarEditor;
