import PropTypes from "prop-types";
import { ErrorMessage, useField } from "formik";
import classNames from "classnames";
import { IconContext } from "react-icons";

const TextInput = ({
  title,
  autoComplete,
  placeholder,
  isRequired,
  maxLength,
  paddingLeft,
  icon,
  bigTitle = false,
  ...props
}) => {
  const [field] = useField(props);

  return (
    <div className={"w-full relative"}>
      <IconContext.Provider
        value={{ color: "#717171", className: "text-input-icon", size: "22" }}
      >
        {icon}
      </IconContext.Provider>
      <label className={"w-full h-fit flex flex-col justify-between gap-2"}>
        <div className={"flex gap-3 items-center"}>
          <span
            className={classNames({
              " font-medium dark:text-gray-400 select-none": true,
              "text-sm": !bigTitle,
              "font-semibold": bigTitle,
            })}
          >
            {title}
          </span>
          {isRequired && <span className={"font-bold text-red-900"}>*</span>}
          <ErrorMessage
            name={field.name}
            component={"small"}
            className={"text-xs block text-red-700 dark:text-red-500"}
          />
        </div>
        <div>
          <input
            {...field}
            {...props}
            autoComplete={autoComplete}
            placeholder={placeholder}
            maxLength={maxLength}
            className={"text-input"}
            type={"peer text"}
            style={{
              paddingLeft: icon
                ? paddingLeft
                  ? paddingLeft
                  : "35px"
                : paddingLeft,
            }}
          />
        </div>
      </label>
    </div>
  );
};

TextInput.propTypes = {
  title: PropTypes.string,
  placeholder: PropTypes.string,
  autoComplete: PropTypes.string,
  isRequired: PropTypes.bool,
  maxLength: PropTypes.number,
  paddingLeft: PropTypes.string,
  bigTitle: PropTypes.bool,
  icon: PropTypes.element,
};

export default TextInput;
