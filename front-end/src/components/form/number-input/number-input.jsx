import { ErrorMessage, useField } from "formik";
import { IconContext } from "react-icons";
import PropTypes from "prop-types";
import classNames from "classnames";
import IconButton from "../../buttons/icon-button/index.js";
import { BsCaretDownSquare, BsCaretUpSquare } from "react-icons/bs";

const NumberInput = ({
  title,
  placeholder,
  bigTitle,
  name,
  min = 0,
  icon,
  isRequired,
  step = 1,
  paddingLeft,
  max,
  ...props
}) => {
  const [field] = useField(name);
  return (
    <div className={"w-full flex relative"}>
      <IconContext.Provider
        value={{ color: "#717171", className: "text-input-icon", size: "22" }}
      >
        {icon}
      </IconContext.Provider>
      <label className={"w-full h-fit flex flex-col justify-between gap-2"}>
        <div className={"flex gap-3 items-center"}>
          <span
            className={classNames({
              " font-medium dark:text-gray-400 select-none": true,
              "text-sm": !bigTitle,
              "font-semibold": bigTitle,
            })}
          >
            {title}
          </span>
          {isRequired && <span className={"font-bold text-red-900"}>*</span>}
          <ErrorMessage
            name={field.name}
            component={"small"}
            className={"text-xs block text-red-700 dark:text-red-500"}
          />
        </div>
        <div>
          <input
            {...field}
            {...props}
            placeholder={placeholder}
            pattern={"[0-9]*"}
            max={max ?? null}
            className={"text-input !h-14"}
            type={"number"}
            min={min}
            step={step}
            style={{
              paddingLeft: icon
                ? paddingLeft
                  ? paddingLeft
                  : "35px"
                : paddingLeft,
            }}
          />
        </div>
        <div
          className={
            "flex flex-col justify-center absolute bottom-[4px] right-2 "
          }
        >
          <IconButton
            icon={<BsCaretUpSquare />}
            smallPadding={true}
            size={15}
            click={() => {
              const newValue = field.value + 1;
              let updatedValue = newValue;

              if (max && newValue > max) {
                updatedValue = field.value;
              }

              field.onChange({
                target: {
                  name: name,
                  value: updatedValue,
                },
              });
            }}
          />
          <IconButton
            icon={<BsCaretDownSquare />}
            smallPadding={true}
            size={15}
            click={() => {
              const newValue = field.value - 1;
              if (newValue >= min)
                field.onChange({
                  target: { name: name, value: newValue },
                });
            }}
          />
        </div>
      </label>
    </div>
  );
};

NumberInput.propTypes = {
  title: PropTypes.string,
  placeholder: PropTypes.string,
  bigTitle: PropTypes.bool,
  name: PropTypes.string,
  min: PropTypes.number,
  icon: PropTypes.element,
  isRequired: PropTypes.bool,
  step: PropTypes.number,
  paddingLeft: PropTypes.string,
  max: PropTypes.number,
};

export default NumberInput;
