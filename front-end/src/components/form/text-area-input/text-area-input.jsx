import PropTypes from "prop-types";
import { ErrorMessage, useField } from "formik";
import classNames from "classnames";

const TextAreaInput = ({
  title,
  placeholder,
  hint,
  maxLength,
  bigTitle = false,
  ...props
}) => {
  const [field] = useField(props);

  return (
    <label className={"w-full h-fit flex flex-col justify-between gap-2 "}>
      <div className={"flex gap-3 items-center"}>
        <span
          className={classNames({
            " font-medium dark:text-gray-400 select-none": true,
            "text-sm ": !bigTitle,
            "font-semibold": bigTitle,
          })}
        >
          {title}
        </span>
        <ErrorMessage
          name={field.name}
          component={"small"}
          className={"text-xs block text-red-700"}
        />
      </div>
      <div className={"flex flex-col gap-1"}>
        <span className={"text-sm text-dark-text dark:text-gray-500"}>
          {hint}
        </span>
        <textarea
          {...field}
          placeholder={placeholder}
          maxLength={maxLength}
          className={
            "w-full min-h-[97px]  p-2.5 bg-component-bg text-secondary border-[1px]  border-tz-border  focus:border-tz-border-hover focus:outline-none focus:ring-0  rounded-lg dark:bg-dark-component-bg dark:text-gray-300 dark:border-dark-border-clr"
          }
        />
        {maxLength && (
          <span
            className={"text-sm text-dark-text self-end dark:text-gray-400"}
          >
            {field.value.length}/{maxLength}
          </span>
        )}
      </div>
    </label>
  );
};

TextAreaInput.propTypes = {
  title: PropTypes.string,
  hint: PropTypes.string,
  maxLength: PropTypes.number,
  placeholder: PropTypes.string,
  bigTitle: PropTypes.bool,
};
export default TextAreaInput;
