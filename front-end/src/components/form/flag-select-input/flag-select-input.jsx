import { useEffect, useState } from "react";
import SelectInput from "../select-input/index.js";
import PropTypes from "prop-types";
import { getAllTags } from "../../../services/tag.js";

const FlagSelectInput = ({ name, type }) => {
  const [flags, setFlags] = useState([]);
  const [flagsIsLoading, setFlagsIsLoading] = useState(false);

  useEffect(() => {
    setFlagsIsLoading(true);
    getAllTags(type === "resource" ? "resource-flags" : "article-flags")
      .then((res) => {
        const categories = res.map((category) => {
          return {
            label: category.title,
            value: category.id,
          };
        });
        setFlags(categories);
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        setFlagsIsLoading(false);
      });
  }, []);

  return (
    <div>
      <SelectInput
        placeholder={"Select a flag"}
        name={name}
        isLoading={flagsIsLoading}
        data={flags}
      />
    </div>
  );
};

FlagSelectInput.propTypes = {
  name: PropTypes.string,
  type: PropTypes.string,
};

export default FlagSelectInput;
