import PropTypes from "prop-types";
import { useField } from "formik";
import { HexColorInput, HexColorPicker } from "react-colorful";
import { useUser } from "../../../utils/user.js";
import { useState } from "react";
import classNames from "classnames";
import { useAutoAnimate } from "@formkit/auto-animate/react";

const ColorInput = ({ name, title, bigTitle, isProfile }) => {
  const { animationParent } = useAutoAnimate();

  const [field] = useField(name);
  const user = useUser();

  const [colorInputIsVisible, setColorInputIsVisible] = useState(false);

  const handleOnChange = (newColor) => {
    field.onChange({ target: { name: name, value: newColor } });
  };

  return (
    <div className={"flex flex-col gap-2"} ref={animationParent}>
      <span
        className={classNames({
          " font-medium dark:text-gray-400 select-none": true,
          "text-sm": !bigTitle,
          "font-semibold": bigTitle,
        })}
      >
        {title}
      </span>
      {isProfile ? (
        <>
          <HexColorInput
            className={
              "peer w-full bg-white dark:bg-dark-component-bg dark:border-dark-border-clr dark:text-gray-300 h-9 border border-border-clr rounded-md py-2 pl-3  focus:outline-none hover:border-hover-border-clr focus:outline-accent focus:border-none transition-all duration-75"
            }
            prefixed={true}
            color={field.value}
            onChange={handleOnChange}
          />
          <div className={"flex gap-6 "}>
            <HexColorPicker
              className={"w-52"}
              color={field.value}
              onChange={handleOnChange}
            />
            <div className={"w-[520px] h-[200px]"}>
              <div
                className={"rounded-md !rounded-b-none w-full h-16"}
                style={{ backgroundColor: field.value }}
              ></div>
              <div
                className={
                  "relative top-[-60px] flex flex-col gap-3 items-center"
                }
              >
                <div
                  className={
                    "w-full flex flex-col items-center justify-center gap-3"
                  }
                >
                  <img
                    src={user.avatar}
                    alt="avatar"
                    className={
                      "mt-1 z-20 w-16 h-16 rounded-full  border-[10px] box-content "
                    }
                    style={{
                      borderColor: field.value,
                    }}
                  />
                  <div
                    className={
                      "relative w-full top-[-40px] mb-[-90px] p-8 !rounded-t-none !rounded-b-none  rounded-y-md z-10 flex flex-col items-center justify-end gap-3 border-t-[1px] border-x-[1px] card-color-schema"
                    }
                  >
                    <span
                      className={"text-xl font-semibold dark:text-gray-300"}
                    >
                      {user.name}
                    </span>
                    <span
                      className={"text-lg font-semibold dark:text-gray-300"}
                    >
                      A simple preview
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      ) : (
        <div className={"flex flex-col gap-5"} ref={animationParent}>
          <div className={"flex items-center gap-3"}>
            <button
              type={"button"}
              className={
                "w-9 h-7 rounded-md dark:border-[1px] dark:border-dark-border-clr"
              }
              onClick={() => {
                setColorInputIsVisible(!colorInputIsVisible);
              }}
              style={{ backgroundColor: field.value }}
            ></button>
            <HexColorInput
              className={
                "peer w-full bg-white dark:bg-dark-component-bg dark:border-dark-border-clr dark:text-gray-300 h-9 border border-border-clr rounded-md py-2 pl-3  focus:outline-none hover:border-hover-border-clr focus:outline-accent focus:border-none transition-all duration-75"
              }
              prefixed={true}
              color={field.value}
              onChange={handleOnChange}
            />
          </div>
          <div
            className={classNames({
              "visible animate-slideUpAndFade": colorInputIsVisible,
              "hidden animate-slideDownAndFade": !colorInputIsVisible,
            })}
            ref={animationParent}
          >
            <HexColorPicker
              className={"w-52"}
              color={field.value}
              onChange={handleOnChange}
            />
          </div>
        </div>
      )}
    </div>
  );
};

ColorInput.propTypes = {
  name: PropTypes.string,
  title: PropTypes.string,
  bigTitle: PropTypes.bool,
  isProfile: PropTypes.bool,
};

export default ColorInput;
