import * as HoverCard from "@radix-ui/react-hover-card";
import PropTypes from "prop-types";
import { useEffect, useState } from "react";
import { getPreviewUserById } from "../../../services/user.js";
import UserPreviewCard from "../user-preview-card/index.js";

const UserPreviewHoverCard = ({ userId, children }) => {
  const [user, setUser] = useState(null);

  const childrenType = children?._owner?.child?.elementType;

  useEffect(() => {
    getPreviewUserById(userId)
      .then((res) => {
        setUser(res);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  return user ? (
    <HoverCard.Root modal={false}>
      <HoverCard.Trigger asChild>
        {childrenType === "button" ? (
          <>{children}</>
        ) : (
          <button>{children}</button>
        )}
      </HoverCard.Trigger>
      <HoverCard.Portal>
        <HoverCard.Content
          className="data-[side=bottom]:animate-slideUpAndFade data-[side=right]:animate-slideLeftAndFade data-[side=left]:animate-slideRightAndFade data-[side=top]:animate-slideDownAndFade data-[state=open]:transition-all"
          sideOffset={5}
        >
          <UserPreviewCard user={user} isHoverCard={true} />

          <HoverCard.Arrow className="fill-white dark:fill-dark-border-clr" />
        </HoverCard.Content>
      </HoverCard.Portal>
    </HoverCard.Root>
  ) : childrenType === "button" ? (
    <>{children}</>
  ) : (
    <button>{children}</button>
  );
};

UserPreviewHoverCard.propTypes = {
  userId: PropTypes.string,
  children: PropTypes.element,
};

export default UserPreviewHoverCard;
