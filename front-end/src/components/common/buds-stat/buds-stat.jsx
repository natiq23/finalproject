import { useUserSignedIn } from "../../../utils/auth.js";
import TooltipContainer from "../tooltip-container/index.js";
import { useEffect, useState } from "react";
import { getBuds } from "../../../services/buds.js";
import CountUp from "react-countup";
import PropTypes from "prop-types";
import classNames from "classnames";
import { ThreeDots } from "react-loader-spinner";

const BudsStat = ({ isTransparent }) => {
  const signedIn = useUserSignedIn();

  const budsSessionStorageName = "local-buds-count";

  const [loading, setLoading] = useState(false);
  const [isNew, setIsNew] = useState(null);

  const cacheCount = sessionStorage.getItem(budsSessionStorageName);
  useEffect(() => {
    setLoading(cacheCount === null);
    getBuds()
      .then((res) => {
        if (cacheCount === null) {
          setIsNew({
            start: 0,
            end: res,
          });
        } else if (+cacheCount !== +res) {
          setIsNew({
            start: cacheCount,
            end: res,
          });
        }

        sessionStorage.setItem(budsSessionStorageName, res);
      })
      .finally(() => {
        setLoading(false);
      });
  }, []);

  if (!signedIn) return null;

  return (
    <div
      className={classNames({
        "w-fit card-color-schema dark:text-white py-1 px-4 rounded-md flex items-center justify-center  gap-6": true,
        "!bg-transparent !border-none": isTransparent,
      })}
    >
      <TooltipContainer tooltip={"Buds count"}>
        <div className={"flex items-center gap-2 font-semibold"}>
          <img src={`/bud.png`} alt={"bud icon"} className={"h-5"} />
          {loading ? (
            <div className={"w-full flex items-center justify-center"}>
              <ThreeDots
                height="24"
                width="24"
                color={"#3B49DF"}
                wrapperClass="radio-wrapper"
                radius="9"
                ariaLabel="three-dots-loading"
                wrapperStyle={{}}
                wrapperClassName=""
                visible={true}
              />
            </div>
          ) : (
            <span className={"text-lg"}>
              {isNew ? (
                <CountUp start={isNew.start} end={isNew.end} separator={""} />
              ) : (
                cacheCount
              )}
            </span>
          )}
        </div>
      </TooltipContainer>
    </div>
  );
};

BudsStat.propTypes = {
  isTransparent: PropTypes.bool,
};

export default BudsStat;
