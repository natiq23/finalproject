import PropTypes from "prop-types";

const UserPreviewHoverCardInfo = ({ label, value, type = "general" }) => {
  return (
    value && (
      <div className={"flex flex-col gap-1 items-start justify-end"}>
        <span
          className={
            "font-semibold text-second-text uppercase text-sm dark:text-gray-600"
          }
        >
          {label}
        </span>
        {type === "email" ? (
          <a
            href={`mailto:${value}`}
            className={" text-accent underline-animation dark:text-gray-300"}
          >
            {value}
          </a>
        ) : (
          <p className={"dark:text-gray-300"}>{value}</p>
        )}
      </div>
    )
  );
};

UserPreviewHoverCardInfo.propTypes = {
  label: PropTypes.string,
  value: PropTypes.string,
  type: PropTypes.string,
};

export default UserPreviewHoverCardInfo;
