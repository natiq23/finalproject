const Divider = () => {
  return (
    <div className={"w-full h-px bg-gray-300 dark:bg-gray-700 rounded-md"} />
  );
};

export default Divider;
