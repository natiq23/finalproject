import SearchOption from "../search-option/index.js";
import { useEffect } from "react";
import PropTypes from "prop-types";

const PageSearchBar = ({
  placeholder = "Search ...",
  onSearch,
  setSearchParam,
  getSearchParam,
  searchOnSearchQueryChange = false,
  options,
}) => {
  const handleSearchOptionChange = (value, selectParamName) => {
    setSearchParam((prev) => {
      prev.set(selectParamName, value);
      return prev;
    });
    onSearch();
  };

  useEffect(() => {
    if (getSearchParam("q")) onSearch();
  }, []);

  return (
    <div className={"z-50"}>
      <div className={"w-full h-9 z-50"}>
        <label
          htmlFor=""
          className={
            "w-full flex justify-center items-center  card-color-schema card-shadow-schema"
          }
        >
          <input
            className={
              "w-full  h-9 bg-transparent dark:text-gray-300 rounded-l-md placeholder-gray-700 py-2 px-3 focus:outline-none"
            }
            type="text"
            value={getSearchParam("q")}
            onKeyDown={(e) => {
              if (e.key === "Enter") {
                onSearch();
              }
            }}
            placeholder={placeholder}
            onChange={(e) => {
              setSearchParam((prev) => {
                prev.set("q", e.target.value);
                return prev;
              });
              if (searchOnSearchQueryChange) onSearch();
            }}
          />
          <div
            className={"w-fit flex items-center justify-end gap-1 rounded-md"}
          >
            {options &&
              options.map((option) => (
                <SearchOption
                  getSearchParam={getSearchParam}
                  key={option.paramName}
                  option={option}
                  onChange={handleSearchOptionChange}
                />
              ))}
          </div>

          <div className={""}></div>
        </label>
      </div>
    </div>
  );
};

PageSearchBar.propTypes = {
  getSearchParam: PropTypes.func,
  options: PropTypes.array,
  searchOnSearchQueryChange: PropTypes.bool,
  placeholder: PropTypes.string,
  defaultSearchValue: PropTypes.string,
  onSearch: PropTypes.func,
  setSearchParam: PropTypes.func,
};

export default PageSearchBar;
