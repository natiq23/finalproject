import { Badge } from "@material-tailwind/react";
import IconAccentButton from "../../buttons/icon-accent-button/index.js";
import { RiNotification3Line } from "react-icons/ri";
import { useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import { getUnreadNotificationsCount } from "../../../services/notifications.js";
import classNames from "classnames";

const NotificationsButton = () => {
  const [count, setCount] = useState(0);

  const navigate = useNavigate();

  useEffect(() => {
    getUnreadNotificationsCount()
      .then((res) => {
        setCount(res);
      })
      .catch((error) => {
        console.log(error);
      });
  });

  return (
    <div
      className={classNames({
        "w-fit rounded-md": true,
        "dark:bg-dark-hover-blue-clr bg-hover-blue-clr":
          window.location.pathname === "/notifications",
      })}
    >
      <Badge
        content={count}
        invisible={count <= 0}
        className={
          "bg-accent p-0 min-w-[20px] min-h-[20px] top-[20%] right-[10%]"
        }
      >
        <IconAccentButton
          icon={<RiNotification3Line />}
          click={() => {
            navigate("/notifications");
          }}
        />
      </Badge>
    </div>
  );
};

export default NotificationsButton;
