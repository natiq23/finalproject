import DropDownMenu from "../drop-down-menu/index.js";
import { RiMoreLine } from "react-icons/ri";
import PropTypes from "prop-types";

const MoreOptions = ({ items }) => {
  return (
    <div>
      <DropDownMenu
        trigger={<RiMoreLine size={22} className={"dark:text-gray-300"} />}
        items={items}
      />
    </div>
  );
};

MoreOptions.propTypes = {
  items: PropTypes.arrayOf(PropTypes.object),
};

export default MoreOptions;
