import DropDownMenu from "../drop-down-menu/index.js";
import * as DropdownMenu from "@radix-ui/react-dropdown-menu";
import { useUser } from "../../../utils/user.js";
import { createModal } from "../../../utils/modal.js";
import { Link, useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import { useAuthData } from "../../../utils/auth.js";
import BudsStat from "../buds-stat/index.js";
import MediaQuery, { useMediaQuery } from "react-responsive";

const ProfileOptions = () => {
  const { avatar, username, name } = useUser();
  const navigate = useNavigate();
  const [dropdownItems, setDropdownItems] = useState([]);
  const { role } = useAuthData();

  const isButtonSize = useMediaQuery({ query: "(max-width: 960px)" });

  useEffect(() => {
    let options = [
      {
        label: "Share Resource",
        renderItem: (
          <DropdownMenu.Item
            className={
              "dd-item group  hover:bg-hover-blue-clr hover:dark:bg-dark-hover-blue-clr hover:outline-none hover:border-none hover:text-accent"
            }
            onClick={() => {
              navigate(`/${username}`);
            }}
          >
            <Link
              to={`/${username}`}
              className={
                "w-full h-full  flex flex-col gap-1  relative  select-none outline-none cursor-pointer rounded-md whitespace-pre     leading-5 hover:dark:text-dark-text-accent "
              }
            >
              <span
                className={
                  "font-semibold text-second-text dark:text-gray-300 w-fit hover:dark:text-dark-text-accent hover:text-accent group-hover:text-accent group-hover:dark:text-dark-text-accent"
                }
              >
                {name}
              </span>
              <span
                className={
                  "text-sm text-dark-text dark:text-gray-500 group-hover:text-accent group-hover:dark:text-dark-text-accent   w-fit"
                }
              >
                @{username}
              </span>
            </Link>
          </DropdownMenu.Item>
        ),
        type: "item",
      },
      {
        type: "separator",
      },

      {
        label: "Dashboard",
        onClick: () => {
          navigate("/dashboard");
        },
        type: "item",
      },
    ];

    if (isButtonSize)
      options.push({
        label: "Create Article",
        onClick: () => {
          navigate("/publish/article");
        },
        type: "item",
      });

    if (role === "Moderator" || role === "Admin")
      options.push({
        label: "Moderator Dashboard",
        onClick: () => {
          navigate("/manage/moderator");
        },
        type: "item",
      });

    options.push(
      {
        label: "Settings",
        onClick: () => {
          navigate("/settings");
        },
        type: "item",
      },
      {
        type: "separator",
      },
      {
        label: "Sign Out",
        onClick: () => {
          createModal("signOut");
        },
        type: "danger-item",
      }
    );

    setDropdownItems(options);
  }, [isButtonSize]);

  return (
    <div className={"  "}>
      <DropDownMenu
        trigger={
          <div
            className={
              "bg-body-bg dark:bg-dark-body-bg rounded-md flex items-center"
            }
          >
            <MediaQuery minWidth={325}>
              <BudsStat isTransparent={true} />
            </MediaQuery>
            <img
              className={"w-10 h-10 rounded-md object-cover"}
              src={avatar}
              alt="avatar"
            />
          </div>
        }
        items={dropdownItems}
      />
    </div>
  );
};

export default ProfileOptions;
