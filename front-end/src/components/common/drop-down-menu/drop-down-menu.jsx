import * as DropdownMenu from "@radix-ui/react-dropdown-menu";
import { nanoid } from "nanoid";
import PropTypes from "prop-types";
import { IconContext } from "react-icons";
import "./drop-down-menu.css";
import classNames from "classnames";

const DropDownMenu = ({ trigger, items }) => {
  // {
  //     label: 'Exit',
  //     icon: <ExitIcon />,
  //     onClick: () => console.log('Logout'),
  //     type:"item | separator | danger-item | green-item"
  // },

  const triggerType = trigger.type;

  return (
    <DropdownMenu.Root modal={false}>
      <DropdownMenu.Trigger
        className={classNames({
          "hover:bg-border-clr hover:dark:bg-dark-hover-bg-clr p-1  border-none focus:outline-none": true,
          "rounded-full": triggerType === "img",
          "rounded-md": triggerType !== "img",
        })}
        asChild
      >
        {triggerType === "button" ? <>{trigger}</> : <button>{trigger}</button>}
      </DropdownMenu.Trigger>

      <DropdownMenu.Portal>
        <DropdownMenu.Content className="dd-content" sideOffset={5}>
          {items.map((item) => {
            switch (item.type) {
              case "item":
                return item.renderItem ? (
                  <div key={nanoid()}>{item.renderItem}</div>
                ) : (
                  <div key={nanoid()}>
                    <DropdownMenu.Item
                      className="dd-item"
                      onClick={item.onClick}
                    >
                      <IconContext.Provider
                        value={{
                          size: "24",
                        }}
                      >
                        {item.icon}
                      </IconContext.Provider>
                      <span>{item.label}</span>
                    </DropdownMenu.Item>
                  </div>
                );
              case "separator":
                return (
                  <div key={nanoid()}>
                    <DropdownMenu.Separator className="dd-separator"></DropdownMenu.Separator>
                  </div>
                );
              case "warning-item":
                return (
                  <div key={nanoid()}>
                    <DropdownMenu.Item
                      className="dd-warning-item"
                      onClick={item.onClick}
                    >
                      <IconContext.Provider
                        value={{
                          size: "24",
                        }}
                      >
                        {item.icon}
                      </IconContext.Provider>
                      <span>{item.label}</span>
                    </DropdownMenu.Item>
                  </div>
                );
              case "danger-item":
                return (
                  <div key={nanoid()}>
                    <DropdownMenu.Item
                      className="dd-danger-item"
                      onClick={item.onClick}
                    >
                      <IconContext.Provider
                        value={{
                          size: "24",
                        }}
                      >
                        {item.icon}
                      </IconContext.Provider>
                      <span>{item.label}</span>
                    </DropdownMenu.Item>
                  </div>
                );
              case "green-item":
                return (
                  <div key={nanoid()}>
                    <DropdownMenu.Item
                      className="dd-green-item"
                      onClick={item.onClick}
                    >
                      <IconContext.Provider
                        value={{
                          size: "24",
                        }}
                      >
                        {item.icon}
                      </IconContext.Provider>
                      <span>{item.label}</span>
                    </DropdownMenu.Item>
                  </div>
                );
              default:
                return (
                  <div key={nanoid()}>
                    <DropdownMenu.Item className="dd-item"></DropdownMenu.Item>
                  </div>
                );
            }
          })}
          <DropdownMenu.Arrow className="fill-border-clr dark:fill-dark-border-clr" />
        </DropdownMenu.Content>
      </DropdownMenu.Portal>
    </DropdownMenu.Root>
  );
};

DropDownMenu.propTypes = {
  trigger: PropTypes.element,
  items: PropTypes.arrayOf(PropTypes.object),
};

export default DropDownMenu;
