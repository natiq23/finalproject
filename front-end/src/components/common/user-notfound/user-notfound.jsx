import PropTypes from "prop-types";
import "./user-notfound.css";

const UserNotfound = ({ username }) => {
  return (
    <div
      className={
        "absolute top-[57px] left-0 w-full h-[calc(100vh-57px)] flex flex-col items-center justify-center bg-[#fb9e64]"
      }
    >
      <div>
        <img src={`/baby.gif`} className={"w-32"} alt="" />
      </div>
      <div className={"w-full flex flex-col gap-4 items-center font-modulus"}>
        <p className={"text-4xl font-bold  "}>Whoops!</p>
        <p className={"text-2xl font-medium "}>
          <span className={"font-modulus font-bold italic"}>{username}</span>{" "}
          wasn&apos;t born yet!
        </p>
      </div>
    </div>
  );
};

UserNotfound.propTypes = {
  username: PropTypes.string,
};

export default UserNotfound;
