import PropTypes from "prop-types";
import Select from "react-select";

const SelectComponent = ({
  placeholder,
  renderPlaceholder,
  defaultOptionValue,
  onChange,
  options,
  isLoading = false,
}) => {
  return (
    <div className={"w-fit   z-50"}>
      <Select
        className={"w-fit min-w-[170px] z-50"}
        classNamePrefix={"react-select"}
        placeholder={renderPlaceholder ? renderPlaceholder : placeholder}
        isClearable={true}
        isSearchable={true}
        isLoading={isLoading}
        defaultValue={options.find(
          (option) => option.value === defaultOptionValue
        )}
        value={options.find((option) => option.value === defaultOptionValue)}
        onChange={onChange}
        options={options}
      />
    </div>
  );
};

SelectComponent.propTypes = {
  options: PropTypes.array,
  isLoading: PropTypes.bool,
  onChange: PropTypes.func,
  defaultOptionValue: PropTypes.string,
  placeholder: PropTypes.string,
  renderPlaceholder: PropTypes.element,
};

export default SelectComponent;
