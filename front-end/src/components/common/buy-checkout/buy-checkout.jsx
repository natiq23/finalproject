import PropTypes from "prop-types";
import Divider from "../divider/index.js";
import PriceIndicator from "../price-indicator/index.js";
import BuyCheckoutInfo from "../buy-checkout-info/index.js";

const BuyCheckout = ({ buyQuantity, product }) => {
  return (
    <div className={"w-full flex flex-col gap-2"}>
      <div
        className={
          "flex flex-col items-start justify-between gap-0.5 dark:text-white mt-3"
        }
      >
        <div className={"w-full flex items-center justify-between"}>
          <h1 className={"font-bold"}>Name:</h1>
          <span className={"ellipsis-overflow"}>{product.name}</span>
        </div>
        <BuyCheckoutInfo label={"Id"} value={product.id} />
      </div>

      <Divider />

      <BuyCheckoutInfo label={"Quantity"} value={buyQuantity} />

      <BuyCheckoutInfo
        label={"Price"}
        renderValue={<PriceIndicator price={product.price} />}
      />

      <BuyCheckoutInfo label={"Discount (%)"} value={product.discount} />

      <BuyCheckoutInfo
        label={"Discounted Price"}
        renderValue={<PriceIndicator price={product.discountedPrice} />}
      />

      <Divider />

      <BuyCheckoutInfo
        label={"Total"}
        renderValue={
          <PriceIndicator price={product.discountedPrice * buyQuantity} />
        }
      />
    </div>
  );
};

BuyCheckout.propTypes = {
  buyQuantity: PropTypes.number,
  product: PropTypes.object,
};
export default BuyCheckout;
