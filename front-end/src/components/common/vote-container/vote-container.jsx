import UpVoteButton from "../../buttons/up-vote-button/index.js";
import VoteCount from "../vote-count/index.js";
import DownVoteButton from "../../buttons/down-vote-button/index.js";
import PropTypes from "prop-types";
import { useState } from "react";
import { ContentCommonService } from "../../../services";
import classNames from "classnames";

const VoteContainer = ({
  voteStatus,
  voteCount,
  contentId,
  contentType,
  contentViewMode,
  direction = "vertical",
}) => {
  const [status, setStatus] = useState(voteStatus);
  const [count, setCount] = useState(voteCount);

  const vote = (status) =>
    ContentCommonService.voteContent(contentType, status, contentId);

  const handleUpVote = () => {
    let newCount = count;
    if (status === "DownVote") {
      newCount += 1;
    }
    newCount = status === "UpVote" ? newCount - 1 : newCount + 1;
    setCount(newCount);
    setStatus(status === "UpVote" ? "UnVote" : "UpVote");
    vote("UpVote")
      .then((res) => {
        console.log(res);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const handleDownVote = () => {
    let newCount = count;
    if (status === "UpVote") {
      newCount -= 1;
    }
    newCount = status === "DownVote" ? newCount + 1 : newCount - 1;
    setCount(newCount);
    setStatus(status === "DownVote" ? "UnVote" : "DownVote");
    vote("DownVote")
      .then((res) => {
        console.log(res);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div
      className={classNames({
        "w-fit h-fit flex": true,
        "gap-2": contentViewMode,
        "flex-col  items-center justify-between": direction === "vertical",
        "flex-row  items-center": direction === "horizontal",
      })}
    >
      <UpVoteButton status={status} click={handleUpVote} />

      <VoteCount status={status} count={count} />

      <DownVoteButton status={status} click={handleDownVote} />
    </div>
  );
};

VoteContainer.propTypes = {
  voteStatus: PropTypes.string,
  voteCount: PropTypes.number,
  contentType: PropTypes.string,
  contentId: PropTypes.string,
  contentViewMode: PropTypes.bool,
};

export default VoteContainer;
