import TooltipContainer from "../tooltip-container/index.js";
import PropTypes from "prop-types";
import classNames from "classnames";

const VoteCount = ({ status, count }) => {
  return (
    <>
      {/*Vote Count*/}
      <TooltipContainer tooltip={"Vote Count"}>
        <span
          className={classNames({
            "font-semibold": true,
            "text-second-text dark:text-gray-300": status === "UnVote",
            "text-[#15ce5c]": status === "UpVote",
            "text-[#e74c3c]": status === "DownVote",
          })}
        >
          {count}
        </span>
      </TooltipContainer>
    </>
  );
};

VoteCount.propTypes = {
  status: PropTypes.string,
  count: PropTypes.number,
};

export default VoteCount;
