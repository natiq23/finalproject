import PropTypes from "prop-types";
import { IconContext } from "react-icons";
import { Progress } from "@material-tailwind/react";

const ProgressBarChart = ({ header, data, labelIcon }) => {
  const total = Object.values(data).reduce((a, b) => a + b, 0);
  console.log("total", total);
  const findPercentage = (value) => {
    return Math.floor((value / total) * 100);
  };

  return (
    <div className={"w-full"}>
      <h1 className={"font-semibold text-lg"}>{header}</h1>
      <div className={"w-full flex flex-col gap-2 items-start justify-start"}>
        {Object.entries(data).map(([key, value]) => (
          <div
            key={key}
            className={"w-full flex  items-center justify-start gap-1.5"}
          >
            <span className={"flex items-center gap-1"}>
              {key}
              <IconContext.Provider value={{ size: "16px", color: "#fde047" }}>
                {labelIcon}
              </IconContext.Provider>
            </span>
            <Progress
              aria-label=""
              value={findPercentage(value)}
              className="w-full"
              size={"md"}
              //@ts-ignore
              color={"yellow"}
            />
            <span>{value}</span>
          </div>
        ))}
      </div>
    </div>
  );
};

ProgressBarChart.propTypes = {
  header: PropTypes.string,
  data: PropTypes.object,
  labelIcon: PropTypes.element,
};

export default ProgressBarChart;
