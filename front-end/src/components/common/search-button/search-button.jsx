import classNames from "classnames";
import IconAccentButton from "../../buttons/icon-accent-button/index.js";
import { RiNotification3Line } from "react-icons/ri";
import { useNavigate } from "react-router-dom";
import { LuSearch } from "react-icons/lu";

const SearchButton = () => {
  const navigate = useNavigate();

  return (
    <div
      className={classNames({
        "w-fit rounded-md": true,
        "dark:bg-dark-hover-blue-clr bg-hover-blue-clr":
          window.location.pathname === "/search",
      })}
    >
      <IconAccentButton
        icon={<LuSearch />}
        size={20}
        click={() => {
          navigate("/search");
        }}
      />
    </div>
  );
};

export default SearchButton;
