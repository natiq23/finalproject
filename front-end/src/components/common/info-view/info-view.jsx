import PropTypes from "prop-types";
import { BsInfoSquareFill } from "react-icons/bs";
import classNames from "classnames";

const InfoView = ({ message, version }) => {
  return (
    <div
      className={classNames({
        "flex items-center gap-2 p-1 dark:text-gray-300": true,
        "rounded-md  border-[1px] border-border-clr dark:border-dark-border-clr  select-none shadow-none hover:shadow smooth-animation":
          version === "input-like",
      })}
    >
      <BsInfoSquareFill fill={"#3B49DF"} size={20} />
      <p>{message}</p>
    </div>
  );
};

InfoView.propTypes = {
  message: PropTypes.string,
  version: PropTypes.string,
};

export default InfoView;
