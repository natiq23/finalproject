import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import DotDivider from "../dot-divider/index.js";
import FollowButton from "../../buttons/follow-button/index.js";
import TooltipContainer from "../tooltip-container/index.js";
import CountUp from "react-countup";
import { useUser } from "../../../utils/user.js";
import classNames from "classnames";
import MediaQuery, { useMediaQuery } from "react-responsive";

const Leader = ({ leader }) => {
  const userId = useUser()?.id;
  const isSmall = useMediaQuery({ query: "(max-width: 520px)" });

  const isCurrentUser = userId === leader.userId;

  return (
    <div
      className={classNames({
        "full-card-schema !flex-row justify-between items-center !border-l-4 !border-l-accent ": true,
        "!border-l-[#4caf50]": isCurrentUser,
      })}
    >
      <div
        className={classNames({
          "w-full flex items-center gap-3 ": true,
          "justify-between": isSmall,
        })}
      >
        <div className={"flex items-center gap-3"}>
          <p className={"font-bold text-2xl"}>{leader.rank}</p>
          <img
            src={leader.profilePhoto}
            alt="avatar"
            className={"w-12 h-12 rounded-full object-cover"}
          />
          <Link
            to={`/${leader.username}`}
            className={classNames({
              "text-lg font-bold dark:text-gray-300 hover:text-accent hover:dark:text-dark-text-accent": true,
              "!text-[#4caf50]": isCurrentUser,
            })}
          >
            {isSmall && isCurrentUser ? "You!" : leader.name}
          </Link>
        </div>

        <MediaQuery minWidth={520}>
          <DotDivider size={18} />
        </MediaQuery>

        <MediaQuery minWidth={350}>
          <div
            className={
              "w-fit card-color-schema dark:text-white py-1 px-4 rounded-md flex items-center justify-center  gap-6"
            }
          >
            <TooltipContainer tooltip={"Buds count"}>
              <p className={"font-semibold flex items-center gap-2"}>
                <img src={`/bud.png`} alt={"bud icon"} className={"h-6"} />
                <span className={"text-lg"}>
                  <CountUp end={leader.budsPoints} separator={""} />
                </span>
              </p>
            </TooltipContainer>
          </div>
        </MediaQuery>

        {!isSmall && isCurrentUser && (
          <div className={"flex items-center gap-2"}>
            <DotDivider size={18} />
            <span className={"text-lg font-bold text-[#4caf50]"}>You!</span>
          </div>
        )}
      </div>

      <MediaQuery minWidth={520}>
        <FollowButton targetId={leader.userId} />
      </MediaQuery>
    </div>
  );
};

Leader.propTypes = {
  leader: PropTypes.object,
};
export default Leader;
