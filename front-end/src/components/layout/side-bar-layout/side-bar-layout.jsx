import Header from "../header/index.js";
import Container from "../../common/container/index.js";
import { Outlet } from "react-router-dom";
import LoginCard from "../../pages/enter-page/login-card/index.js";
import SideBar from "../../side-bar/side-bar/index.js";
import { useUserSignedIn } from "../../../utils/auth.js";
import MediaQuery from "react-responsive";
import DarkModeBillboard from "../../billboards/dark-mode-billboard/index.js";

const SideBarLayout = () => {
  document.title = "Clubrick";

  const signedIn = useUserSignedIn();

  return (
    <div>
      <Header />
      <Container>
        <div className={"flex gap-4"}>
          <MediaQuery minWidth={770}>
            <div className={"w-60 py-4 flex flex-col gap-4"}>
              {!signedIn && <LoginCard />}
              <SideBar />
              <DarkModeBillboard />
            </div>
          </MediaQuery>
          <Outlet />
        </div>
      </Container>
    </div>
  );
};

export default SideBarLayout;
