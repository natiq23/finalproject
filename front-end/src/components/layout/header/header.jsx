import Container from "../../common/container/index.js";
import Brand from "../../common/brand/index.js";
import Search from "../../common/search/index.js";
import LoginButton from "../../pages/enter-page/login-button/index.js";
import SignupButton from "../../pages/enter-page/signup-button/index.js";
import { useUserSignedIn } from "../../../utils/auth.js";
import { useEffect, useState } from "react";
import ProfileOptions from "../../common/profile-options/index.js";
import AccentBorderButton from "../../buttons/accent-border-button/index.js";
import "./header.css";
import {
  Link,
  useLocation,
  useNavigate,
  useSearchParams,
} from "react-router-dom";
import NotificationsButton from "../../common/notifications-button/index.js";
import MediaQuery from "react-responsive";
import IconButton from "../../buttons/icon-button/index.js";
import SearchButton from "../../common/search-button/index.js";
import { RxHamburgerMenu } from "react-icons/rx";
import { toggleDrawerProcess } from "../../../utils/process.js";

const Header = () => {
  const location = useLocation();
  const pathName = location.pathname;

  const [searchParams, setSearchParams] = useSearchParams();
  const [query, setQuery] = useState(
    pathName === "/search" ? searchParams.get("q") : searchParams.get("search")
  );

  const signedIn = useUserSignedIn();
  const navigate = useNavigate();

  useEffect(() => {}, [signedIn]);

  const handleChangeSearchHeader = (searchValue) => {
    setQuery(searchValue);
  };

  const handleSearchHeader = () => {
    if (query.length < 0) return;
    if (pathName === "/search") {
      setSearchParams((prev) => {
        prev.set("q", query);
        return prev;
      });
    } else {
      navigate(`/search?q=${query}`);
    }
  };

  return (
    <div
      className={
        "w-full h-14  bg-white dark:bg-dark-component-bg drop-shadow-sm sticky top-[-1px] z-[60]"
      }
    >
      <Container>
        <div className={"w-full h-14 flex items-center "}>
          <div className={"w-fit flex items-center gap-2 "}>
            <MediaQuery maxWidth={770}>
              <IconButton
                icon={<RxHamburgerMenu />}
                size={20}
                click={toggleDrawerProcess}
              />
            </MediaQuery>
            <Brand />
            <MediaQuery minWidth={820}>
              <div className={"w-[430px]"}>
                <Search
                  placeHolder={"Search..."}
                  searchValue={query}
                  onChange={handleChangeSearchHeader}
                  onSearch={handleSearchHeader}
                  searchOnSearchQueryChange={false}
                />
              </div>
            </MediaQuery>
          </div>
          <div className={"w-full  flex items-center justify-end"}>
            {signedIn ? (
              <div className={"w-full flex gap-[2%]  items-center justify-end"}>
                <MediaQuery maxWidth={820}>
                  <SearchButton />
                </MediaQuery>
                <MediaQuery minWidth={960}>
                  <div className={"w-32"}>
                    <Link to={"/publish/article"}>
                      <AccentBorderButton title={"Create Article"} />
                    </Link>
                  </div>
                </MediaQuery>
                <NotificationsButton />
                <ProfileOptions />
              </div>
            ) : (
              <div className={"flex gap-2"}>
                <div className={"w-20"}>
                  <LoginButton />
                </div>
                <div className={"w-36"}>
                  <SignupButton />
                </div>
              </div>
            )}
          </div>
        </div>
      </Container>
    </div>
  );
};

export default Header;
