import store from "../store";
import { useSelector } from "react-redux";
import {
  startLoading,
  finishReportAbuse,
  finishVerification,
  saveCategories,
  saveFormValues,
  savePublishResourceFormValues,
  startEmailVerification,
  startReportAbuse,
  verifyEmail,
  stopLoading,
  saveProviderAvatar,
  saveFirstAvatar,
  toggleDrawer,
  finishDrawer,
} from "../store/process.js";

export const useLoading = () => useSelector((state) => state.process.loading);

export const useDrawer = () => useSelector((state) => state.process.drawer);

export const toggleDrawerProcess = () => store.dispatch(toggleDrawer());
export const finishDrawerProcess = () => store.dispatch(finishDrawer());

export const startLoadingProcess = (label) =>
  store.dispatch(startLoading(label));

export const stopLoadingProcess = (delay = true) =>
  setTimeout(
    () => {
      store.dispatch(stopLoading());
    },
    delay ? 500 : 0
  );

export const useReportAbuse = () =>
  useSelector((state) => state.process.reportAbuse);

export const startReportAbuseProcess = (reportInfo) =>
  store.dispatch(startReportAbuse(reportInfo));

export const finishReportAbuseProcess = () =>
  store.dispatch(finishReportAbuse());

export const useEmailIsVerified = () =>
  useSelector((state) => state.process.emailVerification.isVerified);

export const useEmailVerify = () =>
  useSelector((state) => state.process.emailVerification.email);

export const startEmailVerificationProcess = (email) =>
  store.dispatch(startEmailVerification(email));

export const verifyEmailVerification = (email) =>
  store.dispatch(verifyEmail(email));

export const finishEmailVerificationProcess = () =>
  store.dispatch(finishVerification());

export const saveFollowedCategories = (categories) =>
  store.dispatch(saveCategories(categories));

export const saveBuildFormValues = (values) =>
  store.dispatch(saveFormValues(values));

export const saveBuildFormProviderAvatarValues = (values) =>
  store.dispatch(saveProviderAvatar(values));

export const saveBuildFormFirstAvatarValues = (values) =>
  store.dispatch(saveFirstAvatar(values));

export const savePublishResourceForm = (values) =>
  store.dispatch(savePublishResourceFormValues(values));

export const useFollowedCategories = () =>
  useSelector((state) => state.process.profileBuild.categories);

export const useBuildFormValues = () =>
  useSelector((state) => state.process.profileBuild.formValues);

export const useBuildFormProviderAvatarValues = () =>
  useSelector((state) => state.process.profileBuild.providerAvatar);

export const useBuildFormFirstAvatarValues = () =>
  useSelector((state) => state.process.profileBuild.firstAvatar);

export const usePublishResourceForm = () =>
  useSelector((state) => state.process.profileBuild.resourcePublishFormValues);
