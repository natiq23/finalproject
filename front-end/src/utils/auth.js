import store from "../store";
import { useSelector } from "react-redux";
import { signIn, logOut, build } from "../store/auth.js";
import { post } from "../services/request.js";

export const useAuthData = () => useSelector((state) => state.auth.auth);

export const useId = () => useSelector((state) => state.auth.auth.id);
export const useName = () => useSelector((state) => state.auth.auth.name);

export const useConnectedAccountType = () =>
  useSelector((state) => state.auth.auth.connectedAccountType);

export const useUserRole = () => useSelector((state) => state.auth.auth.role);

export const useUserEmail = () => useSelector((state) => state.auth.auth.email);

export const useUserAvatar = () =>
  useSelector((state) => state.auth.auth.avatar);

export const useUserProfileBuilt = () =>
  useSelector((state) => state.auth.auth.isProfileBuilt);

export const useUserSignedIn = () =>
  useSelector((state) => state.auth.auth.userSignedIn);

export const signInUser = (userData, rememberMe) =>
  store.dispatch(signIn({ result: userData, rememberMe: rememberMe }));

export const logOutUser = () => store.dispatch(logOut());
export const buildUser = () => store.dispatch(build());
