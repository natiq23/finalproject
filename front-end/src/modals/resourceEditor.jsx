import ModalHeader from "../components/modals/modal-header/index.js";
import { Form, Formik } from "formik";
import TextInputWithIcon from "../components/form/text-input-with-icon/index.js";
import TextInput from "../components/form/text-input/index.js";
import { BiLink } from "react-icons/bi";
import PrimaryAccentButton from "../components/buttons/primary-accent-button/index.js";
import ErrorView from "../components/common/error-view/index.js";
import InfoView from "../components/common/info-view/index.js";
import { useEffect, useState } from "react";
import { useAutoAnimate } from "@formkit/auto-animate/react";
import SelectInput from "../components/form/select-input/index.js";
import InlineLoader from "../components/loaders/inline-loader/index.js";
import { ThreeDots } from "react-loader-spinner";
import { TagService } from "../services";
import { ResourceEditorSchema } from "../validation/Schemas/ResourceEditorSchema.js";
import PropTypes from "prop-types";
import { destroyModal } from "../utils/modal.js";
import { successToast } from "../utils/toast.js";
import { ResourceService } from "../services/";

ResourceEditorModal.propTypes = {
  data: PropTypes.any,
};

export default function ResourceEditorModal({ data }) {
  const [animationParent] = useAutoAnimate();
  const [isLoading, setIsLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [infoMessage, setInfoMessage] = useState("");
  const [categories, setCategories] = useState([]);
  const [flags, setFlags] = useState([]);
  const [categoriesIsLoading, setCategoriesIsLoading] = useState(false);
  const [flagsIsLoading, setFlagsIsLoading] = useState(false);

  const initialValues = {
    resourceId: data.resource.id,
    title: data.resource.title,
    url: data.resource.url,
    categoryId: data.resource.category.id,
    resourceFlagId: data.resource.flag.id,
  };

  useEffect(() => {
    setCategoriesIsLoading(true);
    TagService.getAllTags("categories")
      .then((res) => {
        const categories = res.map((category) => {
          return {
            label: category.title,
            value: category.id,
          };
        });
        setCategories(categories);
        setCategoriesIsLoading(false);
      })
      .catch((error) => {
        console.log(error);
        setCategoriesIsLoading(false);
      });

    setFlagsIsLoading(true);
    TagService.getAllTags("resource-flags")
      .then((res) => {
        const flags = res.map((category) => {
          return {
            label: category.title,
            value: category.id,
          };
        });

        setFlags(flags);
        setFlagsIsLoading(false);
      })
      .catch((error) => {
        console.log(error);
        setFlagsIsLoading(false);
      });
  }, []);

  return (
    <div className={"mx-2 w-max-[600px]"}>
      <ModalHeader title={"Resource Editor"} />
      <div
        className={
          "flex flex-col items-center justify-center gap-4 bg-white dark:bg-dark-component-bg px-6 py-3 rounded-b-md"
        }
      >
        {/*Info Section*/}
        <p className={"text-second-text text-sm dark:text-gray-400"}>
          Have an idea to make this resource more attention-grabbing for the
          community? <br />
          Here you to edit it!
        </p>

        <Formik
          initialValues={initialValues}
          onSubmit={(values) => {
            setInfoMessage("");
            setErrorMessage("");

            setIsLoading(true);
            let resource = values;

            TagService.getTag("categories", resource.categoryId).then((res) => {
              resource.category = res;
              TagService.getTag("resource-flags", resource.resourceFlagId).then(
                (res) => {
                  resource.flag = res;
                  ResourceService.editResource(resource)
                    .then(() => {
                      setIsLoading(false);
                      successToast("Resource updated!");
                      data.updateResource(resource);
                      destroyModal();
                    })
                    .catch((error) => {
                      console.log(error);
                      setErrorMessage(error.message);
                    });
                }
              );
            });
          }}
          validationSchema={ResourceEditorSchema}
        >
          {({ dirty }) => (
            <Form
              className={"w-full flex flex-col gap-4"}
              ref={animationParent}
            >
              <TextInput
                name={"title"}
                placeholder={"Brief description"}
                maxLength={76}
              />
              <TextInput
                name={"url"}
                placeholder={"Paste Url"}
                autoComplete={"url"}
                icon={<BiLink />}
                disabled={true}
              />

              <div className={"w-full flex justify-between gap-4"}>
                <SelectInput
                  name={"categoryId"}
                  placeholder={"Select category"}
                  data={categories}
                  isLoading={categoriesIsLoading}
                />

                <SelectInput
                  name={"resourceFlagId"}
                  placeholder={"Select flag"}
                  data={flags}
                  isLoading={flagsIsLoading}
                />
              </div>

              {isLoading && (
                <InlineLoader
                  loader={
                    <ThreeDots
                      height="24"
                      width="24"
                      color={"#3B49DF"}
                      wrapperClass="radio-wrapper"
                      radius="9"
                      ariaLabel="three-dots-loading"
                      wrapperStyle={{}}
                      wrapperClassName=""
                      visible={true}
                    />
                  }
                  message={"Resource updating..."}
                  version={"input-like"}
                />
              )}

              {infoMessage && (
                <InfoView message={infoMessage} version={"input-like"} />
              )}

              {errorMessage && (
                <ErrorView message={errorMessage} version={"input-like"} />
              )}

              <PrimaryAccentButton
                title={"Save Changes"}
                disabled={!dirty}
                type={"submit"}
              />
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
}
