import ModalHeader from "../components/modals/modal-header/index.js";
import PropTypes from "prop-types";
import { Form, Formik } from "formik";
import TextInput from "../components/form/text-input/index.js";
import PrimaryAccentButton from "../components/buttons/primary-accent-button/index.js";
import { useAutoAnimate } from "@formkit/auto-animate/react";
import { TagEditorSchema } from "../validation/Schemas/TagEditorSchema.js";
import ImageInput from "../components/form/image-input/index.js";
import TagPreview from "../components/common/tag-preview/index.js";
import { TagService } from "../services";
import { useState } from "react";
import { successToast } from "../utils/toast.js";
import InlineLoader from "../components/loaders/inline-loader/index.js";
import { ThreeDots } from "react-loader-spinner";
import ErrorView from "../components/common/error-view/index.js";
import { createModal, destroyModal } from "../utils/modal.js";
import DangerButton from "../components/buttons/danger-button/index.js";
import { getTagColor, getTagTypeTitle } from "../utils/common.js";

export function TagEditor({ data }) {
  const { animationParent } = useAutoAnimate();

  const [loadingMessage, setLoadingMessage] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  const { type, tag, refresh } = data;

  const typeTitle = getTagTypeTitle(type);

  return (
    <div
      className={"mx-2  w-max-[700px] min-w-[400px] box-content"}
      ref={animationParent}
    >
      <ModalHeader title={`${typeTitle} Editor`} />

      <div
        className={
          "w-full flex flex-col items-center justify-center gap-4 bg-white dark:bg-dark-component-bg px-6 py-3 rounded-b-md"
        }
      >
        <Formik
          initialValues={
            tag
              ? { ...tag }
              : {
                  title: "",
                  iconLink: null,
                  description: "",
                  accentColor: "0",
                }
          }
          onSubmit={(values) => {
            console.log("submit");
            setErrorMessage("");
            setLoadingMessage(
              tag ? `${typeTitle} Updating...` : `${typeTitle} publishing...`
            );

            if (tag) {
              TagService.updateTag(type, values)
                .then(() => {
                  setLoadingMessage("");
                  successToast(`${typeTitle} Updated!`);
                  refresh();
                  destroyModal();
                })
                .catch((error) => {
                  setLoadingMessage("");
                  if (error.status === 409) {
                    setErrorMessage(`This ${typeTitle} title already taken.`);
                  } else {
                    setErrorMessage("A problem occured.");
                  }
                });
            } else {
              TagService.insertTag(type, values)
                .then(() => {
                  setLoadingMessage("");
                  successToast(`${typeTitle} Published!`);
                  refresh();
                  destroyModal();
                })
                .catch((error) => {
                  setLoadingMessage("");
                  if (error.status === 409) {
                    setErrorMessage(`This ${typeTitle} already published.`);
                  } else {
                    setErrorMessage("A problem occured.");
                  }
                });
            }
          }}
          validationSchema={TagEditorSchema}
          ref={animationParent}
        >
          {({ dirty, values, setFieldValue }) => (
            <Form
              className={"w-full flex flex-col gap-4"}
              ref={animationParent}
            >
              <TextInput name={"title"} title={"Title"} isRequired={true} />
              <TextInput name={"description"} title={"Description"} />

              <div className={"grid grid-cols-[70px,1fr] gap-6  items-center "}>
                <div
                  className={
                    "w-fit flex flex-col gap-3 items-center justify-center"
                  }
                >
                  <p className={"font-medium dark:text-gray-400"}>Tag Color</p>
                  <button
                    type={"button"}
                    className={
                      "w-9 h-7 rounded-md dark:border-[1px] dark:border-dark-border-clr"
                    }
                    style={{
                      backgroundColor: getTagColor(values.accentColor).text,
                    }}
                    onClick={() => {
                      createModal("tagColorEditor", {
                        colorId: values.accentColor,
                        setColor: (color) => {
                          setFieldValue("accentColor", color.id.toString());
                        },
                      });
                    }}
                  />
                </div>
                <div
                  className={
                    "w-full flex flex-col gap-3 items-center justify-center"
                  }
                >
                  <p className={"font-medium dark:text-gray-400"}>
                    Preview tag
                  </p>
                  <div>
                    <TagPreview
                      title={
                        values.title.length > 0 ? values.title : "peview tag"
                      }
                      colorId={values.accentColor}
                    />
                  </div>
                </div>
              </div>

              <ImageInput name={"iconLink"} title={"Select Icon"} />

              {loadingMessage && (
                <InlineLoader
                  loader={
                    <ThreeDots
                      height="24"
                      width="24"
                      color={"#3B49DF"}
                      wrapperClass="radio-wrapper"
                      radius="9"
                      ariaLabel="three-dots-loading"
                      wrapperStyle={{}}
                      wrapperClassName=""
                      visible={true}
                    />
                  }
                  message={loadingMessage}
                  version={"input-like"}
                />
              )}
              {errorMessage && (
                <ErrorView message={errorMessage} version={"input-like"} />
              )}

              <div className={"flex gap-3"}>
                {tag && (
                  <DangerButton
                    title={`Delete ${typeTitle}`}
                    click={() => {
                      setLoadingMessage(`${typeTitle} Deleting...`);
                      TagService.deleteTag(type, tag.id)
                        .then(() => {
                          setLoadingMessage("");
                          successToast(`${typeTitle} Deleted!`);
                          refresh();
                          destroyModal();
                        })
                        .catch((error) => {
                          console.log(error);
                          setLoadingMessage("");
                          setErrorMessage("A problem occured.");
                        });
                    }}
                  />
                )}

                <PrimaryAccentButton
                  type={"submit"}
                  title={tag ? "Save Changes" : `Publish ${typeTitle}`}
                  click={() => {}}
                  disabled={!dirty && tag}
                />
              </div>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
}

TagEditor.propTypes = {
  data: PropTypes.object,
};
