import ModalHeader from "../components/modals/modal-header/index.js";
import PrimaryAccentButton from "../components/buttons/primary-accent-button/index.js";
import PrimaryButton from "../components/buttons/primary-button/index.js";
import { destroyModal } from "../utils/modal.js";
import { removeUser } from "../utils/user.js";
import { logOutUser } from "../utils/auth.js";
import { signOutSetting } from "../utils/appSettings.js";
import { signOutApi } from "../services/auth.js";

export default function SignOutModal() {
  const handleStay = () => {
    destroyModal();
  };

  const handleSignOut = async () => {
    logOutUser();
    removeUser();
    signOutSetting();
    await signOutApi();

    destroyModal();
  };

  return (
    <div className={"mx-2 w-max-[500px]"}>
      <ModalHeader title={"Sign Out"} />
      <div
        className={
          "flex flex-col items-center justify-center gap-4 bg-white dark:bg-dark-component-bg px-6 py-3 rounded-b-md"
        }
      >
        <p className={"font-semibold text-lg dark:text-gray-300"}>
          Are you sure you want to sign out?
        </p>

        <div className={"w-full flex gap-3"}>
          <PrimaryAccentButton click={handleSignOut} title={"Yes,sign out"} />
          <PrimaryButton click={handleStay} title={"No,stay in"} />
        </div>
      </div>
    </div>
  );
}
