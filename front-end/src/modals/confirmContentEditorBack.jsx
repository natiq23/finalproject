import PropTypes from "prop-types";
import ModalHeader from "../components/modals/modal-header/index.js";
import DangerButton from "../components/buttons/danger-button/index.js";
import PrimaryButton from "../components/buttons/primary-button/index.js";
import { destroyModal } from "../utils/modal.js";

export default function ConfirmContentEditorBack({ data }) {
  return (
    <div className={"mx-2 w-max-[500px]"}>
      <ModalHeader title={"You have unsaved changes"} />
      <div
        className={
          "flex flex-col items-center justify-center gap-4 bg-white dark:bg-dark-component-bg px-6 py-3 rounded-b-md"
        }
      >
        <p className={"dark:text-gray-300 "}>
          You&apos;ve made changes to your {data.contentType}. Do you want to
          navigate to leave this page?
        </p>
        <div className={"w-full flex gap-3"}>
          <DangerButton
            title={"Yes,leave the page"}
            click={() => {
              data.confirm();
              destroyModal();
            }}
          />
          <PrimaryButton
            title={"No,keep editing"}
            click={() => destroyModal()}
          />
        </div>
      </div>
    </div>
  );
}

ConfirmContentEditorBack.propTypes = {
  data: PropTypes.object,
};
