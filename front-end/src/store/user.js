import { createSlice } from "@reduxjs/toolkit";

const loadUserStateFromLocalStorage = () => {
  try {
    const serializedState = localStorage.getItem("user-info");
    if (serializedState === null) {
      return undefined;
    }
    return JSON.parse(serializedState);
  } catch (error) {
    return undefined;
  }
};

const initialState = loadUserStateFromLocalStorage() || {
  user: {
    id: null,
    username: "",
    email: "",
    name: "",
    avatar: "",
  },
};

const user = createSlice({
  name: "user",
  initialState,
  reducers: {
    save: (state, action) => {
      state.user.id = action.payload.id;
      state.user.username = action.payload.username;
      state.user.name = action.payload.name;
      state.user.email = action.payload.email;
      state.user.avatar = action.payload.avatar;
      localStorage.setItem("user-info", JSON.stringify(state));
    },
    update: (state, action) => {
      state.user.id = action.payload.id ?? state.user.id;
      state.user.username = action.payload.username ?? state.user.username;
      state.user.name = action.payload.name ?? state.user.name;
      state.user.email = action.payload.email ?? state.user.email;
      state.user.avatar = action.payload.avatar ?? state.user.avatar;
      localStorage.setItem("user-info", JSON.stringify(state));
    },
    remove: (state) => {
      state.user.id = null;
      state.user.userName = null;
      state.user.name = null;
      state.user.email = null;
      state.user.avatar = null;
      localStorage.removeItem("user-info");
    },
  },
});

export const { save, update, remove } = user.actions;
export default user.reducer;
