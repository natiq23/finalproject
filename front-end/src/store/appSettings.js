import { createSlice } from "@reduxjs/toolkit";

const loadSettingsStateFromLocalStorage = () => {
  try {
    const serializedState = localStorage.getItem("app-settings");
    if (serializedState === null) {
      return undefined;
    }
    return JSON.parse(serializedState);
  } catch (error) {
    return undefined;
  }
};

const initialState = loadSettingsStateFromLocalStorage() || {
  theme: "Light",
};

const appSettings = createSlice({
  name: "appSettings",
  initialState,
  reducers: {
    toggleTheme: (state) => {
      state.theme = state.theme === "Light" ? "Dark" : "Light";
      localStorage.setItem("app-settings", JSON.stringify(state));
    },
    update: (state, action) => {
      state.theme = action.payload.theme;
      localStorage.setItem("app-settings", JSON.stringify(state));
    },
    signOut: (state) => {
      state.theme = "light";
      localStorage.removeItem("app-settings");
    },
  },
});

export const { toggleTheme, signOut, update } = appSettings.actions;

export default appSettings.reducer;
