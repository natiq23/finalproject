import Yup from "../validation-localization.js";
export const TagEditorSchema = Yup.object().shape({
  title: Yup.string().min(3).required(),
  accentColor: Yup.string().required(),
});
