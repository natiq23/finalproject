import Yup from "../validation-localization.js";

export const PublishProductSchema = Yup.object().shape({
  name: Yup.string().required(),
  photoPaths: Yup.array().min(1, "Please upload at least 2 image").required(),
  description: Yup.string().required(),
  price: Yup.number().min(1).required(),
  discount: Yup.number().min(0).max(100),
  stock: Yup.number().min(1).required(),
});
