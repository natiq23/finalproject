import Yup from "../validation-localization.js";

export const ReportActionSchema = Yup.object().shape({
  reportStatus: Yup.string().required(),
  moderatorMessage: Yup.string(),
});
