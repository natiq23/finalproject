import { get, patch } from "./request.js";

export const getUnreadNotificationsCount = () =>
  get("Notifications/GetUnreadNotificationsCount");

export const getAllNotifications = () =>
  get("Notifications/GetAllNotifications");

export const getAllNotificationsByType = (type) =>
  get(`Notifications/GetNotificationsByType/${type}`);

export const readNotifications = () => patch("Notifications/ReadNotifications");
