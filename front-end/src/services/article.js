import { get } from "./request.js";
import { generateAllUrl } from "../utils/common.js";
import { useUser } from "../utils/user.js";

export const getAllArticle = (q, category, flag, sort) => {
  let query = generateAllUrl("articles/GetAllArticle?", {
    q,
    category,
    flag,
    sort,
  });
  return get(query);
};

export const getAllSavedArticle = (q, category, flag, sort) => {
  let query = generateAllUrl("articles/GetAllUserSavedArticle?", {
    q,
    category,
    flag,
    sort,
  });
  return get(query);
};

export const getFlowArticle = () => get("articles/GetArticlesForFlow");

export const getMoreFromArticle = (articleId) =>
  get(`articles/GetArticleMoreFrom/${articleId}`);

export const getUserArticles = (userId) => {
  return get(`articles/GetAllUserArticle?UserId=${userId}`);
};

export const getFeed = () => get("articles/GetArticlesForFeed");
