import { get } from "./request.js";

export const checkUrlIsAvailable = (url) => get(`Common/CheckUrl?url=${url}`);
