import { get, patch, post } from "./request.js";

export const insertReport = (data) =>
  post("Report/ReportContent", { ReportDTO: data });

export const getAllReports = () => get("Report/GetAllReports");

export const getAllModeratorReports = (id) =>
  get(`Report/GetAllReportsByModeratorId?ModeratorId=${id}`);

export const getReport = (id) => get(`Report/GetReportById?ReportId=${id}`);

export const submitReport = (data) => patch("Report/SubmitReport", data);
